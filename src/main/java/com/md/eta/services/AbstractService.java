/**
Author	:	 Raj Varadarajan @ Sopra Group Limited.
Date	:	 Thu Jul 02 08:22:02 BST 2009
Description:	 
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.md.eta.services;

import com.md.eta.dao.FailedMessagesDao;
import com.md.eta.dao.TechErrorsDao;
import com.md.eta.dao.XrcEngineerSignaturesDao;
import com.md.eta.dao.XrcProgrammeStampsDao;
import com.md.eta.dao.admin.GenericDao;
import com.md.eta.dao.admin.ParamsDao;
import com.md.eta.dao.admin.RoleMapDao;
import com.md.eta.dao.admin.XrcAcDataDao;
import com.md.eta.dao.admin.XrcAirframerDao;
import com.md.eta.dao.admin.XrcConcessionStatusesDao;
import com.md.eta.dao.admin.XrcDamageTypesDao;
import com.md.eta.dao.admin.XrcDepartmentDao;
import com.md.eta.dao.admin.XrcDepartmentLocationsDao;
import com.md.eta.dao.admin.XrcDeptUserDao;
import com.md.eta.dao.admin.XrcEmailNotificationsDao;
import com.md.eta.dao.admin.XrcEvtNotificationDelegatesDao;
import com.md.eta.dao.concessions.CustomerConcessionDao;
import com.md.eta.dao.concessions.MyConcessionDao;
import com.md.eta.dao.concessions.MyCustomerConcessionDao;
import com.md.eta.dao.concessions.RegisterFormDao;
import com.md.eta.dao.concessions.XxmSpAuditDocsDao;
import com.md.eta.dao.concessions.XxmSpDocRequestDao;
import com.md.eta.dao.generic.QpListHeadersDao;
import com.md.eta.dao.iportal.screenFunction.ScreenFunctionsDao;
import com.md.eta.dao.iportal.users.XiaSpUserDao;
import com.md.eta.dao.menus.SsoMenuDao;
import com.md.eta.dao.portal.screenFunction.PortalScreenFunctionsDao;
import com.md.eta.dao.portal.users.XxmSpUsersDao;
import com.md.eta.dao.rc.XrcConcessionRolesDao;

/**
 * 
 * @author Raj
 */
public abstract class AbstractService {
	private ParamsDao paramsDao;
	private ScreenFunctionsDao screenFunctionsDao;
	private XxmSpUsersDao xxmSpUsersDao;
	private XrcAcDataDao xrcAcDataDao;
	private XrcAirframerDao xrcAirframerDao;
	private XrcDepartmentDao xrcDepartmentDao;
	
	private GenericDao genericDao;
	
	private QpListHeadersDao qpListHeadersDao;
	private XiaSpUserDao xiaSpUserDao;
	
	private XrcConcessionStatusesDao xrcConcessionStatusesDao;
	
	private PortalScreenFunctionsDao portalScreenFunctionsDao;
	private XrcConcessionRolesDao xrcConcessionRolesDao;
	
	
	private RoleMapDao roleMapDao;

	private CustomerConcessionDao customerConcessionDao;

	
	private XxmSpDocRequestDao xxmSpDocRequestDao;
	private MyConcessionDao myConcessionDao;
	private XxmSpAuditDocsDao xxmSpAuditDocsDao;
	private MyCustomerConcessionDao myCustomerConcessionDao;
	private XrcDamageTypesDao xrcDamageTypesDao;
	private XrcDeptUserDao xrcDeptUserDao;
	
	private XrcProgrammeStampsDao xrcProgrammeStampsDao;
	private XrcEngineerSignaturesDao xrcEngineerSignaturesDao;
		
	
	
	private TechErrorsDao techErrorsDao;
	
	private XrcDepartmentLocationsDao xrcDepartmentLocationsDao;
	private XrcEmailNotificationsDao xrcEmailNotificationsDao;
	private XrcEvtNotificationDelegatesDao xrcEvtNotificationDelegatesDao;
	
	private FailedMessagesDao failedMessagesDao;
	private RegisterFormDao registerFormDao;
	private SsoMenuDao ssoMenuDao;
	
	public XxmSpAuditDocsDao getXxmSpAuditDocsDao() {
		return xxmSpAuditDocsDao;
	}

	public void setXxmSpAuditDocsDao(XxmSpAuditDocsDao xxmSpAuditDocsDao) {
		this.xxmSpAuditDocsDao = xxmSpAuditDocsDao;
	}

	public MyConcessionDao getMyConcessionDao() {
		return myConcessionDao;
	}

	public void setMyConcessionDao(MyConcessionDao myConcessionDao) {
		this.myConcessionDao = myConcessionDao;
	}

	public XxmSpDocRequestDao getXxmSpDocRequestDao() {
		return xxmSpDocRequestDao;
	}

	public void setXxmSpDocRequestDao(XxmSpDocRequestDao xxmSpDocRequestDao) {
		this.xxmSpDocRequestDao = xxmSpDocRequestDao;
	}

	

	public CustomerConcessionDao getCustomerConcessionDao() {
		return customerConcessionDao;
	}

	public void setCustomerConcessionDao(
			CustomerConcessionDao customerConcessionDao) {
		this.customerConcessionDao = customerConcessionDao;
	}

	

	public PortalScreenFunctionsDao getPortalScreenFunctionsDao() {
		return portalScreenFunctionsDao;
	}

	public void setPortalScreenFunctionsDao(
			PortalScreenFunctionsDao portalScreenFunctionsDao) {
		this.portalScreenFunctionsDao = portalScreenFunctionsDao;
	}

	

	public XrcConcessionStatusesDao getXrcConcessionStatusesDao() {
		return xrcConcessionStatusesDao;
	}

	public void setXrcConcessionStatusesDao(
			XrcConcessionStatusesDao xrcConcessionStatusesDao) {
		this.xrcConcessionStatusesDao = xrcConcessionStatusesDao;
	}

	
	public GenericDao getGenericDao() {
		return genericDao;
	}

	public void setGenericDao(GenericDao genericDao) {
		this.genericDao = genericDao;
	}

	
	public ParamsDao getParamsDao() {
		return paramsDao;
	}

	public void setParamsDao(ParamsDao paramsDao) {
		this.paramsDao = paramsDao;
	}

	

	public void setScreenFunctionsDao(ScreenFunctionsDao screenFunctionsDao) {
		this.screenFunctionsDao = screenFunctionsDao;
	}

	public ScreenFunctionsDao getScreenFunctionsDao() {
		return screenFunctionsDao;
	}

	

	public XxmSpUsersDao getXxmSpUsersDao() {
		return xxmSpUsersDao;
	}

	public void setXxmSpUsersDao(XxmSpUsersDao xxmSpUsersDao) {
		this.xxmSpUsersDao = xxmSpUsersDao;
	}

	public void setXrcAcDataDao(XrcAcDataDao xrcAcDataDao) {
		this.xrcAcDataDao = xrcAcDataDao;
	}

	public XrcAcDataDao getXrcAcDataDao() {
		return xrcAcDataDao;
	}
	
	public XrcAirframerDao getXrcAirframerDao() {
		return xrcAirframerDao;
	}

	public void setXrcAirframerDao(XrcAirframerDao xrcAirframerDao) {
		this.xrcAirframerDao = xrcAirframerDao;
	}



	public QpListHeadersDao getQpListHeadersDao() {
		return qpListHeadersDao;
	}

	public void setQpListHeadersDao(QpListHeadersDao qpListHeadersDao) {
		this.qpListHeadersDao = qpListHeadersDao;
	}

	public XiaSpUserDao getXiaSpUserDao() {
		return xiaSpUserDao;
	}

	public void setXiaSpUserDao(XiaSpUserDao xiaSpUserDao) {
		this.xiaSpUserDao = xiaSpUserDao;
	}

	public void setXrcConcessionRolesDao(
			XrcConcessionRolesDao xrcConcessionRolesDao) {
		this.xrcConcessionRolesDao = xrcConcessionRolesDao;
	}

	public XrcConcessionRolesDao getXrcConcessionRolesDao() {
		return xrcConcessionRolesDao;
	}



	public RoleMapDao getRoleMapDao() {
		return roleMapDao;
	}

	public void setRoleMapDao(RoleMapDao roleMapDao) {
		this.roleMapDao = roleMapDao;
	}



	

	/**
	 * @return the myCustomerConcessionDao
	 */
	public MyCustomerConcessionDao getMyCustomerConcessionDao() {
		return myCustomerConcessionDao;
	}

	/**
	 * @param myCustomerConcessionDao
	 *            the myCustomerConcessionDao to set
	 */
	public void setMyCustomerConcessionDao(
			MyCustomerConcessionDao myCustomerConcessionDao) {
		this.myCustomerConcessionDao = myCustomerConcessionDao;
	}

	public XrcProgrammeStampsDao getXrcProgrammeStampsDao() {
		return xrcProgrammeStampsDao;
	}

	public void setXrcProgrammeStampsDao(XrcProgrammeStampsDao xrcProgrammeStampsDao) {
		this.xrcProgrammeStampsDao = xrcProgrammeStampsDao;
	}

	public XrcEngineerSignaturesDao getXrcEngineerSignaturesDao() {
		return xrcEngineerSignaturesDao;
	}

	public void setXrcEngineerSignaturesDao(
			XrcEngineerSignaturesDao xrcEngineerSignaturesDao) {
		this.xrcEngineerSignaturesDao = xrcEngineerSignaturesDao;
	}



	public XrcDamageTypesDao getXrcDamageTypesDao() {
		return xrcDamageTypesDao;
	}

	public void setXrcDamageTypesDao(XrcDamageTypesDao xrcDamageTypesDao) {
		this.xrcDamageTypesDao = xrcDamageTypesDao;
	}

	public TechErrorsDao getTechErrorsDao() {
		return techErrorsDao;
	}

	public void setTechErrorsDao(TechErrorsDao techErrorsDao) {
		this.techErrorsDao = techErrorsDao;
	}



	
	public XrcDepartmentDao getXrcDepartmentDao() {
		return xrcDepartmentDao;
	}

	public void setXrcDepartmentDao(XrcDepartmentDao xrcDepartmentDao) {
		this.xrcDepartmentDao = xrcDepartmentDao;
	}



	public XrcDeptUserDao getXrcDeptUserDao() {
		return xrcDeptUserDao;
	}

	public void setXrcDeptUserDao(XrcDeptUserDao xrcDeptUserDao) {
		this.xrcDeptUserDao = xrcDeptUserDao;
	}

	public void setXrcDepartmentLocationsDao(XrcDepartmentLocationsDao xrcDepartmentLocationsDao) {
		this.xrcDepartmentLocationsDao = xrcDepartmentLocationsDao;
	}

	public XrcDepartmentLocationsDao getXrcDepartmentLocationsDao() {
		return xrcDepartmentLocationsDao;
	}

	public void setXrcEmailNotificationsDao(XrcEmailNotificationsDao xrcEmailNotificationsDao) {
		this.xrcEmailNotificationsDao = xrcEmailNotificationsDao;
	}

	public XrcEmailNotificationsDao getXrcEmailNotificationsDao() {
		return xrcEmailNotificationsDao;
	}

	public void setXrcEvtNotificationDelegatesDao(
			XrcEvtNotificationDelegatesDao xrcEvtNotificationDelegatesDao) {
		this.xrcEvtNotificationDelegatesDao = xrcEvtNotificationDelegatesDao;
	}

	public XrcEvtNotificationDelegatesDao getXrcEvtNotificationDelegatesDao() {
		return xrcEvtNotificationDelegatesDao;
	}
	


	public FailedMessagesDao getFailedMessagesDao() {
		return failedMessagesDao;
	}

	public void setFailedMessagesDao(FailedMessagesDao failedMessagesDao) {
		this.failedMessagesDao = failedMessagesDao;
	}
	
	public RegisterFormDao getRegisterFormDao() {
		return registerFormDao;
	}

	public void setRegisterFormDao(RegisterFormDao registerFormDao) {
		this.registerFormDao = registerFormDao;
	}

	public SsoMenuDao getSsoMenuDao() {
		return ssoMenuDao;
	}

	public void setSsoMenuDao(SsoMenuDao ssoMenuDao) {
		this.ssoMenuDao = ssoMenuDao;
	}
}
