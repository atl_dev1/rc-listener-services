package com.md.eta.services;

import java.sql.SQLException;
import com.md.eta.model.FailedMessages;

public interface FailedMessagesService {
	
	public Long save(FailedMessages pFailedMessages) throws SQLException;
	
	public FailedMessages fetchById(FailedMessages pFailedMessages) throws SQLException;	
}
