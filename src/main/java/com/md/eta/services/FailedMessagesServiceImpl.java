package com.md.eta.services;

import java.sql.SQLException;

import com.md.eta.model.FailedMessages;

public class FailedMessagesServiceImpl extends AbstractService implements FailedMessagesService{
	
	@Override
	public Long save(FailedMessages pFailedMessages) throws SQLException{
		return getFailedMessagesDao().save(pFailedMessages);
	}
	
	@Override
	public FailedMessages fetchById(FailedMessages pFailedMessages) throws SQLException{
		return getFailedMessagesDao().fetchById(pFailedMessages);
	}
}
