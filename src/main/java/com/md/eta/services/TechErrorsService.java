package com.md.eta.services;

import java.sql.SQLException;
import java.util.ArrayList;

import com.md.eta.model.TechErrors;

public interface TechErrorsService {

	public ArrayList<TechErrors> fetch(TechErrors pTecErrors)	throws SQLException;
	
	public ArrayList<TechErrors> fetchQueueMsg (TechErrors pTecErrors) throws SQLException;
	
	public Long save(TechErrors pTecErrors) throws SQLException;
	
	public int update(TechErrors pTecErrors) throws SQLException;
	
	public  Long getCount(TechErrors techErrors)  throws SQLException;
	
	public Long getCountQueueMsg(TechErrors pTechErrors) throws SQLException;
	
	public int delete(TechErrors pTecErrors) throws SQLException; 
	
	public TechErrors fetchById(TechErrors pTechErrors) throws SQLException;
}
