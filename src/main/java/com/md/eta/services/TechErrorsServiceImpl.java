package com.md.eta.services;

import java.sql.SQLException;
import java.util.ArrayList;

import com.md.eta.model.TechErrors;

public class TechErrorsServiceImpl extends AbstractService implements TechErrorsService {
	
	@Override
	public ArrayList<TechErrors> fetch(TechErrors pTecErrors)	throws SQLException {
		// TODO Auto-generated method stub
		return getTechErrorsDao().fetch(pTecErrors);
	}
	
	@Override
	public ArrayList<TechErrors> fetchQueueMsg(TechErrors pTecErrors)
			throws SQLException {
		// TODO Auto-generated method stub
		return getTechErrorsDao().fetchQueueMsg(pTecErrors);
	}

	@Override
	public Long save(TechErrors pTecErrors) throws SQLException {
		// TODO Auto-generated method stub
		return getTechErrorsDao().save(pTecErrors);
	}

	@Override
	public int update(TechErrors pTecErrors) throws SQLException {
		// TODO Auto-generated method stub
		return getTechErrorsDao().update(pTecErrors);
	}
	
	@Override
	public Long getCount(TechErrors pTecErrors) throws SQLException {
		return getTechErrorsDao().getCount(pTecErrors);
	}
	
	@Override
	public Long getCountQueueMsg(TechErrors pTechErrors) throws SQLException {
		// TODO Auto-generated method stub
		return getTechErrorsDao().getCountQueueMsg(pTechErrors);
	}
	
	@Override
	public int delete(TechErrors pTecErrors) throws SQLException {
		return getTechErrorsDao().delete(pTecErrors);
	}
	
	@Override
	public TechErrors fetchById(TechErrors pTechErrors) throws SQLException {
		return getTechErrorsDao().fetchById(pTechErrors);
	}



}
