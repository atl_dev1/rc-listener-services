package com.md.eta.services;

import java.sql.SQLException;
import java.util.ArrayList;

import com.md.eta.model.XrcEngineerSignatures;

public interface XrcEngineerSignaturesService {

	ArrayList<XrcEngineerSignatures> fetch(
			XrcEngineerSignatures pXrcEngineerSignatures) throws SQLException;

	int save(XrcEngineerSignatures pXrcEngineerSignatures) throws Exception;

	int update(XrcEngineerSignatures pXrcEngineerSignatures)
			throws SQLException;

	Long getCount(XrcEngineerSignatures pXrcEngineerSignatures)
			throws SQLException;

	int saveSign(XrcEngineerSignatures pXrcEngineerSignatures) throws Exception;
	
	public ArrayList<XrcEngineerSignatures> fetchSignature(XrcEngineerSignatures pXrcEngineerSignatures) throws SQLException;
}
