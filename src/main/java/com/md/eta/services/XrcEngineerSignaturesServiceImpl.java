package com.md.eta.services;

import java.sql.SQLException;
import java.util.ArrayList;

import com.md.eta.model.XrcEngineerSignatures;

public class XrcEngineerSignaturesServiceImpl extends AbstractService implements  XrcEngineerSignaturesService {

@Override
public ArrayList<XrcEngineerSignatures> fetch(XrcEngineerSignatures pXrcEngineerSignatures) throws SQLException {
	return getXrcEngineerSignaturesDao().fetch(pXrcEngineerSignatures);
}
@Override
public int save(XrcEngineerSignatures pXrcEngineerSignatures) throws Exception {
	// TODO Auto-generated method stub
	return  getXrcEngineerSignaturesDao().save(pXrcEngineerSignatures);
}
@Override
public int update(XrcEngineerSignatures pXrcEngineerSignatures) throws SQLException {
	// TODO Auto-generated method stub
	return  getXrcEngineerSignaturesDao().update(pXrcEngineerSignatures);
}
@Override
public Long getCount(XrcEngineerSignatures pXrcEngineerSignatures) throws SQLException {
	// TODO Auto-generated method stub
	return  getXrcEngineerSignaturesDao().getCount(pXrcEngineerSignatures); 
}

@Override
public int saveSign(XrcEngineerSignatures pXrcEngineerSignatures) throws Exception {
	// TODO Auto-generated method stub
	return  getXrcEngineerSignaturesDao().saveSign(pXrcEngineerSignatures);
}

@Override
public ArrayList<XrcEngineerSignatures> fetchSignature(XrcEngineerSignatures pXrcEngineerSignatures) throws SQLException {
	return getXrcEngineerSignaturesDao().fetchSignature(pXrcEngineerSignatures);
}

 }