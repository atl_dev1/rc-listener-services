package com.md.eta.services;

import java.sql.SQLException;
import java.util.ArrayList;

import com.md.eta.model.XrcEngineerSignatures;
import com.md.eta.model.XrcProgrammeStamps;

public interface XrcProgrammeStampsService{

	public ArrayList<XrcProgrammeStamps> fetchStamps(Long concId) throws SQLException;
	
	ArrayList<XrcProgrammeStamps> fetch(XrcProgrammeStamps pXrcProgrammeStamps)
			throws SQLException;

	int save(XrcProgrammeStamps pXrcProgrammeStamps) throws Exception;

	int update(XrcProgrammeStamps pXrcProgrammeStamps) throws SQLException;

	Long getCount(XrcProgrammeStamps pXrcProgrammeStamps) throws SQLException;
	
	public ArrayList<XrcProgrammeStamps> fetchEnggStamps (XrcProgrammeStamps pXrcProgrammeStamps) throws SQLException;
	
	public int updateEnggStamps (XrcProgrammeStamps pXrcProgrammeStamps) throws SQLException;
	
}