package com.md.eta.services;

import java.sql.SQLException;
import java.util.ArrayList;

import com.md.eta.model.XrcEngineerSignatures;
import com.md.eta.model.XrcProgrammeStamps;

public class XrcProgrammeStampsServiceImpl extends AbstractService implements  XrcProgrammeStampsService {

@Override
public ArrayList<XrcProgrammeStamps> fetchStamps(Long concId) throws SQLException {
	return getXrcProgrammeStampsDao().fetchStamps(concId);
}		
@Override
public ArrayList<XrcProgrammeStamps> fetch(XrcProgrammeStamps pXrcProgrammeStamps) throws SQLException {
	return getXrcProgrammeStampsDao().fetch(pXrcProgrammeStamps);
}
@Override
public int save(XrcProgrammeStamps pXrcProgrammeStamps) throws Exception {
	// TODO Auto-generated method stub
	return  getXrcProgrammeStampsDao().save(pXrcProgrammeStamps);
}
@Override
public int update(XrcProgrammeStamps pXrcProgrammeStamps) throws SQLException {
	// TODO Auto-generated method stub
	return  getXrcProgrammeStampsDao().update(pXrcProgrammeStamps);
}
@Override
public Long getCount(XrcProgrammeStamps pXrcProgrammeStamps) throws SQLException {
	// TODO Auto-generated method stub
	return  getXrcProgrammeStampsDao().getCount(pXrcProgrammeStamps); 
}
@Override
public ArrayList<XrcProgrammeStamps> fetchEnggStamps(
		XrcProgrammeStamps pXrcProgrammeStamps) throws SQLException {
	// TODO Auto-generated method stub
	return getXrcProgrammeStampsDao().fetchEnggStamps(pXrcProgrammeStamps);
}
@Override
public int updateEnggStamps(XrcProgrammeStamps pXrcProgrammeStamps)
		throws SQLException {
	// TODO Auto-generated method stub
	return getXrcProgrammeStampsDao().updateEnggStamps(pXrcProgrammeStamps);
}

 }