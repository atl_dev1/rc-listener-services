package com.md.eta.services.admin;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.md.eta.model.admin.Company;
import com.md.eta.model.concessions.XrcDocumentTypes;
import com.md.eta.model.concessions.XrcOperators;
import com.md.eta.model.util.KeyValueBean;

public interface GenericService {
	
	public ArrayList<Company> getCustomers(Company company) throws SQLException;
	
	public ArrayList<XrcOperators> getOperators(XrcOperators pXrcOperators) throws SQLException;
	
	public abstract ArrayList<KeyValueBean> getRcCustomerNames(Company comp) throws SQLException;
	public abstract ArrayList<Company> getCustomerNames(Company comp) throws SQLException;
	public abstract ArrayList<Company> getCustomerNumbers(Company comp) throws SQLException;
	public abstract ArrayList<Company> getPartyNumbers(Company comp) throws SQLException;
	public abstract ArrayList<KeyValueBean> getPriceListNames() throws SQLException;
	
	public ArrayList<Company> getCustomersF7x(Company company) throws SQLException;
	
	public abstract ArrayList<KeyValueBean> getIportalUnallocatedFunctions(Integer usrId)
	throws SQLException;

	public ArrayList<KeyValueBean> getPortalUnallocatedFunctions(Integer usrId)
	throws SQLException;

	public List<KeyValueBean> getCurrency() throws Exception;
	
	public List<KeyValueBean> getCountryList(String name)  throws SQLException;
	
	public ArrayList<KeyValueBean> getCountryDropDown(String code, String name) throws Exception;

	public ArrayList<KeyValueBean> getIportalUnallocatedRoles(Integer usrId) throws SQLException;
	
	public ArrayList<XrcDocumentTypes> getDocumentTypes() throws SQLException;
	
	public abstract ArrayList<XrcDocumentTypes> getOnbaseDocTypes(String attachType) throws SQLException;
	
	public ArrayList<XrcDocumentTypes> getDocTypesSupportInfo() throws SQLException;
}