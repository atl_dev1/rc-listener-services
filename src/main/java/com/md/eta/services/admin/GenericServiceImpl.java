package com.md.eta.services.admin;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.md.eta.model.admin.Company;
import com.md.eta.model.concessions.XrcDocumentTypes;
import com.md.eta.model.concessions.XrcOperators;
import com.md.eta.model.util.KeyValueBean;
import com.md.eta.services.AbstractService;

public class GenericServiceImpl extends AbstractService implements
		GenericService {

	@Override
	public ArrayList<Company> getCustomers(Company company) throws SQLException {
		// TODO Auto-generated method stub
		return getGenericDao().getCustomers(company); 
	}
	
	@Override
	public ArrayList<XrcOperators> getOperators(XrcOperators pXrcOperators) throws SQLException{
		return getGenericDao().getOperators(pXrcOperators);
	}
		
	@Override
	public ArrayList<Company> getCustomersF7x(Company company) throws SQLException {
		// TODO Auto-generated method stub
		return getGenericDao().getCustomersF7x(company); 
	}

	@Override
	public ArrayList<KeyValueBean> getRcCustomerNames(Company comp)
			throws SQLException {
		// TODO Auto-generated method stub
		return getGenericDao().getRcCustomerNames(comp); 	
	}
	
	@Override
	public ArrayList<Company> getCustomerNumbers(Company comp)
			throws SQLException {
		// TODO Auto-generated method stub
		return getGenericDao().getCustomerNumbers(comp); 	
	}

	@Override
	public ArrayList<Company> getCustomerNames(Company comp)
			throws SQLException {
		// TODO Auto-generated method stub
		return getGenericDao().getCustomerNames(comp); 
	}
	
	@Override
	public ArrayList<Company> getPartyNumbers(Company comp)
			throws SQLException {
		// TODO Auto-generated method stub
		return getGenericDao().getPartyNumbers(comp); 
	}

	@Override
	public ArrayList<KeyValueBean> getPriceListNames() throws SQLException {
		// TODO Auto-generated method stub
		return getGenericDao().getPriceListNames();
	}

	@Override
	public ArrayList<KeyValueBean> getIportalUnallocatedFunctions(Integer usrId)
			throws SQLException {
		return getGenericDao().getIportalUnallocatedFunctions(usrId);
	}
	
	@Override
	public ArrayList<KeyValueBean> getPortalUnallocatedFunctions(Integer usrId) throws SQLException {
		return getGenericDao().getPortalUnallocatedFunctions(usrId);
	}
	
	public List<KeyValueBean> getCurrency() throws Exception{ 
		List<KeyValueBean> currencyList = new ArrayList<KeyValueBean>();
		currencyList.add(new KeyValueBean("", "Select Currency.."));
		currencyList.add(new KeyValueBean("EUR", "EUR"));
		currencyList.add(new KeyValueBean("GBP", "GBP"));
		currencyList.add(new KeyValueBean("USD", "USD"));
		return currencyList;
	}

	@Override
	public List<KeyValueBean> getCountryList(String name) throws SQLException {
		// TODO Auto-generated method stub
		return getGenericDao().getCountryList(name);
	}

	@Override
	public ArrayList<KeyValueBean> getIportalUnallocatedRoles(Integer usrId) throws SQLException {
		return getGenericDao().getIportalUnallocatedRoles(usrId);
	}

	@Override
	public ArrayList<KeyValueBean> getCountryDropDown(String code, String name)
			throws Exception {
		// TODO Auto-generated method stub
		return getGenericDao().getCountryDropDown(code, name);
	}
	
	@Override
	public ArrayList<XrcDocumentTypes> getDocumentTypes() throws SQLException {
		// TODO Auto-generated method stub
		return getGenericDao().getDocumentTypes(); 
	}
	
	@Override
	public ArrayList<XrcDocumentTypes> getOnbaseDocTypes(String attachType) throws SQLException {
		return getGenericDao().getOnbaseDocTypes(attachType);
	}
	
	@Override
	public ArrayList<XrcDocumentTypes> getDocTypesSupportInfo() throws SQLException {
		// TODO Auto-generated method stub
		return getGenericDao().getDocTypesSupportInfo(); 
	}
}