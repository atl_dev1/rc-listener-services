package com.md.eta.services.admin;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.md.eta.model.admin.Parameters;

public interface ParamService {
	
	public ArrayList<Parameters> getParam(Parameters parameters) throws SQLException;
	
	public int save(Parameters parameter) throws SQLException ;
	
	public int update(Parameters parameter) throws SQLException ;
	
	Long getCount(Parameters parameters) throws SQLException;
	
	public abstract ArrayList<Parameters> getAllParams(Parameters parameters) throws SQLException;
	
	List getParams();
	
	List getParamsStartingWith(String name);

}