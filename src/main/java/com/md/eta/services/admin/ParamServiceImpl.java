package com.md.eta.services.admin;


import java.sql.SQLException;
import java.util.ArrayList;


import java.util.List;

import com.md.eta.model.admin.Parameters;
import com.md.eta.services.AbstractService;


	/**
	 * @param args
	 */
	public class ParamServiceImpl extends AbstractService implements ParamService {

		@Override
		public ArrayList<Parameters> getParam(Parameters parameters)
				throws SQLException {
			// TODO Auto-generated method stub
			return getParamsDao().fetch(parameters);
		}

		@Override
		public int save(Parameters parameters) throws SQLException {
			// TODO Auto-generated method stub
			return getParamsDao().save(parameters);
		}

		@Override
		public int update(Parameters parameters) throws SQLException {
			// TODO Auto-generated method stub
			return getParamsDao().update(parameters);
		}

		@Override
		public Long getCount(Parameters parameters) throws SQLException {
			// TODO Auto-generated method stub
			return getParamsDao().getCount(parameters); 
		}

		@Override
		public ArrayList<Parameters> getAllParams(Parameters parameters)throws SQLException {
			// TODO Auto-generated method stub
			return getParamsDao().getAllParams(parameters); 
		}

		@Override
		public List getParams() {
			// TODO Auto-generated method stub
			return getParamsDao().getParams();
		}

		@Override
		public List getParamsStartingWith(String name) {
			return getParamsDao().getParamsStartingWith(name);
		}

	}