package com.md.eta.services.admin;

import java.sql.SQLException;
import java.util.ArrayList;

import com.md.eta.model.admin.RoleMap;
import com.md.eta.model.iportal.users.XiaSpUser;
import com.md.eta.model.util.KeyValueBean;


public interface RoleMapService {
	
	public abstract ArrayList<RoleMap> fetch(RoleMap roleMap) throws SQLException;
	
	public abstract ArrayList<RoleMap> view(RoleMap roleMap) throws SQLException;
	
	public ArrayList<RoleMap> getRoles(RoleMap roleMap) throws SQLException;
	
	public int save(RoleMap roleMap)  throws SQLException;
	
	public int update(RoleMap roleMap) throws SQLException;
	
	public int delete(RoleMap roleMap) throws SQLException;

	Long getCount(RoleMap roleMap)throws SQLException;
	
	public ArrayList<RoleMap> getRoleList(RoleMap roleMap) throws SQLException;
	
	public ArrayList<KeyValueBean> getUserList(XiaSpUser user) throws SQLException;
	
	public ArrayList<XiaSpUser> getUserRoleList(ArrayList roleList) throws SQLException;
	
}