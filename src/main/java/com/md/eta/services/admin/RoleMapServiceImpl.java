package com.md.eta.services.admin;

import java.sql.SQLException;
import java.util.ArrayList;

import com.md.eta.model.admin.RoleMap;
import com.md.eta.model.iportal.users.XiaSpUser;
import com.md.eta.model.util.KeyValueBean;
import com.md.eta.services.AbstractService;

public class RoleMapServiceImpl extends AbstractService implements RoleMapService {

	@Override
	public ArrayList<RoleMap> fetch(RoleMap roleMap) throws SQLException {
		// TODO Auto-generated method stub
		return getRoleMapDao().fetch(roleMap);
	}
	
	@Override
	public ArrayList<RoleMap> view(RoleMap roleMap) throws SQLException {
		// TODO Auto-generated method stub
		return getRoleMapDao().view(roleMap);
	}
	
	@Override
	public ArrayList<RoleMap> getRoles(RoleMap roleMap) throws SQLException {
		// TODO Auto-generated method stub
		return getRoleMapDao().getRoles(roleMap);
	}
	
	@Override
	public int save(RoleMap roleMap) throws SQLException {
		// TODO Auto-generated method stub
		return getRoleMapDao().save(roleMap);
	}

	@Override
	public int update(RoleMap roleMap) throws SQLException {
		// TODO Auto-generated method stub
		return getRoleMapDao().update(roleMap);
	}

	@Override
	public int delete(RoleMap roleMap) throws SQLException {
		// TODO Auto-generated method stub
		return getRoleMapDao().delete(roleMap);
	}
	@Override
	public Long getCount(RoleMap roleMap) throws SQLException {
		// TODO Auto-generated method stub
		return getRoleMapDao().getCount(roleMap);
	}
	@Override
	public ArrayList<RoleMap> getRoleList(RoleMap roleMap) throws SQLException {
		// TODO Auto-generated method stub
		return getRoleMapDao().getRoleList(roleMap);
	}
	@Override
	public ArrayList<KeyValueBean> getUserList(XiaSpUser user) throws SQLException {
		// TODO Auto-generated method stub
		return getRoleMapDao().getUserList(user);
	}

	@Override
	public ArrayList<XiaSpUser> getUserRoleList(ArrayList roleList)
			throws SQLException {
		// TODO Auto-generated method stub
		return getRoleMapDao().getUserRoleList(roleList);
	}


}