package com.md.eta.services.admin;

import java.sql.SQLException;
import java.util.ArrayList;

import com.md.eta.model.admin.XrcAcData;



public interface XrcAcDataService {
	public  ArrayList<XrcAcData> fetch(XrcAcData xrcAcData) throws SQLException;
    
	public int update(XrcAcData xrcAcData)  throws SQLException;
    
	public  int  save(XrcAcData xrcAcData)  throws SQLException;

	public  Long getCount(XrcAcData xrcAcData)  throws SQLException;
	
	public ArrayList<XrcAcData> getAcData(XrcAcData xrcAcData) throws SQLException;

}
