package com.md.eta.services.admin;

import java.sql.SQLException;
import java.util.ArrayList;



import com.md.eta.model.admin.XrcAcData;
import com.md.eta.services.AbstractService;

public class XrcAcDataServiceImpl extends AbstractService implements XrcAcDataService {

	@Override
	public ArrayList<XrcAcData> fetch(XrcAcData xrcAcData) throws SQLException {
		return getXrcAcDataDao().fetch(xrcAcData);
	}

	@Override
	public int update(XrcAcData xrcAcData) throws SQLException {
		return getXrcAcDataDao().update(xrcAcData);
	}

	@Override
	public int save(XrcAcData xrcAcData) throws SQLException {
		return getXrcAcDataDao().save(xrcAcData);
	}

	@Override
	public Long getCount(XrcAcData xrcAcData) throws SQLException {
		return getXrcAcDataDao().getCount(xrcAcData);
	}
	
	@Override
	public ArrayList<XrcAcData> getAcData(XrcAcData xrcAcData) throws SQLException {
		return getXrcAcDataDao().getAcData(xrcAcData);
	}

}
