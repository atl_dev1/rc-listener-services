package com.md.eta.services.admin;

import java.sql.SQLException;
import java.util.ArrayList;

import com.md.eta.model.admin.XrcAcData;
import com.md.eta.model.admin.XrcAirframers;
import com.md.eta.model.util.KeyValueBean;

public interface XrcAirframerService {
	
	public  Long getCount(XrcAirframers xrcAirframers)  throws SQLException;
	
	public  ArrayList<XrcAirframers> fetch(XrcAirframers xrcAirframer) throws SQLException;
	
	public int save(XrcAirframers pXrcAirframers) throws SQLException;
	
	public ArrayList<XrcAirframers> getAfData (XrcAirframers pXrcAirframers) throws SQLException;
	
	public int getAcForAf(XrcAcData xrcAcData) throws SQLException;
	
	public int update(XrcAirframers pXrcAirframers) throws SQLException;
	
	public ArrayList<KeyValueBean> getXrcAirframersList (XrcAirframers pXrcAirframers) throws SQLException;
}
