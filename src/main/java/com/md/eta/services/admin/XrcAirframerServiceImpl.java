package com.md.eta.services.admin;

import java.sql.SQLException;
import java.util.ArrayList;

import com.md.eta.model.admin.XrcAcData;
import com.md.eta.model.admin.XrcAirframers;
import com.md.eta.model.util.KeyValueBean;
import com.md.eta.services.AbstractService;

public class XrcAirframerServiceImpl extends AbstractService implements XrcAirframerService {
	
	@Override
	public Long getCount(XrcAirframers xrcAirframers) throws SQLException {
		return getXrcAirframerDao().getCount(xrcAirframers);
	}
	
	@Override
	public ArrayList<XrcAirframers> fetch(XrcAirframers xrcAirframers) throws SQLException {
		return getXrcAirframerDao().fetch(xrcAirframers);
	}
	
	@Override
	public int save(XrcAirframers pXrcAirframers) throws SQLException {
		return getXrcAirframerDao().save(pXrcAirframers);
	}
	
	@Override
	public ArrayList<XrcAirframers> getAfData (XrcAirframers pXrcAirframers) throws SQLException {
		return getXrcAirframerDao().getAfData(pXrcAirframers);
	}
	
	@Override
	public int getAcForAf(XrcAcData xrcAcData) throws SQLException {
		return getXrcAirframerDao().getAcForAf(xrcAcData);
	}
	
	@Override
	public int update(XrcAirframers pXrcAirframers) throws SQLException {
		return getXrcAirframerDao().update(pXrcAirframers);
	}
	
	@Override
	public ArrayList<KeyValueBean> getXrcAirframersList (XrcAirframers pXrcAirframers) throws SQLException {
		return getXrcAirframerDao().getXrcAirframersList(pXrcAirframers);
	}
}
