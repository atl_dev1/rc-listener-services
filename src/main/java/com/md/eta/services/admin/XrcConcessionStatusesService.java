package com.md.eta.services.admin;

import java.sql.SQLException;
import java.util.ArrayList;

import com.md.eta.model.admin.XrcConcessionStatuses;

public interface XrcConcessionStatusesService {
	
	public ArrayList<XrcConcessionStatuses> fetch (XrcConcessionStatuses pXrcConcessionStatuses) throws SQLException;
	
	public abstract int save(XrcConcessionStatuses pXrcConcessionStatuses) throws SQLException;
	
	public abstract int update(XrcConcessionStatuses pXrcConcessionStatuses) throws SQLException;
	
	Long getCount(XrcConcessionStatuses xrcConcessionStatuses) throws SQLException;

	public ArrayList<XrcConcessionStatuses> getStatuses(XrcConcessionStatuses pXrcConcessionStatuses) throws SQLException;
	
	public int delete(XrcConcessionStatuses concessionStatus) throws SQLException;
	
	public int isDeleteCheck(XrcConcessionStatuses concessionStatus) throws SQLException;
	
	
	

}
