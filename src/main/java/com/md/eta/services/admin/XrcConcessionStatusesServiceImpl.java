package com.md.eta.services.admin;

import java.sql.SQLException;
import java.util.ArrayList;

import com.md.eta.model.admin.XrcConcessionStatuses;
import com.md.eta.services.AbstractService;

public class XrcConcessionStatusesServiceImpl extends AbstractService implements
		XrcConcessionStatusesService {

	@Override
	public ArrayList<XrcConcessionStatuses> fetch(
			XrcConcessionStatuses pXrcConcessionStatuses) throws SQLException {
		// TODO Auto-generated method stub
		return getXrcConcessionStatusesDao().fetch(pXrcConcessionStatuses);
	}
	
	@Override
	public int save(XrcConcessionStatuses pXrcConcessionStatuses) throws SQLException {
		return getXrcConcessionStatusesDao().save(pXrcConcessionStatuses);
	}
	
	@Override
	public int update(XrcConcessionStatuses pXrcConcessionStatuses)throws SQLException {
		return getXrcConcessionStatusesDao().update(pXrcConcessionStatuses);
		
	}
	
	@Override
	public Long getCount(XrcConcessionStatuses xrcConcessionStatuses)throws SQLException {
		return getXrcConcessionStatusesDao().getCount(xrcConcessionStatuses); 
	}
	
	@Override
	public ArrayList<XrcConcessionStatuses> getStatuses(XrcConcessionStatuses pXrcConcessionStatuses) throws SQLException {
		return getXrcConcessionStatusesDao().getStatuses(pXrcConcessionStatuses);
	}
	
	@Override
	public int delete(XrcConcessionStatuses concessionStatus) throws SQLException {
		return getXrcConcessionStatusesDao().delete(concessionStatus);
	}
	
	@Override
	public int isDeleteCheck(XrcConcessionStatuses concessionStatus) throws SQLException {
		return getXrcConcessionStatusesDao().isDeleteCheck(concessionStatus);
	}
	

}
