package com.md.eta.services.admin;

import java.sql.SQLException;
import java.util.ArrayList;

import com.md.eta.model.admin.XrcAcData;
import com.md.eta.model.concessions.XrcDamageTypes;



public interface XrcDamageTypesService {
	
	public ArrayList<XrcDamageTypes> fetch(XrcDamageTypes xrcDamageTypes) throws SQLException;
	
	public int update(XrcDamageTypes xrcDamageTypes)  throws SQLException;
    
	public  int  save(XrcDamageTypes xrcDamageTypes)  throws SQLException;

	public  Long getCount(XrcDamageTypes xrcDamageTypes)  throws SQLException;
	
	public ArrayList<XrcDamageTypes> getDamageTypes(XrcDamageTypes xrcDamageTypes) throws SQLException;

}
