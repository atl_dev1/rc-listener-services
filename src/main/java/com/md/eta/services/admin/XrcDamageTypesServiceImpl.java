package com.md.eta.services.admin;

import java.sql.SQLException;
import java.util.ArrayList;

import com.md.eta.model.concessions.XrcDamageTypes;
import com.md.eta.services.AbstractService;

public class XrcDamageTypesServiceImpl extends AbstractService implements XrcDamageTypesService {

	@Override
	public ArrayList<XrcDamageTypes> fetch(XrcDamageTypes xrcDamageTypes)
			throws SQLException {
		return getXrcDamageTypesDao().fetch(xrcDamageTypes);
	}

	@Override
	public int update(XrcDamageTypes xrcDamageTypes) throws SQLException {
		return getXrcDamageTypesDao().update(xrcDamageTypes);
	}

	@Override
	public int save(XrcDamageTypes xrcDamageTypes) throws SQLException {
		return getXrcDamageTypesDao().save(xrcDamageTypes);
	}

	@Override
	public Long getCount(XrcDamageTypes xrcDamageTypes) throws SQLException {
		return getXrcDamageTypesDao().getCount(xrcDamageTypes);
	}

	@Override
	public ArrayList<XrcDamageTypes> getDamageTypes(
			XrcDamageTypes xrcDamageTypes) throws SQLException {
		return getXrcDamageTypesDao().getDamageTypes(xrcDamageTypes);
	}
	
}
