package com.md.eta.services.admin;

import java.sql.SQLException;
import java.util.ArrayList;

import com.md.eta.model.concessions.XrcDepartmentLocations;

public interface XrcDepartmentLocationsService {

	Long getCount(XrcDepartmentLocations pXrcDepartmentLocations) throws SQLException;

	ArrayList<XrcDepartmentLocations> fetch(XrcDepartmentLocations pXrcDepartmentLocations) throws SQLException;

	int save(XrcDepartmentLocations pXrcDepartmentLocations) throws SQLException;
	
	public ArrayList<XrcDepartmentLocations> getDeptLocation(XrcDepartmentLocations pXrcDepartmentLocations) throws SQLException;

	int update(XrcDepartmentLocations pXrcDepartmentLocations) throws SQLException;

}
