package com.md.eta.services.admin;

import java.sql.SQLException;
import java.util.ArrayList;

import com.md.eta.model.concessions.XrcDepartmentLocations;
import com.md.eta.services.AbstractService;

public class XrcDepartmentLocationsServiceImpl extends AbstractService implements XrcDepartmentLocationsService {
	
	@Override
	public  Long getCount(XrcDepartmentLocations pXrcDepartmentLocations)  throws SQLException{
		return getXrcDepartmentLocationsDao().getCount(pXrcDepartmentLocations);
	}

	@Override
	public  ArrayList<XrcDepartmentLocations> fetch(XrcDepartmentLocations pXrcDepartmentLocations) throws SQLException {
		return getXrcDepartmentLocationsDao().fetch(pXrcDepartmentLocations);
	}
	
	@Override
	public int save(XrcDepartmentLocations pXrcDepartmentLocations) throws SQLException {
		return getXrcDepartmentLocationsDao().save(pXrcDepartmentLocations);
	}
	
	@Override
	public int update (XrcDepartmentLocations pXrcDepartmentLocations) throws SQLException {
		return getXrcDepartmentLocationsDao().update(pXrcDepartmentLocations);
	}

	@Override
	public ArrayList<XrcDepartmentLocations> getDeptLocation(
			XrcDepartmentLocations pXrcDepartmentLocations) throws SQLException {
		return getXrcDepartmentLocationsDao().getDeptLocation(pXrcDepartmentLocations);
	}

}
