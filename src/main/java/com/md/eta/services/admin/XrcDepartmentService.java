package com.md.eta.services.admin;

import java.sql.SQLException;
import java.util.ArrayList;

import com.md.eta.model.concessions.XrcDepartments;
import com.md.eta.model.iportal.users.XiaSpUser;

public interface XrcDepartmentService {
	
	public  Long getCount(XrcDepartments xrcDepartments)  throws SQLException;
	
	public  ArrayList<XrcDepartments> fetch(XrcDepartments xrcDepartments) throws SQLException;
	
	public int save(XrcDepartments pXrcDepartments) throws SQLException;
	
	public ArrayList<XrcDepartments> getDeptData(XrcDepartments pXrcDepartments) throws SQLException;
	
	public int update (XrcDepartments pXrcDepartments) throws SQLException;
	
//	ArrayList<XiaSpUser> fetchAllocatedUsr(XrcDepartments xrcDepartments) throws SQLException;
	
//	public Long getAllocateUserCount(XrcDepartments xrcDepartments) throws SQLException;
	
	ArrayList<XiaSpUser> fetchUnAllocatedUsr(XrcDepartments xrcDepartments) throws SQLException;

	ArrayList<XiaSpUser> fetchAllocatedUsr(XrcDepartments xrcDepartments,
			XiaSpUser user) throws SQLException;

	Long getAllocateUserCount(XrcDepartments xrcDepartments, XiaSpUser user)
			throws SQLException;
	
}
