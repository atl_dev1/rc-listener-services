package com.md.eta.services.admin;

import java.sql.SQLException;
import java.util.ArrayList;

import com.md.eta.model.concessions.XrcDepartments;
import com.md.eta.model.iportal.users.XiaSpUser;
import com.md.eta.services.AbstractService;

public class XrcDepartmentServiceImpl extends AbstractService implements XrcDepartmentService {
	
	public  Long getCount(XrcDepartments xrcDepartments)  throws SQLException{
		return getXrcDepartmentDao().getCount(xrcDepartments);
	}

	@Override
	public  ArrayList<XrcDepartments> fetch(XrcDepartments xrcDepartments) throws SQLException {
		return getXrcDepartmentDao().fetch(xrcDepartments);
	}
	
	@Override
	public int save(XrcDepartments pXrcDepartments) throws SQLException {
		return getXrcDepartmentDao().save(pXrcDepartments);
	}
	
	@Override
	public ArrayList<XrcDepartments> getDeptData(XrcDepartments pXrcDepartments) throws SQLException {
		return getXrcDepartmentDao().getDeptData(pXrcDepartments);
	}
	
	@Override
	public int update (XrcDepartments pXrcDepartments) throws SQLException {
		return getXrcDepartmentDao().update(pXrcDepartments);
	}

	@Override
	public ArrayList<XiaSpUser> fetchAllocatedUsr(XrcDepartments xrcDepartments, XiaSpUser user) throws SQLException {
		return getXrcDepartmentDao().fetchAllocatedUsr(xrcDepartments, user);
	}
	
	@Override
	public Long getAllocateUserCount(XrcDepartments xrcDepartments, XiaSpUser user) throws SQLException {
		return getXrcDepartmentDao().getAllocateUserCount(xrcDepartments, user);
	}
	
	public ArrayList<XiaSpUser> fetchUnAllocatedUsr(XrcDepartments xrcDepartments) throws SQLException{
		return getXrcDepartmentDao().fetchUnAllocatedUsr(xrcDepartments);
	}
}
