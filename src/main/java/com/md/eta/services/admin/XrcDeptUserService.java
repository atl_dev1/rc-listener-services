package com.md.eta.services.admin;

import java.sql.SQLException;
import java.util.ArrayList;

import com.md.eta.model.concessions.XrcDepartments;
import com.md.eta.model.iportal.users.XiaSpUser;
import com.md.eta.model.util.KeyValueBean;

public interface XrcDeptUserService {

	public ArrayList<KeyValueBean> getAllocatedUsers(Long deptId) throws SQLException;
	
	public ArrayList<KeyValueBean> getUnallocatedUsers(Long deptId) throws SQLException;
	
	public Integer userExists(Long deptId,Long userId) throws SQLException;
	
	public Integer allocateUser(Long deptId,Long userId) throws SQLException;
	
	public int deleteUser(Long deptId,Long userId) throws SQLException;
	
	public ArrayList<XiaSpUser> existUserList(Long deptId) throws SQLException;
	
}
