package com.md.eta.services.admin;

import java.sql.SQLException;
import java.util.ArrayList;

import com.md.eta.model.concessions.XrcDepartments;
import com.md.eta.model.iportal.users.XiaSpUser;
import com.md.eta.model.util.KeyValueBean;
import com.md.eta.services.AbstractService;

public class XrcDeptUserServiceImpl extends AbstractService implements XrcDeptUserService {

	@Override
	public ArrayList<KeyValueBean> getAllocatedUsers(Long deptId)
			throws SQLException {
		return getXrcDeptUserDao().getAllocatedUsers(deptId);
	}

	@Override
	public ArrayList<KeyValueBean> getUnallocatedUsers(Long deptId)
			throws SQLException {
		return getXrcDeptUserDao().getUnallocatedUsers(deptId);
	}

	@Override
	public Integer userExists(Long deptId, Long userId) throws SQLException {
		return getXrcDeptUserDao().userExists(deptId, userId);
	}

	@Override
	public Integer allocateUser(Long deptId, Long userId) throws SQLException {
		return getXrcDeptUserDao().allocateUser(deptId, userId);
	}

	@Override
	public int deleteUser(Long deptId, Long userId) throws SQLException {
		return getXrcDeptUserDao().deleteUser(deptId, userId);
	}

	@Override
	public ArrayList<XiaSpUser> existUserList(Long deptId) throws SQLException {
		return getXrcDeptUserDao().existUserList(deptId);
	}
	
	
}
