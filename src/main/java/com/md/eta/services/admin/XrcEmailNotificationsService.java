package com.md.eta.services.admin;

import java.sql.SQLException;
import java.util.ArrayList;

import com.md.eta.model.concessions.XrcEmailNotifications;

public interface XrcEmailNotificationsService {

	Long getCount(XrcEmailNotifications pXrcEmailNotifications)
			throws SQLException;

	ArrayList<XrcEmailNotifications> fetch(
			XrcEmailNotifications pXrcEmailNotifications) throws SQLException;

	int save(XrcEmailNotifications pXrcEmailNotifications) throws SQLException;

	int update(XrcEmailNotifications pXrcEmailNotifications)
			throws SQLException;
	
	ArrayList<XrcEmailNotifications> getEmailData(XrcEmailNotifications pXrcEmailNotifications) throws SQLException;
}
