package com.md.eta.services.admin;

import java.sql.SQLException;
import java.util.ArrayList;

import com.md.eta.model.concessions.XrcEmailNotifications;
import com.md.eta.services.AbstractService;

public class XrcEmailNotificationsServiceImpl extends AbstractService implements XrcEmailNotificationsService {

	@Override
	public  Long getCount(XrcEmailNotifications pXrcEmailNotifications)  throws SQLException{
		return getXrcEmailNotificationsDao().getCount(pXrcEmailNotifications);
	}

	@Override
	public  ArrayList<XrcEmailNotifications> fetch(XrcEmailNotifications pXrcEmailNotifications) throws SQLException {
		return getXrcEmailNotificationsDao().fetch(pXrcEmailNotifications);
	}
	
	@Override
	public int save(XrcEmailNotifications pXrcEmailNotifications) throws SQLException {
		return getXrcEmailNotificationsDao().save(pXrcEmailNotifications);
	}
	
	@Override
	public int update (XrcEmailNotifications pXrcEmailNotifications) throws SQLException {
		return getXrcEmailNotificationsDao().update(pXrcEmailNotifications);
	}
	
	public ArrayList<XrcEmailNotifications> getEmailData(XrcEmailNotifications pXrcEmailNotifications) throws SQLException{
		return getXrcEmailNotificationsDao().getEmailData(pXrcEmailNotifications);
	}
}
