package com.md.eta.services.admin;

import java.sql.SQLException;
import java.util.ArrayList;

import com.md.eta.model.admin.XrcEventDefinitions;
import com.md.eta.model.concessions.XrcEmailNotifications;
import com.md.eta.model.concessions.XrcEvtNotificationDelegates;
import com.md.eta.model.util.KeyValueBean;

public interface XrcEvtNotificationDelegatesService {

	Long getCount(XrcEvtNotificationDelegates pXrcEvtNotificationDelegates)
			throws SQLException;

	ArrayList<XrcEvtNotificationDelegates> fetch(
			XrcEvtNotificationDelegates pXrcEvtNotificationDelegates)
			throws SQLException;

	int save(XrcEvtNotificationDelegates pXrcEvtNotificationDelegates)
			throws SQLException;

	int update(XrcEvtNotificationDelegates pXrcEvtNotificationDelegates)
			throws SQLException;
	
	ArrayList<KeyValueBean> getXrcEventsList (XrcEventDefinitions pXrcEventDefinitions) throws SQLException;
	
	ArrayList<XrcEvtNotificationDelegates> getDelegatesData(XrcEvtNotificationDelegates pXrcEvtNotificationDelegates) throws SQLException;
}
