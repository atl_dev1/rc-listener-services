package com.md.eta.services.admin;

import java.sql.SQLException;
import java.util.ArrayList;

import com.md.eta.model.admin.XrcEventDefinitions;
import com.md.eta.model.concessions.XrcEvtNotificationDelegates;
import com.md.eta.model.util.KeyValueBean;
import com.md.eta.services.AbstractService;

public class XrcEvtNotificationDelegatesServiceImpl extends AbstractService implements XrcEvtNotificationDelegatesService{

	@Override
	public  Long getCount(XrcEvtNotificationDelegates pXrcEvtNotificationDelegates)  throws SQLException{
		return getXrcEvtNotificationDelegatesDao().getCount(pXrcEvtNotificationDelegates);
	}

	@Override
	public  ArrayList<XrcEvtNotificationDelegates> fetch(XrcEvtNotificationDelegates pXrcEvtNotificationDelegates) throws SQLException {
		return getXrcEvtNotificationDelegatesDao().fetch(pXrcEvtNotificationDelegates);
	}
	
	@Override
	public int save(XrcEvtNotificationDelegates pXrcEvtNotificationDelegates) throws SQLException {
		return getXrcEvtNotificationDelegatesDao().save(pXrcEvtNotificationDelegates);
	}
	
	@Override
	public int update (XrcEvtNotificationDelegates pXrcEvtNotificationDelegates) throws SQLException {
		return getXrcEvtNotificationDelegatesDao().update(pXrcEvtNotificationDelegates);
	}
	
	@Override
	public ArrayList<KeyValueBean> getXrcEventsList (XrcEventDefinitions pXrcEventDefinitions) throws SQLException{
		return getXrcEvtNotificationDelegatesDao().getXrcEventsList(pXrcEventDefinitions);
	}
	
	@Override
	public ArrayList<XrcEvtNotificationDelegates> getDelegatesData(XrcEvtNotificationDelegates pXrcEvtNotificationDelegates) throws SQLException{
		return getXrcEvtNotificationDelegatesDao().getDelegatesData(pXrcEvtNotificationDelegates);
	}
}
