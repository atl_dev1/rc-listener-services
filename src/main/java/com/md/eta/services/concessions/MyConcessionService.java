package com.md.eta.services.concessions;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.md.eta.model.admin.XrcAcData;
import com.md.eta.model.admin.XrcEventDefinitions;
import com.md.eta.model.concessions.ACType;
import com.md.eta.model.concessions.CMM;
import com.md.eta.model.concessions.ConcessionClassAttributes;
import com.md.eta.model.concessions.ConcessionDashboardChart;
import com.md.eta.model.concessions.ConcessionDashboardOpenTable;
import com.md.eta.model.concessions.ConcessionsAircraftTypes;
import com.md.eta.model.concessions.ConcessionsCustomers;
import com.md.eta.model.concessions.ConcessionsIportalSearchVW;
import com.md.eta.model.concessions.ConcessionsIssueTypes;
import com.md.eta.model.concessions.ConcessionsPriority;
import com.md.eta.model.concessions.CustomerConcessions;
import com.md.eta.model.concessions.DialogDocuments;
import com.md.eta.model.concessions.DocumentPack;
import com.md.eta.model.concessions.EmailSentEntry;
import com.md.eta.model.concessions.GearType;
import com.md.eta.model.concessions.OnbaseHistoricDocument;
import com.md.eta.model.concessions.Operator;
import com.md.eta.model.concessions.XrcAircraftGroup;
import com.md.eta.model.concessions.XrcBaseMaterials;
import com.md.eta.model.concessions.XrcClassifications;
import com.md.eta.model.concessions.XrcConcessionStatuses;
import com.md.eta.model.concessions.XrcDamageTypes;
import com.md.eta.model.concessions.XrcDepartments;
import com.md.eta.model.concessions.XrcDiaryEntries;
import com.md.eta.model.concessions.XrcDocumentTypes;
import com.md.eta.model.concessions.XrcDocuments;
import com.md.eta.model.concessions.XrcEmailRecipientsView;
import com.md.eta.model.concessions.XrcEngClassAttribHistory;
import com.md.eta.model.concessions.XrcEngClassAttributes;
import com.md.eta.model.concessions.XrcEngineeringAttributes;
import com.md.eta.model.concessions.XrcMaterialGrades;
import com.md.eta.model.concessions.XrcMaterialSpecifications;
import com.md.eta.model.concessions.XrcMaterials;
import com.md.eta.model.concessions.XrcPart;
import com.md.eta.model.concessions.XrcPriorities;
import com.md.eta.model.concessions.XrcRepairCategories;
import com.md.eta.model.concessions.XrcServiceOffering;
import com.md.eta.model.concessions.XrcSites;
import com.md.eta.model.concessions.XrcValidationDecisions;
import com.md.eta.model.concessions.XrcWysiwygContent;
import com.md.eta.model.concessions.XrcWysiwygContentTypes;
import com.md.eta.model.concessions.XxmSpConcessions;
import com.md.eta.model.iportal.users.XiaSpUser;
import com.md.eta.model.iportal.users.XrcConcessionRoles;
import com.md.eta.model.portal.users.XxmSpUsers;
import com.md.eta.model.util.KeyValueBean;

public interface MyConcessionService {

	public ArrayList<ACType> getACTypes(String name) throws Exception;
	

	public Integer delete(Long concId) throws Exception;

	public ArrayList<CMM> getCMMList(String name) throws Exception;

	public ArrayList<ACType> getAircraftTypes(String name, Integer usrId) throws Exception;

	public ArrayList<ACType> getHCAircraftTypes(String name) throws SQLException;

	public ACType getACType(Integer name) throws Exception;
	
	public ArrayList<ACType> getAircraftTypes(String name) throws Exception;

	public ArrayList<GearType> getGearTypes(String name, Integer acId) throws Exception;

	public ArrayList<ACType> getAircraftTypes(Integer fnId, String name, Integer usrId) throws Exception;

	public ArrayList<Operator> getOperators(String name) throws Exception;
	
	public ArrayList<ConcessionsAircraftTypes> getConcessionAcTypes(String name) throws Exception;
	
	public ArrayList<ConcessionsCustomers> getConcessionsCustomers(String name) throws SQLException;
	
	public Long getPreassessCount(XxmSpConcessions pXxmSpConcessions, int userId)throws SQLException;

	public Long getCount(XxmSpConcessions pXxmSpConcessions,String isSearchConc) throws SQLException;
	
	public Long getHCCount(ConcessionsIportalSearchVW pConcessionsIportalSearchVw , int userId) throws SQLException;

	public ArrayList<XrcPart> getParts(String name) throws Exception;

	public ArrayList<XiaSpUser> getConcessionUsers(XiaSpUser user, String screen) throws Exception;
	
	// code for preassessment - karthick
	
	public ArrayList<XxmSpConcessions> fetchSubmittedConcessions(XxmSpConcessions pXxmSpConcessions, int userId) throws SQLException;

	public ArrayList<XxmSpConcessions> findConcessions(XxmSpConcessions conc,String isSearchConc) throws SQLException;
	
	public ArrayList<ConcessionsIportalSearchVW> fetch (ConcessionsIportalSearchVW pConcessionsIportalSearchVw, int userId) throws SQLException;
	
	public ArrayList<XxmSpConcessions> findEditConcessions(XxmSpConcessions bean) throws SQLException;

	public int getConcessionsCount(XxmSpConcessions conc) throws SQLException;
	
	public ArrayList<XrcValidationDecisions> fetch (XrcValidationDecisions pXrcValidationDecisions) throws SQLException;
	
	public ArrayList<ConcessionDashboardOpenTable> fetchDashboardContent(ConcessionDashboardOpenTable concessionDashboard) throws SQLException;
	
	public ArrayList<ConcessionDashboardOpenTable> fetchDashboardRepairStatus(ConcessionDashboardOpenTable concessionDashboard) throws SQLException;
	
	public ArrayList<ConcessionDashboardOpenTable> fetchDashboardOverdueRepair(ConcessionDashboardOpenTable concessionDashboard) throws SQLException;
	
	@SuppressWarnings("rawtypes")
	public LinkedHashMap fetchFirstChart(ConcessionDashboardChart concessionDashboard) throws SQLException;
	
	@SuppressWarnings("rawtypes")
	public LinkedHashMap fetchSecondChart(ConcessionDashboardChart concessionDashboard) throws SQLException;
	
	@SuppressWarnings("rawtypes")
	public LinkedHashMap fetchThirdChart(ConcessionDashboardChart concessionDashboard) throws SQLException;
	
	@SuppressWarnings("rawtypes")
	public ArrayList fetchChart(ConcessionDashboardChart concessionDashboard) throws SQLException;
	/**
	 * @param conc
	 * @return
	 * @throws SQLException
	 */
	public int saveConcession(XxmSpConcessions conc) throws SQLException;

	/**
	 * @param bean
	 * @return
	 * @throws SQLException
	 */
	int createConcession(XxmSpConcessions bean,List<XrcDocuments> docList) throws SQLException;

	ArrayList<XrcConcessionStatuses> fetchStatuses(XrcConcessionStatuses pXrcConcessionStatuses) throws SQLException;
	
	public ArrayList<String> loadConcessionRoles(Integer userId) throws SQLException;
	
	ArrayList<XrcConcessionStatuses> fetchConcessionStatuses() throws SQLException;
	
	ArrayList<ConcessionsIssueTypes> fetchIssueTypes(ConcessionsIssueTypes pXrcIssueTypes) throws SQLException;
	
	public ArrayList<XrcPriorities> fetchPriority (XrcPriorities pXrcPriorities) throws SQLException;
	
	public ArrayList<ConcessionsPriority> fetchConcessionsPriority (ConcessionsPriority pPriority) throws SQLException;
	
	public ArrayList<XrcSites> fetchSites (XrcSites pXrcSites) throws SQLException;
	
	ArrayList<XrcServiceOffering> fetchServiceOfferings(XrcServiceOffering xrcServiceOffering) throws SQLException;

	/**
	 * @param doname
	 * @return
	 */
	public int deleteDocument(Integer doId) throws SQLException;

	/**
	 * @param doname
	 */
	public Double getUsdPrice(Integer itemId) throws SQLException;

	ArrayList<XrcPart> getSolarParts(String partnumber) throws Exception;

	ArrayList<XrcPart> getRCParts(String partnumber) throws Exception;

	public String getIsMilitary(int fnId, int usrId) throws Exception;

	ArrayList<XrcDiaryEntries> getDiaryEntries(Long concId) throws Exception;

	public ArrayList<XrcEventDefinitions> getAvailableEvents(Long concId, Integer usrId,String standardFlag);
	
	public ArrayList<XrcEventDefinitions> getAvailableEventsPre(Long concId, Integer usrId,String standardFlag);

	public abstract void updateFinalRCDocRequested(Long concId, int docId, int usrId) throws SQLException;

	public abstract void createFinalRCDocRequested(Long concId, int docId, int usrId) throws SQLException;

	public abstract boolean isFinalRCDocRequested(Long concId, int docId,int usrId) throws SQLException;

	public Long save(XrcWysiwygContent pXrcWysiwygContent) throws SQLException;
	
	public Long save(XrcEngineeringAttributes pXrcEngineeringAttributes) throws SQLException;
	
	public int update(XrcEngineeringAttributes pXrcEngineeringAttributes) throws SQLException;
	
	public int update (XrcWysiwygContent pXrcWysiwygContent) throws SQLException;
		
	public ArrayList<XrcWysiwygContentTypes> fetch (XrcWysiwygContentTypes pXrcWysiwygContentTypes) throws SQLException;
	
	public ArrayList<XrcDocumentTypes> fetchDocxTypes () throws SQLException;
	
	public ArrayList<XrcWysiwygContent> fetch (XrcWysiwygContent pXrcWysiwygContent) throws SQLException;
	
	public ArrayList<XrcWysiwygContent> fetchView (XrcWysiwygContent pXrcWysiwygContent) throws SQLException;
	
//	public ArrayList<XrcWysiwygContent> fetchContent (Long concId,int eventId,int contentTypeId) throws SQLException;
	public ArrayList<XrcWysiwygContent> fetchContent (Long concId,String contentTypesCode) throws SQLException;

	public ArrayList<XrcEngineeringAttributes> fetch (XrcEngineeringAttributes pXrcEngineeringAttributes) throws SQLException;

	public ArrayList<KeyValueBean> fetch (XrcMaterialGrades pXrcMaterialGrades) throws SQLException;

	public ArrayList<XrcMaterials> fetch (XrcMaterials pXrcMaterials) throws SQLException;

	public ArrayList<KeyValueBean> fetch (XrcBaseMaterials pXrcBaseMaterials) throws SQLException;

	public ArrayList<KeyValueBean> fetch (XrcClassifications pXrcClassifications) throws SQLException;

	public ArrayList<KeyValueBean> fetch (XrcRepairCategories pXrcRepairCategories) throws SQLException;

	public ArrayList<XrcDocuments> fetch (XrcDocuments pXrcDocuments) throws SQLException;
	
	public ArrayList<XrcWysiwygContent> fetchData(Long concId) throws SQLException;

	public XrcDocuments save(XrcDocuments pXrcDocuments, boolean external) throws SQLException;
	
	public Long saveDiaryEntry(XrcDiaryEntries bean) throws SQLException;
	
	public ArrayList<XrcDepartments> fetchGroup(XrcDepartments pXrcDepartments) throws SQLException;
	
	public ArrayList<XrcDepartments> fetchIndividual(XrcDepartments pXrcDepartments) throws SQLException;
	
	public ArrayList<XrcConcessionRoles> fetchRoles(int usrId) throws SQLException;
	
	public ArrayList<XxmSpConcessions> fetchConcessions (XxmSpConcessions pXxmSpConcessions) throws SQLException;
	
	public boolean isConcessionExists(Long concId) throws SQLException;
	
	public Long createDiaryEvent(XrcDiaryEntries xrcDiaryEntries) throws SQLException;
	
	public ArrayList<XrcDiaryEntries> fetchConcEvents(XxmSpConcessions pXxmSpConcessions,int userId) throws SQLException; 
	
	public ArrayList<XrcDocuments> getFinalRCDoc(Long concId) throws SQLException; 
	
	public String getEvaluationType(Long concId) throws SQLException;
	
	public String getAirframer(Long concId) throws SQLException;
	
	public XrcDocuments update(XrcDocuments pXrcDocuments) throws SQLException;
	
	public ArrayList<KeyValueBean> fetch (XrcDamageTypes pXrcDamageTypes) throws SQLException;
	
	public int update(XxmSpConcessions pXxmSpConcessions) throws SQLException;
	
	public ArrayList<KeyValueBean> fetch (XrcMaterialSpecifications pXrcMaterialSpecifications) throws SQLException;
	
	public ArrayList<XrcDiaryEntries> fetch (XrcDiaryEntries pXrcDiaryEntries) throws SQLException;

	public List<XrcDocuments> fetchPendingOnbaseDocuments() throws SQLException;
	
	public XrcDocuments fetchDocumentById( Integer id) throws SQLException;

	public XrcEventDefinitions getXrcEventDefinitions(int eventId) throws SQLException;

	public HashMap<String, String> getMailProperties(int eventId, long concId) throws SQLException;

	public ArrayList<XrcDiaryEntries> getMaxDiaryEntries(Long xrcConcessionId) throws SQLException;

	public XxmSpConcessions fetchConcessionById(Long concId) throws SQLException;
	
	public abstract void createPart(XxmSpConcessions sp) throws SQLException;

	public abstract void createParentPart(XxmSpConcessions sp) throws SQLException;

	public abstract void createOperator(XrcDiaryEntries diary, XxmSpConcessions sp)	throws SQLException;
	
	public Double getUSDPrice(Long custId, Long partId) throws SQLException;
	
	public abstract String getRCNumber(Integer acType, Integer gearType,String site) throws SQLException;

	public Boolean isBAECondition( Long concId ) throws SQLException;

	public String getDocumentTypeById(int docTypeId) throws SQLException;
	
	public XrcDocumentTypes getDocumentTypeByCode(String docTypeCode) throws SQLException;
	
	public XrcDiaryEntries getLatestEvent( Long concId, String eventName) throws SQLException;
	
//	public String chargeAndProcess(Long concId) throws SQLException;
	
	public int chargeAndProcess(Long concId) throws SQLException;

	String getConcIssueNo(Long concId) throws SQLException;
	
	ArrayList<DocumentPack> fetchDocumentPack(DocumentPack pTmpDocPackTbl)	throws SQLException;

	ArrayList<DocumentPack> fetchDialogDocuments(DialogDocuments pTmpDocPackTbl) throws SQLException;

	ArrayList<String> getTemplateNames(ArrayList<String> docTypes);
	
	public String getRTEValidationLevelCode(Long concId) throws SQLException;
	
	public String getUserRTEValidationLevelCode(Long concId,Integer userId) throws SQLException;
	
	public String getRTEValidationDate(Long concId) throws SQLException;
	
	public Integer getRTEValidationUser(Long concId) throws SQLException ;
	
	public ArrayList<XrcAircraftGroup> fetchAircraftGroup(XrcAircraftGroup pXrcAircraftGroup) throws SQLException;
	
	public boolean checkPreassessCondition(Long concId) throws SQLException;
	
	public List<XrcEmailRecipientsView> getMailRecipients(int eventId, long concId) throws SQLException;
	
	public HashMap<String, String> getMailProperties(XrcEmailRecipientsView xrcEmailRecipientsView) throws SQLException;
	
	public Integer getMaxEvent(Long concId) throws SQLException;
	
	public int saveEmailSentEntry(EmailSentEntry emailSentEntry) throws SQLException;
	
	public boolean checkATRAircraft(Long concId) throws SQLException;
	
	public XxmSpConcessions getConcession(Long concId) throws SQLException;
	
	public int rollBackEntries(Long concId,int eventId) throws SQLException;
	
	public String getConcRevision(Long concId,String revType) throws SQLException;
	
	public int getXrcEventDefinitionId(String strEventDesc);
	
	public ArrayList<XrcAcData> fetchJarFarsReq(Long concId) throws SQLException;
	
	
	public String getCSCEmail(Long concId) throws SQLException;
	
	String getSubClass(Long concId) throws SQLException;
	
	public XxmSpConcessions save(XxmSpConcessions cc) throws SQLException;
	
	public ArrayList<XrcDocuments> fetchViewDoc(XrcDocuments pXrcDocuments) throws SQLException;
	
	public ArrayList<XrcDocuments> fetchDocxDoc(XrcDocuments pXrcDocuments) throws SQLException;
	
	public String getUsrJobTitle(Integer usrIds) throws SQLException;
	
	public ArrayList<XxmSpUsers> fetchRteUsrPdfGen(Integer userId) throws SQLException;
	
	public ArrayList<XrcDocuments> fetchAttachDoc(XrcDocuments pXrcDocuments) throws SQLException;
	
	String getAcTypeDocGen(Long concId) throws SQLException;
	
	String getGearDocGen(Long concId) throws SQLException;
	
	String getIsNewWorkflow(Long concId) throws SQLException;
	
	public ArrayList<OnbaseHistoricDocument> fetchHistoricDocument(OnbaseHistoricDocument pOnbaseHistoricDocument) throws SQLException;
	
	public int saveDoc(XrcDocuments doc) throws SQLException;
	
	public Long getXrcRcId(Long xrcId) throws SQLException;
	
	public ArrayList<CustomerConcessions> fetchCustConcessions(CustomerConcessions pCustomerConcessions) throws SQLException;
	
	public int update(CustomerConcessions pCustomerConcessions) throws SQLException;
	
	public String getXxmSpUserName(Integer usrId) throws SQLException;
	
	public int processConcession(XxmSpConcessions bean,XrcDiaryEntries entry) throws SQLException;
	
	public void saveFile(XxmSpConcessions concession); 
	
	public ArrayList<XrcSites> fetchHisSites (XrcSites pXrcSites) throws SQLException;
	
	public String fetchUsrMail(Integer userId) throws SQLException;
	
	public ArrayList<ConcessionClassAttributes> fetch(Long concId) throws SQLException;
	
	public int[] save(ArrayList<XrcEngClassAttributes> pXrcEngClassAttributes);
	
	public int[] update (ArrayList<XrcEngClassAttributes> pXrcEngClassAttributes) throws SQLException;
	
	public int update (XrcEngClassAttributes pXrcEngClassAttributes) throws SQLException;
	
	public ArrayList<XrcEngClassAttributes> fetch (XrcEngClassAttributes pXrcEngClassAttributes) throws SQLException;
	
	public int refreshClassAttr(Long concId) throws SQLException;
	
	public int[] saveHistory(ArrayList<XrcEngClassAttributes> pXrcEngClassAttributes,Long engHistoryId);
	
	public Long saveHistory(XrcEngineeringAttributes pXrcEngineeringAttributes) throws SQLException;
	
	public Integer isCustomerMapped(Integer id) throws SQLException;
	
	public Integer isPartMapped(Integer partId) throws SQLException;
	
	public ArrayList<CMM> getXrcCmmList(Integer cmmId) throws SQLException;


	public XiaSpUser getUser(Integer usrId) throws SQLException;


	public Map getEmailSmsMap(Integer usrId) throws SQLException;
	
	public List<XrcDocuments> fetchFailedOnbaseDmzDocs() throws SQLException;
}
