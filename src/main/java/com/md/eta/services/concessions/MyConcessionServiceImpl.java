package com.md.eta.services.concessions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.annotation.Transactional;

import com.md.eta.model.admin.XrcAcData;
import com.md.eta.model.admin.XrcEventDefinitions;
import com.md.eta.model.concessions.ACType;
import com.md.eta.model.concessions.CMM;
import com.md.eta.model.concessions.ConcessionClassAttributes;
import com.md.eta.model.concessions.ConcessionDashboardChart;
import com.md.eta.model.concessions.ConcessionDashboardOpenTable;
import com.md.eta.model.concessions.ConcessionsAircraftTypes;
import com.md.eta.model.concessions.ConcessionsCustomers;
import com.md.eta.model.concessions.ConcessionsIportalSearchVW;
import com.md.eta.model.concessions.ConcessionsIssueTypes;
import com.md.eta.model.concessions.ConcessionsPriority;
import com.md.eta.model.concessions.CustomerConcessions;
import com.md.eta.model.concessions.DialogDocuments;
import com.md.eta.model.concessions.DocumentPack;
import com.md.eta.model.concessions.EmailSentEntry;
import com.md.eta.model.concessions.GearType;
import com.md.eta.model.concessions.OnbaseHistoricDocument;
import com.md.eta.model.concessions.Operator;
import com.md.eta.model.concessions.XrcAircraftGroup;
import com.md.eta.model.concessions.XrcBaseMaterials;
import com.md.eta.model.concessions.XrcClassifications;
import com.md.eta.model.concessions.XrcConcessionStatuses;
import com.md.eta.model.concessions.XrcDamageTypes;
import com.md.eta.model.concessions.XrcDepartments;
import com.md.eta.model.concessions.XrcDiaryEntries;
import com.md.eta.model.concessions.XrcDocumentTypes;
import com.md.eta.model.concessions.XrcDocuments;
import com.md.eta.model.concessions.XrcEmailRecipientsView;
import com.md.eta.model.concessions.XrcEngClassAttribHistory;
import com.md.eta.model.concessions.XrcEngClassAttributes;
import com.md.eta.model.concessions.XrcEngineeringAttributes;
import com.md.eta.model.concessions.XrcMaterialGrades;
import com.md.eta.model.concessions.XrcMaterialSpecifications;
import com.md.eta.model.concessions.XrcMaterials;
import com.md.eta.model.concessions.XrcPart;
import com.md.eta.model.concessions.XrcPriorities;
import com.md.eta.model.concessions.XrcRepairCategories;
import com.md.eta.model.concessions.XrcServiceOffering;
import com.md.eta.model.concessions.XrcSites;
import com.md.eta.model.concessions.XrcValidationDecisions;
import com.md.eta.model.concessions.XrcWysiwygContent;
import com.md.eta.model.concessions.XrcWysiwygContentTypes;
import com.md.eta.model.concessions.XxmSpConcessions;
import com.md.eta.model.concessions.XxmSpDocRequests;
import com.md.eta.model.iportal.users.XiaSpUser;
import com.md.eta.model.iportal.users.XrcConcessionRoles;
import com.md.eta.model.portal.users.XxmSpUsers;
import com.md.eta.model.util.KeyValueBean;
import com.md.eta.model.util.SpicewebConstants;
import com.md.eta.services.AbstractService;

public class MyConcessionServiceImpl extends AbstractService implements
		MyConcessionService {

	Logger logger = Logger.getLogger(MyConcessionServiceImpl.class);
	String watchDirPath;
	String inSubmitFolder;
	String concessionsDirPath;
	private String inDraftFolder;

	public ArrayList<ACType> getACTypes(String name) throws SQLException {
		ArrayList<ACType> list1 = getMyConcessionDao().getAircraftTypes(name);
		return list1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.md.spiceweb.services.ConcessionsService#getACType(java.lang.Integer)
	 */

	public ACType getACType(Integer Id) throws Exception {

		ArrayList<ACType> list1 = getMyConcessionDao().getAircraftType(Id);

		/*
		 * for (ACType act : list1){ System.out.println(act); }
		 */
		return list1.get(0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.md.spiceweb.services.ConcessionsService#getOperators(java.lang.String
	 * )
	 */

	public ArrayList<Operator> getOperators(String name) throws Exception {
		ArrayList<Operator> list2 = getMyConcessionDao().getOperators(name);

		/*
		 * for (Operator act : list2){ System.out.println(act); }
		 */
		return list2;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.md.spiceweb.services.ConcessionsService#getParts(java.lang.String)
	 */

	public ArrayList<XrcPart> getParts(String name) throws Exception {
		ArrayList<XrcPart> list2 = getMyConcessionDao().getParts(name);
		return list2;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.md.spiceweb.services.ConcessionsService#getConcessionUsers(int)
	 */

	public ArrayList<XiaSpUser> getConcessionUsers(XiaSpUser user, String screen)
			throws Exception {
		ArrayList<XiaSpUser> list2 = getMyConcessionDao().getConcessionUsers(
				user, screen);
		return list2;
	}

	public ArrayList<ConcessionsAircraftTypes> getConcessionAcTypes(String name)
			throws Exception {
		ArrayList<ConcessionsAircraftTypes> list1 = getMyConcessionDao()
				.getConcessionsAircraftTypes(name);
		return list1;
	}

	public ArrayList<ConcessionsCustomers> getConcessionsCustomers(String name)
			throws SQLException {
		ArrayList<ConcessionsCustomers> list1 = getMyConcessionDao()
				.getConcessionsCustomers(name);
		return list1;
	}

	@Transactional
	public int createConcession(XxmSpConcessions bean,
			List<XrcDocuments> docList) throws SQLException {
		XxmSpConcessions pXxmSpConcessions = bean;
		logger.debug("Concession created for concID : " + bean.getConcId());
		String site = getMyConcessionDao().getRCSite(bean);
		pXxmSpConcessions.setSite(site);
		int retVal = getMyConcessionDao().save(pXxmSpConcessions);
		// first insert the Draft Created Event followed by Submission
		ArrayList<Integer> eventIdList = new ArrayList<Integer>();
		int eventId = XrcEventDefinitions.DRAFT_CREATED;
		XrcDiaryEntries entry = new XrcDiaryEntries();
		entry.setXrcConcessionId(bean.getConcId());
		entry.setXrcDiaryEntryDate(bean.getCreatedDate());
		entry.setXrcEventId(eventId); // for Draft
		entry.setXrcSender(bean.getUsrId());
		eventIdList.add(eventId);
		createDiaryEvent(entry);

		/*
		 * eventId = XrcEventDefinitions.CUSTOMER_SUBMIT_RC;
		 * entry.setXrcEventId(eventId); // for submission
		 * createDiaryEvent(entry);
		 */
		logger.debug("Draft created for concID : " + entry.getXrcConcessionId());
		boolean newBaseData = false;
		// check if new parts then change the event to added new base data.
		/*
		 * if (pXxmSpConcessions.getNewPartNo() != null ||
		 * pXxmSpConcessions.getNewParentPart() != null ||
		 * pXxmSpConcessions.getNewOperatorName() != null) { newBaseData = true;
		 * }
		 */

		entry = new XrcDiaryEntries();
		eventId = XrcEventDefinitions.CUSTOMER_SUBMIT_RC;
		entry.setXrcDiaryEntry("System Generated Event");
		entry.setXrcConcessionId(bean.getConcId());
		entry.setXrcDiaryEntryDate(bean.getSubmittedDate());
		entry.setXrcEventId(eventId); // for submission
		eventIdList.add(eventId);
		long entryId = createDiaryEvent(entry);

//		String newWorkFlow = getMyConcessionDao().getIsNewWorkflow(bean.getConcId());
		for (XrcDocuments doc : docList) {
			doc.setDiaryEntriesId(entryId);
			doc.setOnbaseHandle(null);
			save(doc, true);
			// ************Changes for EW-576************ -Jay
			// if(pXxmSpConcessions.getSite().equals("TOR")){
			// if(newWorkFlow!=null && newWorkFlow.equals("N")){
			// getMyCustomerConcessionDao().saveDoc(doc);
			// }
		}

		return processConcession(bean,entry);
		/*
		 * boolean isPreassessConc = false;
		 * isPreassessConc=checkPreassessCondition(bean.getConcId());
		 * 
		 * if(!isPreassessConc){ entry.setXrcDiaryEntryDate(new
		 * Timestamp(System.currentTimeMillis()));
		 * entry.setXrcEventId(XrcEventDefinitions.PROCESS_CONCESSION); // for
		 * submission
		 * entry.setXrcDiaryEntry("System Generated event: PROCESS_CONCESSION");
		 * entry.setXrcConcessionId(bean.getConcId()); createDiaryEvent(entry);
		 * }
		 * 
		 * logger.debug("Event created for concID : " +
		 * entry.getXrcConcessionId() + " and Event ID : " +
		 * entry.getXrcEventId()); return entry.getXrcEventId();
		 */
	}

	public int processConcession(XxmSpConcessions bean,XrcDiaryEntries entry) throws SQLException {
		boolean isPreassessConc = false;
		//XrcDiaryEntries entry = new XrcDiaryEntries();
		isPreassessConc = checkPreassessCondition(bean.getConcId());

		if (!isPreassessConc) {
			entry.setXrcDiaryEntryDate(new Timestamp(System.currentTimeMillis()));
			entry.setXrcEventId(XrcEventDefinitions.PROCESS_CONCESSION); // for
																			// submission
			entry.setXrcDiaryEntry("System Generated event: PROCESS_CONCESSION");
			entry.setXrcConcessionId(bean.getConcId());
			createDiaryEvent(entry);
		}
		logger.debug("Event created for concID : " + entry.getXrcConcessionId()
				+ " and Event ID : " + entry.getXrcEventId());
		return entry.getXrcEventId();
	}

	public int saveConcession(XxmSpConcessions bean) throws SQLException {
		XxmSpConcessions pXxmSpConcessions = bean;
		return getMyConcessionDao().update(pXxmSpConcessions);
	}

	/*
	 * code for preassesment - karthick code start
	 */

	/*
	 * @Override public ArrayList<XxmSpConcessions>
	 * fetchSubmittedConcessions(XxmSpConcessions pXxmSpConcessions) throws
	 * SQLException {
	 * 
	 * ArrayList<XxmSpConcessions> xxmpList = new ArrayList<XxmSpConcessions>();
	 * ArrayList<XxmSpConcessions> results = new ArrayList<XxmSpConcessions>();
	 * 
	 * // //Set the CompletionDate xxmpList =
	 * getMyConcessionDao().fetchSubmittedConcessions(pXxmSpConcessions);
	 * 
	 * for (XxmSpConcessions xxmSpConcessions : xxmpList) { Timestamp
	 * completionDate =
	 * getMyConcessionDao().getCompletionDate(xxmSpConcessions.getConcId());
	 * xxmSpConcessions.setCompletionDate(completionDate);
	 * results.add(xxmSpConcessions); }
	 * 
	 * return results; }
	 */

	public ArrayList<XxmSpConcessions> fetchSubmittedConcessions(
			XxmSpConcessions pXxmSpConcessions, int userId) throws SQLException {
		// XxmSpConcessions pXxmSpConcessions = new XxmSpConcessions(bean);

		ArrayList<XxmSpConcessions> xxmpList = new ArrayList<XxmSpConcessions>();
		// ArrayList<XxmSpConcessions> results = new
		// ArrayList<XxmSpConcessions>();

		// //Set the CompletionDate
		xxmpList = getMyConcessionDao().fetchSubmittedConcessions(
				pXxmSpConcessions, userId);

		/*
		 * for (XxmSpConcessions xxmSpConcessions : xxmpList) { // Set the site
		 * value String site; if(xxmSpConcessions.getSite()==null ||
		 * xxmSpConcessions.getSite()==""){ site =
		 * getMyConcessionDao().getRCSite(xxmSpConcessions);
		 * xxmSpConcessions.setSite(site); }else{ site =
		 * xxmSpConcessions.getSite(); } // Set the Address value
		 * ShippingAddress address = new ShippingAddress(); if
		 * (site.equalsIgnoreCase("GLO")) { address.setSht1(
		 * "Messier-Bugatti-Dowty Landing Gears & Systems Integration Division"
		 * ); address.setSht2("Cheltenham Road East,");
		 * address.setSht3("Gloucester, GL2 9QH,"); address.setSht4("England");
		 * xxmSpConcessions.setAddress(address); // xxmSpConcessions.setAddress(
		 * "Messier-Bugatti-Dowty Landing Gears & Systems Integration Division"
		 * +"\n" // + "Cheltenham Road East," + "\n" + //
		 * "Gloucester, GL2 9QH"+"\n"+"England");
		 * xxmSpConcessions.setAddress(address); } else if
		 * (site.equalsIgnoreCase("VEL")) {
		 * address.setSht1("Messier-Bugatti-Dowty");
		 * address.setSht2("Inovel Parc Sud,");
		 * address.setSht3("78140, Vélizy-Villacoublay");
		 * address.setSht4("France"); xxmSpConcessions.setAddress(address); //
		 * xxmSpConcessions
		 * .setAddress("Messier-Bugatti-Dowty"+"\n"+"Inovel Parc Sud,"
		 * +"\n"+"78140, Vélizy-Villacoublay"+"\n"+"France"); } else if
		 * (site.equalsIgnoreCase("TOR")) {
		 * address.setSht1("Messier-Dowty Inc");
		 * address.setSht2("574 Monarch Avenue,");
		 * address.setSht3("Ajax, Ontario L1S 2G8"); address.setSht4("Canada");
		 * xxmSpConcessions.setAddress(address); //
		 * xxmSpConcessions.setAddress("Messier-Dowty Inc,"
		 * +"\n"+"574 Monarch Avenue,"
		 * +"\n"+"Ajax, Ontario L1S 2G8"+"\n"+"Canada"); }
		 * 
		 * // Set the CompletionDate Timestamp completionDate =
		 * getMyConcessionDao().getCompletionDate(
		 * xxmSpConcessions.getConcId());
		 * xxmSpConcessions.setCompletionDate(completionDate);
		 * 
		 * results.add(xxmSpConcessions); }
		 */
		return xxmpList;
	}

	/* code for preassessment end */

	public ArrayList<XxmSpConcessions> findConcessions(XxmSpConcessions bean,
			String isSearchConc) throws SQLException {
		// XxmSpConcessions pXxmSpConcessions = new XxmSpConcessions(bean);

		ArrayList<XxmSpConcessions> xxmpList = new ArrayList<XxmSpConcessions>();
		ArrayList<XxmSpConcessions> results = new ArrayList<XxmSpConcessions>();
		// ArrayList<XxmSpConcessions> results = new
		// ArrayList<XxmSpConcessions>();

		// //Set the CompletionDate
		xxmpList = getMyConcessionDao().fetch(bean, isSearchConc);

		/*
		 * for (XxmSpConcessions xxmSpConcessions : xxmpList) { String site; //
		 * Set the site value if(xxmSpConcessions.getSite()==null ||
		 * xxmSpConcessions.getSite().isEmpty()){ site =
		 * getMyConcessionDao().getRCSite(xxmSpConcessions);
		 * xxmSpConcessions.setSite(site); }else{ site =
		 * xxmSpConcessions.getSite(); }
		 * 
		 * // Set the Address value ShippingAddress address = new
		 * ShippingAddress(); if (site.equalsIgnoreCase("GLO")) {
		 * address.setSht1
		 * ("Messier-Bugatti-Dowty Landing Gears & Systems Integration Division"
		 * ); address.setSht2("Cheltenham Road East,");
		 * address.setSht3("Gloucester, GL2 9QH,"); address.setSht4("England");
		 * xxmSpConcessions.setAddress(address); // xxmSpConcessions.setAddress(
		 * "Messier-Bugatti-Dowty Landing Gears & Systems Integration Division"
		 * +"\n" // + "Cheltenham Road East," + "\n" + //
		 * "Gloucester, GL2 9QH"+"\n"+"England");
		 * xxmSpConcessions.setAddress(address); } else if
		 * (site.equalsIgnoreCase("VEL")) {
		 * address.setSht1("Messier-Bugatti-Dowty");
		 * address.setSht2("Inovel Parc Sud,");
		 * address.setSht3("78140, Vélizy-Villacoublay");
		 * address.setSht4("France"); xxmSpConcessions.setAddress(address); //
		 * xxmSpConcessions
		 * .setAddress("Messier-Bugatti-Dowty"+"\n"+"Inovel Parc Sud,"
		 * +"\n"+"78140, Vélizy-Villacoublay"+"\n"+"France"); } else if
		 * (site.equalsIgnoreCase("TOR")) {
		 * address.setSht1("Messier-Dowty Inc");
		 * address.setSht2("574 Monarch Avenue,");
		 * address.setSht3("Ajax, Ontario L1S 2G8"); address.setSht4("Canada");
		 * xxmSpConcessions.setAddress(address); //
		 * xxmSpConcessions.setAddress("Messier-Dowty Inc,"
		 * +"\n"+"574 Monarch Avenue,"
		 * +"\n"+"Ajax, Ontario L1S 2G8"+"\n"+"Canada"); }
		 * 
		 * // Set the CompletionDate Timestamp completionDate =
		 * getMyConcessionDao().getCompletionDate(
		 * xxmSpConcessions.getConcId());
		 * xxmSpConcessions.setCompletionDate(completionDate);
		 * 
		 * results.add(xxmSpConcessions); }
		 */
		for (XxmSpConcessions xxmSpConcessions : xxmpList) {
			
			Timestamp completionDate = getMyConcessionDao().getCompletionDate(xxmSpConcessions.getConcId());
			xxmSpConcessions.setCompletionDate(completionDate);
			
			results.add(xxmSpConcessions);		
		}
		
		return results;
	}

	public ArrayList<ConcessionsIportalSearchVW> fetch(
			ConcessionsIportalSearchVW pConcessionsIportalSearchVw, int userId)
			throws SQLException {

		ArrayList<ConcessionsIportalSearchVW> results = new ArrayList<ConcessionsIportalSearchVW>();

		results = getMyConcessionDao().fetch(pConcessionsIportalSearchVw,
				userId);

		return results;
	}

	// -------------------------- code for preassessment count
	// method-----------------------------
	@Override
	public Long getPreassessCount(XxmSpConcessions pXxmSpConcessions, int userId)
			throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao()
				.getPreassessCount(pXxmSpConcessions, userId);
	}

	@Override
	public Long getCount(XxmSpConcessions pXxmSpConcessions, String isSearchConc)
			throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().getCount(pXxmSpConcessions, isSearchConc);
	}

	@Override
	public Long getHCCount(
			ConcessionsIportalSearchVW pConcessionsIportalSearchVw, int userId)
			throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().getHCCount(pConcessionsIportalSearchVw,
				userId);
	}

	public ArrayList<XxmSpConcessions> findEditConcessions(XxmSpConcessions bean)
			throws SQLException {
		// XxmSpConcessions pXxmSpConcessions = new XxmSpConcessions(bean);

		ArrayList<XxmSpConcessions> xxmpList = new ArrayList<XxmSpConcessions>();
		// ArrayList<XxmSpConcessions> results = new
		// ArrayList<XxmSpConcessions>();

		// //Set the CompletionDate
		xxmpList = getMyConcessionDao().fetchEdit(bean);

		/*
		 * for (XxmSpConcessions xxmSpConcessions : xxmpList) { String site; //
		 * Set the site value if(xxmSpConcessions.getSite()==null ||
		 * xxmSpConcessions.getSite()==""){ site =
		 * getMyConcessionDao().getRCSite(xxmSpConcessions);
		 * xxmSpConcessions.setSite(site); }else{ site =
		 * xxmSpConcessions.getSite(); }
		 * 
		 * // Set the Address value // if (site.equalsIgnoreCase("GLO")){ //
		 * xxmSpConcessions.setAddress(
		 * "Messier-Bugatti-Dowty Landing Gears & Systems Integration Division"
		 * +"\n"
		 * +"Cheltenham Road East,"+"\n"+"Gloucester, GL2 9QH"+"\n"+"England");
		 * // } else if(site.equalsIgnoreCase("VEL")){ //
		 * xxmSpConcessions.setAddress
		 * ("Messier-Bugatti-Dowty"+"\n"+"Inovel Parc Sud,"
		 * +"\n"+"78140, V&eacute;lizy-Villacoublay"+"\n"+"France"); // } else
		 * if (site.equalsIgnoreCase("TOR")){ //
		 * xxmSpConcessions.setAddress("Messier-Dowty Inc.,"
		 * +"\n"+"574 Monarch Avenue,"
		 * +"\n"+"Ajax, Ontario L1S 2G8"+"\n"+"Canada"); // } ShippingAddress
		 * address = new ShippingAddress(); if (site.equalsIgnoreCase("GLO")) {
		 * address.setSht1(
		 * "Messier-Bugatti-Dowty Landing Gears & Systems Integration Division"
		 * ); address.setSht2("Cheltenham Road East,");
		 * address.setSht3("Gloucester, GL2 9QH,"); address.setSht4("England");
		 * xxmSpConcessions.setAddress(address); // xxmSpConcessions.setAddress(
		 * "Messier-Bugatti-Dowty Landing Gears & Systems Integration Division"
		 * +"\n" // + "Cheltenham Road East," + "\n" + //
		 * "Gloucester, GL2 9QH"+"\n"+"England");
		 * xxmSpConcessions.setAddress(address); } else if
		 * (site.equalsIgnoreCase("VEL")) {
		 * address.setSht1("Messier-Bugatti-Dowty");
		 * address.setSht2("Inovel Parc Sud,");
		 * address.setSht3("78140, Vélizy-Villacoublay");
		 * address.setSht4("France"); xxmSpConcessions.setAddress(address); //
		 * xxmSpConcessions
		 * .setAddress("Messier-Bugatti-Dowty"+"\n"+"Inovel Parc Sud,"
		 * +"\n"+"78140, Vélizy-Villacoublay"+"\n"+"France"); } else if
		 * (site.equalsIgnoreCase("TOR")) {
		 * address.setSht1("Messier-Dowty Inc");
		 * address.setSht2("574 Monarch Avenue,");
		 * address.setSht3("Ajax, Ontario L1S 2G8"); address.setSht4("Canada");
		 * xxmSpConcessions.setAddress(address); //
		 * xxmSpConcessions.setAddress("Messier-Dowty Inc,"
		 * +"\n"+"574 Monarch Avenue,"
		 * +"\n"+"Ajax, Ontario L1S 2G8"+"\n"+"Canada"); }
		 * 
		 * // Set the CompletionDate Timestamp completionDate =
		 * getMyConcessionDao().getCompletionDate(
		 * xxmSpConcessions.getConcId());
		 * xxmSpConcessions.setCompletionDate(completionDate);
		 * 
		 * results.add(xxmSpConcessions); }
		 */

		return xxmpList;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.md.spiceweb.services.ConcessionsService#getCMMList(java.lang.String)
	 */

	public ArrayList<CMM> getCMMList(String name) throws Exception {

		return getMyConcessionDao().getCmmList(name);
	}

	public XrcDocuments getLatestDocument(Long concId) throws SQLException {
		ArrayList<XrcDocuments> docs = getMyConcessionDao().getLatestDocument(
				concId);
		if (docs.size() >= 1)
			return docs.get(0);
		else
			return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.md.spiceweb.services.ConcessionsService#getGearTypes(java.lang.String
	 * )
	 */

	public ArrayList<GearType> getGearTypes(String name, Integer acId)
			throws Exception {
		ArrayList<GearType> list2 = getMyConcessionDao().getGearTypes(name,
				acId);

		return list2;
	}

	public ArrayList<ACType> getAircraftTypes(Integer fnId, String name,
			Integer usrId) throws Exception {
		ArrayList<ACType> list2 = getMyConcessionDao().getAircraftTypes(fnId,
				name, usrId);

		return list2;
	}

	public String getIsMilitary(int fnId, int usrId) throws Exception {
		return getMyConcessionDao().getIsMilitary(fnId, usrId);
	}

	public ArrayList<XrcDocuments> getFinalRCDoc(Long concId)
			throws SQLException {
		ArrayList<XrcDocuments> docs = getMyConcessionDao().getLatestDocument(
				concId);
		if (docs.size() >= 1)
			return docs;
		else
			return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.md.spiceweb.services.ConcessionsService#fetchStatuses(com.md.spiceweb
	 * .domain.concessions.XrcConcessionStatuses)
	 */

	public ArrayList<XrcConcessionStatuses> fetchStatuses(
			XrcConcessionStatuses pXrcConcessionStatuses) throws SQLException {
		return getMyConcessionDao().fetchStatuses(pXrcConcessionStatuses);
	}

	public ArrayList<String> loadConcessionRoles(Integer userId)
			throws SQLException {
		return getMyConcessionDao().loadConcessionRoles(userId);
	}

	public ArrayList<XrcConcessionStatuses> fetchConcessionStatuses()
			throws SQLException {
		return getMyConcessionDao().fetchConcessionStatuses();
	}

	public ArrayList<ConcessionsIssueTypes> fetchIssueTypes(
			ConcessionsIssueTypes pXrcIssueTypes) throws SQLException {
		return getMyConcessionDao().fetchIssueTypes(pXrcIssueTypes);
	}

	@Override
	public ArrayList<XrcPriorities> fetchPriority(XrcPriorities pXrcPriorities)
			throws SQLException {
		return getMyConcessionDao().fetchPriority(pXrcPriorities);
	}

	@Override
	public ArrayList<ConcessionsPriority> fetchConcessionsPriority(
			ConcessionsPriority pPriority) throws SQLException {
		return getMyConcessionDao().fetchConcessionsPriority(pPriority);
	}

	public ArrayList<XrcSites> fetchSites(XrcSites pXrcSites)
			throws SQLException {
		return getMyConcessionDao().fetchSites(pXrcSites);
	}

	public ArrayList<XrcValidationDecisions> fetch(
			XrcValidationDecisions pXrcValidationDecisions) throws SQLException {
		return getMyConcessionDao().fetch(pXrcValidationDecisions);
	}

	/*
	 * 
	 * created this method to fetch service code, service name from
	 * XrcServiceOffering table for Dashboard page
	 * 
	 * Venkat 27/9/2013
	 */

	public ArrayList<XrcServiceOffering> fetchServiceOfferings(
			XrcServiceOffering oXrcServiceOffering) throws SQLException {
		return getMyConcessionDao().fetchServiceOfferings(oXrcServiceOffering);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.md.spiceweb.services.ConcessionsService#getConcessionsCount(com.md
	 * .spiceweb.domain.concessions.XxmSpConcessions)
	 */

	public int getConcessionsCount(XxmSpConcessions conc) throws SQLException {
		return getMyConcessionDao().getConcessionsCount(conc);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.md.spiceweb.services.ConcessionsService#deleteDocument(java.lang.
	 * String)
	 */

	public int deleteDocument(Integer doId) throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().deleteDocument(doId);
	}

	public Double getUsdPrice(Integer itemId) throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().getUsdPrice(itemId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.md.spiceweb.services.ConcessionsService#getRCParts(java.lang.String)
	 */

	public ArrayList<XrcPart> getRCParts(String partnumber) throws Exception {
		// TODO Auto-generated method stub
		return getMyConcessionDao().getRCParts(partnumber);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.md.spiceweb.services.ConcessionsService#getSolarParts(java.lang.String
	 * )
	 */

	public ArrayList<XrcPart> getSolarParts(String partnumber) throws Exception {
		// TODO Auto-generated method stub
		return getMyConcessionDao().getSolarParts(partnumber);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.md.spiceweb.services.ConcessionsService#getAircraftTypes(java.lang
	 * .String, java.lang.Integer)
	 */

	public ArrayList<ACType> getAircraftTypes(String name, Integer usrId)
			throws Exception {
		return getMyConcessionDao().getAircraftTypes(name, usrId);
	}

	public ArrayList<ACType> getAircraftTypes(String name) throws Exception {
		return getMyConcessionDao().getAircraftTypes(name);
	}

	public ArrayList<ACType> getHCAircraftTypes(String name)
			throws SQLException {
		return getMyConcessionDao().getHCAircraftTypes(name);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.md.spiceweb.services.ConcessionsService#getDiaryEntries(java.lang
	 * .Integer)
	 */

	public ArrayList<XrcDiaryEntries> getDiaryEntries(Long concId)
			throws Exception {
		// TODO Auto-generated method stub
		return getMyConcessionDao().getDiaryEntries(concId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.md.spiceweb.services.ConcessionsService#getAvailableEvents(java.lang
	 * .Integer, java.lang.String)
	 */

	public ArrayList<XrcEventDefinitions> getAvailableEvents(Long concId,
			Integer usrId, String standardFlag) {
		// TODO Auto-generated method stub
		return getMyConcessionDao().getAvailableEvents(concId, usrId,
				standardFlag);
	}

	public ArrayList<XrcEventDefinitions> getAvailableEventsPre(Long concId,
			Integer usrId, String standardFlag) {
		// TODO Auto-generated method stub
		return getMyConcessionDao().getAvailableEventsPre(concId, usrId,
				standardFlag);
	}

	public boolean isFinalRCDocRequested(Long concId, int docId, int usrId)
			throws SQLException {
		XxmSpDocRequests req = new XxmSpDocRequests();
		req.setConcId(concId);
		req.setDocId(docId);
		req.setReqUser(usrId);
		req.setReqCompleted("N");
		ArrayList<XxmSpDocRequests> reqList = getXxmSpDocRequestDao()
				.fetch(req);
		if (reqList != null && reqList.size() > 0) {
			return true;
		}
		return false;
	}

	public void createFinalRCDocRequested(Long concId, int docId, int usrId)
			throws SQLException {
		XxmSpDocRequests req = new XxmSpDocRequests();
		req.setConcId(concId);
		req.setDocId(docId);
		req.setReqUser(usrId);
		req.setReqCompleted("N");
		req.setReqDate(new Timestamp(System.currentTimeMillis()));
		getXxmSpDocRequestDao().save(req);

	}

	public void updateFinalRCDocRequested(Long concId, int docId, int usrId)
			throws SQLException {
		XxmSpDocRequests req = new XxmSpDocRequests();
		req.setConcId(concId);
		req.setDocId(docId);
		req.setReqUser(usrId);
		req.setReqCompleted("Y");
		req.setReqCompletedDate(new Timestamp(System.currentTimeMillis()));
		getXxmSpDocRequestDao().update(req);

	}

	public ArrayList<XxmSpConcessions> fetchConcessions(
			XxmSpConcessions pXxmSpConcessions) throws SQLException {
		ArrayList<XxmSpConcessions> concList = new ArrayList<XxmSpConcessions>();

		concList = getMyConcessionDao().fetchConcessions(pXxmSpConcessions);

		return concList;
	}

	@Override
	public Integer delete(Long concId) throws Exception {
		// TODO Auto-generated method stub
		return getMyConcessionDao().delete(concId);
	}

	@Override
	public Long save(XrcWysiwygContent pXrcWysiwygContent) throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().save(pXrcWysiwygContent);
	}

	@Override
	public Long save(XrcEngineeringAttributes pXrcEngineeringAttributes)
			throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().save(pXrcEngineeringAttributes);
	}

	@Override
	public int update(XrcEngineeringAttributes pXrcEngineeringAttributes)
			throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().update(pXrcEngineeringAttributes);
	}

	@Override
	public int update(XrcWysiwygContent pXrcWysiwygContent) throws SQLException {
		return getMyConcessionDao().update(pXrcWysiwygContent);
	}

	@Override
	public ArrayList<XrcEngineeringAttributes> fetch(
			XrcEngineeringAttributes pXrcEngineeringAttributes)
			throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().fetch(pXrcEngineeringAttributes);
	}

	@Override
	public ArrayList<XrcWysiwygContent> fetch(
			XrcWysiwygContent pXrcWysiwygContent) throws SQLException {
		return getMyConcessionDao().fetch(pXrcWysiwygContent);
	}

	@Override
	public ArrayList<XrcWysiwygContent> fetchView(
			XrcWysiwygContent pXrcWysiwygContent) throws SQLException {
		return getMyConcessionDao().fetchView(pXrcWysiwygContent);
	}

	/*
	 * @Override public ArrayList<XrcWysiwygContent> fetchContent (Long
	 * concId,int eventId,int contentTypeId) throws SQLException{ return
	 * getMyConcessionDao().fetchContent(concId,eventId,contentTypeId); }
	 */

	@Override
	public ArrayList<XrcWysiwygContent> fetchContent(Long concId,
			String contentTypesCode) throws SQLException {
		return getMyConcessionDao().fetchContent(concId, contentTypesCode);
	}

	@Override
	public ArrayList<KeyValueBean> fetch(XrcMaterialGrades pXrcMaterialGrades)
			throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().fetch(pXrcMaterialGrades);
	}

	@Override
	public ArrayList<XrcMaterials> fetch(XrcMaterials pXrcMaterials)
			throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().fetch(pXrcMaterials);
	}

	@Override
	public ArrayList<KeyValueBean> fetch(XrcBaseMaterials pXrcBaseMaterials)
			throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().fetch(pXrcBaseMaterials);
	}

	@Override
	public ArrayList<KeyValueBean> fetch(XrcClassifications pXrcClassifications)
			throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().fetch(pXrcClassifications);
	}

	@Override
	public ArrayList<KeyValueBean> fetch(
			XrcRepairCategories pXrcRepairCategories) throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().fetch(pXrcRepairCategories);
	}

	@Override
	public ArrayList<XrcWysiwygContentTypes> fetch(
			XrcWysiwygContentTypes pXrcWysiwygContentTypes) throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().fetch(pXrcWysiwygContentTypes);
	}
	
	public ArrayList<XrcDocumentTypes> fetchDocxTypes () throws SQLException{
		return getMyConcessionDao().fetchDocxTypes();
	}
	
	@Override
	public ArrayList<XrcDocuments> fetch(XrcDocuments pXrcDocuments)
			throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().fetch(pXrcDocuments);
	}

	@Override
	public ArrayList<XrcWysiwygContent> fetchData(Long concId)
			throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().fetchData(concId);
	}

	@Transactional
	@Override
	public XrcDocuments save(XrcDocuments pXrcDocuments, boolean external)
			throws SQLException {
		// TODO Auto-generated method stub

		/*
		 * XxmSpConcessions conc =
		 * getMyConcessionDao().getConcession(pXrcDocuments.getConcId()); if
		 * (conc != null && conc.getConcessionNo() != null &&
		 * conc.getConcessionNo().trim().length() > 0) {
		 * pXrcDocuments.setRelativePath(conc.getConcessionNo() + "/"+
		 * pXrcDocuments.getOriginalName());
		 * pXrcDocuments.setRealPath(getConcessionsDirPath() + "/"+
		 * conc.getConcessionNo() + "/"+ pXrcDocuments.getOriginalName()); }
		 * String message = pXrcDocuments.getOriginalName() + "_"+
		 * pXrcDocuments.getRealPath() + "_"+ pXrcDocuments.getRelativePath();
		 * logger.debug("before save Document :" + message);
		 */
		XrcDocuments saveDocument1 = null;
		try {
			saveDocument1 = getMyConcessionDao().save(pXrcDocuments, external);
			logger.debug("Saving Document:");
		} catch (DataIntegrityViolationException e) {
			saveDocument1 = update(pXrcDocuments);
			logger.debug("Updating Document:");
			throw e;
		}
		return saveDocument1;
	}

	@Override
	@Transactional
	public Long saveDiaryEntry(XrcDiaryEntries bean) throws SQLException {
		return getMyConcessionDao().saveDiaryEntry(bean);
	}

	@Override
	public ArrayList<XrcDepartments> fetchGroup(XrcDepartments pXrcDepartments)
			throws SQLException {
		return getMyConcessionDao().fetchGroup(pXrcDepartments);
	}

	@Override
	public ArrayList<XrcDepartments> fetchIndividual(
			XrcDepartments pXrcDepartments) throws SQLException {
		return getMyConcessionDao().fetchIndividual(pXrcDepartments);
	}

	@Override
	public ArrayList<XrcConcessionRoles> fetchRoles(int usrId)
			throws SQLException {
		return getMyConcessionDao().fetchRoles(usrId);
	}

	@Override
	public ArrayList<XrcDiaryEntries> fetchConcEvents(
			XxmSpConcessions pXxmSpConcessions, int userId) throws SQLException {
		return getMyConcessionDao().fetchConcEvents(pXxmSpConcessions, userId);
	}

	@Override
	public boolean isConcessionExists(Long concId) throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().isConcessionExists(concId);
	}

	@Transactional
	public Long createDiaryEvent(XrcDiaryEntries diary) throws SQLException {
		Long retVal = 0L;
		Long concId;
		String newWorkFlow = getMyConcessionDao().getIsNewWorkflow(
				diary.getXrcConcessionId());

		logger.debug("Entering createDiaryEvent for Diary Object---------------->>>"
				+ diary.getXrcConcessionId());
		if (diary.getXrcEventId() == XrcEventDefinitions.FINAL_RC_RESPONSE) {
			// Code for Fina Rc Response
		} else if (diary.getXrcEventId() == XrcEventDefinitions.CREATE_NEW_BASE_DATA) {
			// Code for new part
			logger.debug("Entering CREATE_NEW_BASE_DATA----------------->>>>>>>"
					+ diary.getXrcConcessionId());
			XxmSpConcessions spCon = new XxmSpConcessions();
			spCon.setConcId(diary.getXrcConcessionId());
			ArrayList<XxmSpConcessions> list;

			list = getMyConcessionDao().fetchEdit(spCon);
			XxmSpConcessions sp = list.get(0);
			if (sp.getPartId() == null || sp.getPartId() <= 0) {
				createPart(sp);
			}

			if (sp.getParentId() == null || sp.getParentId() <= 0) {
				createParentPart(sp);
			}

			if (sp.getOperatorId() == null || sp.getOperatorId() <= 0) {
				createOperator(diary, sp);
			}

			retVal = getMyConcessionDao().saveDiaryEntry(diary);
			// now process needs to be inserted
			diary.setXrcEventId(XrcEventDefinitions.PROCESS_CONCESSION);
			diary.setXrcDiaryEntry("This is created as a result of adding new base data");
			diary.setXrcDiaryEntryDate(new Timestamp(System.currentTimeMillis()));
			// ************Changes for EW-576************ -Jay
			// if(sp.getSite().equals("TOR")){
			if (newWorkFlow != null && newWorkFlow.equals("N")) {
				sp = getMyCustomerConcessionDao().save(sp);				
					saveFile(sp);				
			} else {
				sp.setConcessionNo(getMyConcessionDao().getRCNumber(
						sp.getAcType(), sp.getGearType(), sp.getSite()));
			}
			retVal = getMyConcessionDao().saveDiaryEntry(diary);
			getMyConcessionDao().update(sp);
			// addDiary(diary);
			// retVal = getCustomerConcessionDao().createDiaryEntry(diary);

		} else if (diary.getXrcEventId() == XrcEventDefinitions.PROCESS_CONCESSION) {
			logger.debug("Entering PROCESS_CONCESSION--------for ConcID------>>>>>"
					+ diary.getXrcConcessionId());
			XxmSpConcessions spCon = new XxmSpConcessions();
			spCon.setConcId(diary.getXrcConcessionId());
			ArrayList<XxmSpConcessions> list;

			XxmSpConcessions sp = getMyConcessionDao().getConcession(
					spCon.getConcId());
			// ************Changes for EW-576************ -Jay
			// if(sp.getSite().equals("TOR")){
			if (newWorkFlow != null && newWorkFlow.equals("N")) {
				sp = getMyCustomerConcessionDao().save(sp);
				saveFile(sp);
			} else {
				sp.setConcessionNo(getMyConcessionDao().getRCNumber(
						sp.getAcType(), sp.getGearType(), sp.getSite()));
			}
			getMyConcessionDao().update(sp);
			retVal = getMyConcessionDao().saveDiaryEntry(diary);
			
		} else if (diary.getXrcEventId() == XrcEventDefinitions.CUSTOMER_SUBMIT_RC) {
			XxmSpConcessions spCon = new XxmSpConcessions();
			spCon.setConcId(diary.getXrcConcessionId());
			ArrayList<XxmSpConcessions> list;
			int ccId = 0;

			XxmSpConcessions sp = getMyConcessionDao().getConcession(
					spCon.getConcId());
			/*
			 * if
			 * (!getMyCustomerConcessionDao().isConcessionExists(sp.getConcId(
			 * ))) { // sp = getMyCustomerConcessionDao().save(sp); //
			 * getMyConcessionDao().update(sp); } else {
			 * getMyCustomerConcessionDao().getCcId(sp.getConcId()); }
			 */
			retVal = getMyConcessionDao().saveDiaryEntry(diary);
						
			/* sp = getMyConcessionDao().getConcession(spCon.getConcId()); if
			 * (sp.getConcessionNo() != null) { String dirPath =
			 * getConcessionsDirPath()+ sp.getConcessionNo()+
			 * System.getProperty("file.separator"); File dir = new
			 * File(dirPath); if (!dir.isDirectory()) dir.mkdir(); XrcDocuments
			 * xrcDocuments = new XrcDocuments();
			 * xrcDocuments.setConcId(sp.getConcId()); ArrayList<XrcDocuments>
			 * list1 = getMyConcessionDao().fetch(xrcDocuments); for (int count
			 * = 0; count < list1.size(); count++) { XrcDocuments xpm1 =
			 * (XrcDocuments) list1.get(count); String doname = xpm1.getName();
			 * logger.debug("File checks 1"+ getInSubmitFolder()+ doname+
			 * " EXISTS "+ new File(getInSubmitFolder() + doname).exists());
			 * logger.debug("File checks 2"+ getConcessionsDirPath()+
			 * sp.getConcessionNo() + System.getProperty("file.separator")+
			 * xpm1.getOriginalName()+ " exists " + !((new
			 * File(getConcessionsDirPath()+ sp.getConcessionNo() +
			 * System.getProperty("file.separator")+
			 * xpm1.getOriginalName())).exists()));
			 * logger.debug("File checks 3"+ getWatchDirPath()+
			 * System.getProperty("file.separator") + doname+ " exists " + ((new
			 * File(getWatchDirPath()+ System.getProperty("file.separator")+
			 * doname))).exists()); if ((new File(getInSubmitFolder() +
			 * doname).exists()) && !((new File(getConcessionsDirPath() +
			 * sp.getConcessionNo() + System.getProperty("file.separator") +
			 * xpm1.getOriginalName())).exists())) {
			 * fileCopy(getInSubmitFolder() + doname,getConcessionsDirPath()+
			 * sp.getConcessionNo()+ System.getProperty("file.separator")+
			 * xpm1.getOriginalName());
			 * xpm1.setRealPath(getConcessionsDirPath()+ sp.getConcessionNo()+
			 * System.getProperty("file.separator")+ xpm1.getOriginalName());
			 * xpm1.setRelativePath(sp.getConcessionNo() + "/"+
			 * xpm1.getOriginalName()); update(xpm1); int returnVal =
			 * getMyCustomerConcessionDao().saveDoc(xpm1);
			 * logger.debug("Save Doc--------------->>> :" + returnVal); } else
			 * if ((new File(getWatchDirPath() + doname).exists())&& !((new
			 * File(getConcessionsDirPath()+ sp.getConcessionNo()+
			 * System.getProperty("file.separator")+
			 * xpm1.getOriginalName())).exists())) { fileCopy(getWatchDirPath()
			 * + doname,getConcessionsDirPath()+ sp.getConcessionNo()+
			 * System.getProperty("file.separator")+ xpm1.getOriginalName());
			 * xpm1.setRealPath(getConcessionsDirPath() + sp.getConcessionNo() +
			 * System.getProperty("file.separator") + xpm1.getOriginalName());
			 * xpm1.setRelativePath(sp.getConcessionNo() + "/"+
			 * xpm1.getOriginalName()); update(xpm1); int returnVal =
			 * getMyCustomerConcessionDao().saveDoc(xpm1);
			 * logger.debug("Save Doc--------------->>> :"+ returnVal); } } }
			 */

		} else if (diary.getXrcEventId() == XrcEventDefinitions.ADDITIONAL_INFORMATION) {
			logger.debug("Entering ADDITIONAL_INFORMATION-------------->>>for concId :"
					+ diary.getXrcConcessionId());
			retVal = getMyConcessionDao().saveDiaryEntry(diary);
			// retVal = addDiary(diary);
		} else if (diary.getXrcEventId() == XrcEventDefinitions.SUBMIT_RESPONSE) {
			logger.debug("Entering SUBMIT_RESPONSE-------------->>>for concId :"
					+ diary.getXrcConcessionId());
			// retVal = addDiary(diary);

			retVal = getMyConcessionDao().saveDiaryEntry(diary);
		} else {
			logger.debug("Entering Else Conditions---------for concId -->>>"
					+ diary.getXrcConcessionId() + " and Event Id :"
					+ diary.getXrcEventId());
			// logger.debug("ENGINEERING_CLOSE----->>>>>" +
			// XrcEventDefinitions.ENGINEERING_CLOSE);

			concId = diary.getXrcConcessionId();
			retVal = getMyConcessionDao().saveDiaryEntry(diary);
			if (diary.getXrcEventId() == XrcEventDefinitions.ENGINEERING_CLOSE) {
				logger.debug("Entering ENGINEERING_CLOSE---------for concId -->>>"
						+ diary.getXrcConcessionId());
				// getMyCustomerConcessionDao().updateFinalRC(concId);
			}

		}
		return retVal;
	}
	
	

	public List<XrcDocuments> getOldDocList(List<XrcDocuments> docList, long entryId)
			throws Exception {
		logger.debug("docList : " + docList);

		File inFile = null;

		List<XrcDocuments> finalDocList = new ArrayList<XrcDocuments>();	
		
		/*if (docList != null) {
			for (XrcDocuments doc : docList) {
				doc.setId(null);
				doc.setDiaryEntriesId(entryId);
				doc.setRealPath("ONBASE");
				String sessionId = getFileTransferAPI().login();
				if (sessionId != null && !"".equals(sessionId)) {
					Long docHandle = getFileTransferAPI().docExists(sessionId,
							FileTransferAPI.KWD_XRC_DOC_ID, doc.getName());
					logger.info(doc.getName() + " has a onbase handle of "
							+ docHandle);
					if (docHandle != null && docHandle > 0) {
						doc.setOnbaseHandle(docHandle);
						if (docHandle > 1) {
							doc.setDocAvailableDate(new Timestamp(System
									.currentTimeMillis()));
						}
						inFile = getFileTransferAPI().download(sessionId,
								docHandle);
						if (inFile != null && inFile.exists()) {
							doc.setInFile(inFile);
						}

					}

					getFileTransferAPI().logout(sessionId);
				} else {
					logger.error("Onbase login unsuccesful sessionId = "
							+ sessionId);
				}
				finalDocList.add(doc);
			}
		}*/
		return finalDocList;
	}
	
	
	
	public void saveFile(XxmSpConcessions concession) {
		logger.debug("Enter TERC file adding");
		logger.debug("Concession : "+ concession);
		String separator = "/";
		XrcDocuments document = new XrcDocuments();
		document.setConcId(concession.getConcId());
		ArrayList<XrcDocuments> docList = null;
		try {
			logger.debug(getMyConcessionDao()!=null?" ---- Exist":"---Not Exist");
			docList = getMyConcessionDao().fetch(document);
			logger.debug("Doc List : "+ docList!=null?docList.size():"--");
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			logger.error(e1);
		}
		for (XrcDocuments doc : docList) {
			try {
				if (concession.getConcessionNo() != null) {
					File file = new File(getInDraftFolder()+doc.getName());
					 if (file.exists()) {
						 logger.debug("File Path : "+getConcessionsDirPath()+ concession.getConcessionNo());
						 File checkFile = new File(getConcessionsDirPath()+ concession.getConcessionNo()); 
						 if(!checkFile.exists()){
							 checkFile.mkdir();
						 }
						 file.renameTo(
								 new File(getConcessionsDirPath()
										 + concession.getConcessionNo() + separator
										 + doc.getOriginalName()));
						 doc.setRealPath(getConcessionsDirPath()
								 + concession.getConcessionNo() + separator
								 + doc.getOriginalName());
						 doc.setRelativePath(concession.getConcessionNo()
								 + separator + doc.getOriginalName());						 
					 }	else{
						 logger.debug("File Path : "+getConcessionsDirPath()+ concession.getConcessionNo());
						 File checkFile = new File(getConcessionsDirPath()+ concession.getConcessionNo()); 
						 if(!checkFile.exists()){
							 checkFile.mkdir();
						 }
						 doc.setRealPath(getConcessionsDirPath()
								 + concession.getConcessionNo() + separator
								 + doc.getOriginalName());
						 doc.setRelativePath(concession.getConcessionNo()
								 + separator + doc.getOriginalName());	
					 }
				}
				getMyCustomerConcessionDao().saveDoc(doc);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void createOperator(XrcDiaryEntries diary, XxmSpConcessions sp)
			throws SQLException {
		int opId = getMyCustomerConcessionDao().createNewOperator(
				sp.getNewOperatorName(), sp.getNewOperatorCountry(),
				sp.getNewOperatorType(), diary.getXrcSender());
		if (opId > 0) {
			sp.setNewOperatorName("");
			sp.setNewOperatorType("");
			sp.setNewOperatorCountry("");
			sp.setOperatorId(opId);
			getMyConcessionDao().update(sp);
		}
	}

	@Override
	public void createParentPart(XxmSpConcessions sp) throws SQLException {
		int parentPartId = getMyCustomerConcessionDao().createNewPart(
				sp.getNewParentPart(), sp.getNewParentPartDesc());
		sp.setParentId(parentPartId);
		sp.setParentPartNum(sp.getNewParentPart());
		sp.setNewParentPart("");
		sp.setNewParentPartDesc("");
		if (parentPartId > 0) {
			getMyConcessionDao().update(sp);
		}
	}

	@Override
	public void createPart(XxmSpConcessions sp) throws SQLException {
		int partId = getMyCustomerConcessionDao().createNewPart(
				sp.getNewPartNo(), sp.getNewPartDesc());
		sp.setPartId(partId);
		sp.setPartNum(sp.getNewPartNo());
		sp.setNewPartNo("");
		sp.setNewPartDesc("");
		if (partId > 0) {
			getMyConcessionDao().update(sp);
		}
	}

	/**
	 * @return the watchDirPath
	 */
	public String getWatchDirPath() {
		return watchDirPath;
	}

	/**
	 * @param watchDirPath
	 *            the watchDirPath to set
	 */
	public void setWatchDirPath(String watchDirPath) {
		this.watchDirPath = watchDirPath;
	}

	/**
	 * @return the inSubmitFolder
	 */
	public String getInSubmitFolder() {
		return inSubmitFolder;
	}

	/**
	 * @param inSubmitFolder
	 *            the inSubmitFolder to set
	 */
	public void setInSubmitFolder(String inSubmitFolder) {
		this.inSubmitFolder = inSubmitFolder;
	}

	private void fileCopy(String source, String destination) {
		logger.debug("------fileCopy------source--------->>>>" + source);
		logger.debug("------fileCopy------destination--------->>>>"
				+ destination);
		FileInputStream fin = null;
		FileOutputStream fout = null;

		File file = new File(source);
		File file1 = new File(destination);
		file.renameTo(file1);
	}

	public HashMap<String, String> getCustMail(int eventId, long concId)
			throws SQLException {
		HashMap<String, String> properties = new HashMap<String, String>();

		XxmSpConcessions pXxmSpConcessions = new XxmSpConcessions();
		pXxmSpConcessions.setConcId(concId);
		logger.debug("GetCustMail-------------->>>>> :" + concId);
		ArrayList<XxmSpConcessions> xmp;
		XxmSpConcessions sp;
		xmp = getMyConcessionDao().fetchEdit(pXxmSpConcessions);
		if (xmp != null && xmp.size() > 0) {
			sp = (XxmSpConcessions) xmp.get(0);
			int contactId = sp.getContactId() == null ? sp.getUsrId() : sp
					.getContactId();
			String usrCustEmail = getMyConcessionDao().getPortalUser(contactId);
			properties = getMailDetails(eventId, concId);
			properties.put("MAIL_TO", usrCustEmail);
		}
		logger.debug("GetCustMail-------------->>>>> completed");
		return properties;
	}

	public HashMap<String, String> getMailDetails(int eventId, long concId)
			throws SQLException {
		HashMap<String, String> properties = new HashMap<String, String>();
		String subject = "";
		String body = "";
		ArrayList<XxmSpConcessions> xmp;
		XxmSpConcessions sp;
		XrcDiaryEntries diary = new XrcDiaryEntries();
		diary.setXrcEventId(eventId);
		diary.setXrcConcessionId(concId);

		logger.debug("getMailDetails------------------>>>for ConcId :" + concId);
		logger.debug("Parameter details------------->>>> :" + concId + " and "
				+ eventId);
		if (diary != null) {
			XrcEventDefinitions event = getMyConcessionDao()
					.getXrcEventDefinitions(diary.getXrcEventId());
			XxmSpConcessions pXxmSpConcessions = new XxmSpConcessions();
			pXxmSpConcessions.setConcId(diary.getXrcConcessionId());

			sp = getMyConcessionDao().fetchConcessionById(
					diary.getXrcConcessionId());
			if (sp != null) {

				if (event.getXrcNotifySubject() != null
						&& event.getXrcNotifySubject().length() > 0) {
					subject = SpicewebConstants.getParam(event
							.getXrcNotifySubject());
				}
				if (event.getXrcNotifyBody() != null
						&& event.getXrcNotifyBody().length() > 0) {
					body = SpicewebConstants.getParam(event.getXrcNotifyBody());
				}
				if (body != null && body.length() > 0) {
					body += "\r";
					if (sp.getConcessionNo() != null) {
						body += "Repair Concession Number  :"
								+ sp.getConcessionNo();
					}
					if (sp.getCustRef() != null) {
						body += " and  Customer Reference :  "
								+ sp.getCustRef() + "  .";
					}
					if (event.getXrcNotifyUrl() != null
							&& event.getXrcNotifyUrl().length() > 0) {
						body = body
								+ "\r"
								+ SpicewebConstants.getParam(event
										.getXrcNotifyUrl());
					}
					if (event.getXrcNotifyDisclaimerUK() != null
							&& event.getXrcNotifyDisclaimerUK().length() > 0) {
						body = body
								+ "\r"
								+ SpicewebConstants.getParam(event
										.getXrcNotifyDisclaimerUK());
					}
					if (event.getXrcNotifyDisclaimerFR() != null
							&& event.getXrcNotifyDisclaimerFR().length() > 0) {
						body = body
								+ "\r"
								+ SpicewebConstants.getParam(event
										.getXrcNotifyDisclaimerFR());
					}
				}
				if (subject != null && subject.length() > 0) {
					properties.put("SUBJECT", subject);
					logger.debug("Subject-------------->>>>>>>> :" + subject);
				}
				if (body != null && body.length() > 0) {
					properties.put("BODY", body);
					logger.debug("body-------------->>>>>> :" + body);
				}
			}
		} else {
			logger.debug("---------------Diary is Null--------------------");
		}

		// logger.debug("properties------------>>>" + properties);
		return properties;
	}

	public HashMap<String, String> getUsrMail(XrcDiaryEntries diary, int usrId)
			throws SQLException {
		HashMap<String, String> properties = new HashMap<String, String>();

		XiaSpUser user;
		ArrayList<XrcDiaryEntries> entries = getMyConcessionDao()
				.getMaxDiaryEntries(diary.getXrcConcessionId());
		if (entries != null && entries.size() > 0) {
			XrcDiaryEntries dairy1 = entries.get(0);

			logger.debug("properties for mail diary::::::" + diary);
			user = getMyConcessionDao().getUser(usrId);
			properties = getMailDetails(diary.getXrcEventId(),
					diary.getXrcConcessionId());
			// logger.debug("properties for mail-------------->>>" +
			// properties);
			String mailTo = "";
			if (user.getUsrEmail() != null) {
				properties.put("MAIL_TO", user.getUsrEmail());

			}
			if (user.getUsrMobile() != null) {
				properties.put("TEXT_TO", user.getUsrMobile()
						+ "@messier-dowty.com");
			}
		}
		// logger.debug("properties for after mail-------------->>>"+
		// properties);

		return properties;
	}

	public HashMap<String, String> getUsrTxtMail(XrcDiaryEntries diary)
			throws SQLException {
		HashMap<String, String> properties = new HashMap<String, String>();
		logger.debug("getUsrTxtMail----------------->>>>>>for ConcID : "
				+ diary.getXrcConcessionId());

		XiaSpUser user;
		ArrayList<XrcDiaryEntries> entries = getMyConcessionDao()
				.getMaxDiaryEntries(diary.getXrcConcessionId());
		if (entries != null && entries.size() > 0) {
			XrcDiaryEntries dairy1 = entries.get(0);
			int usrId = getMyConcessionDao().getRCContact(
					dairy1.getXrcConcessionId());
			Integer usrname = (Integer) diary.getXrcSender();
			user = getMyConcessionDao().getUser(usrname);
			properties = getMailDetails(diary.getXrcEventId(),
					diary.getXrcConcessionId());
			if (user.getUsrMobile() != null) {
				properties
						.put("TO", user.getUsrMobile() + "@messier-dowty.com");
			}
		}

		return properties;
	}

	/*
	 * public void sendMail(HashMap<String, String> properties) throws
	 * MessagingException { double id = Math.random(); }
	 */

	/*
	 * @SuppressWarnings("unused") private int addDiary(XrcDiaryEntries diary) {
	 * HashMap<String, String> emailProps = new HashMap<String, String>();
	 * HashMap<String, String> smsProps = new HashMap<String, String>(); int
	 * returnValue = -1; try { returnValue =
	 * getMyCustomerConcessionDao().createDiaryEntry(diary); return returnValue;
	 * 
	 * }
	 */

	@Override
	public String getEvaluationType(Long concId) throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().getEvaluationType(concId);
	}

	@Override
	public String getAirframer(Long concId) throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().getAirframer(concId);
	}

	@Override
	public XrcDocuments update(XrcDocuments pXrcDocuments) throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().update(pXrcDocuments);
	}

	@Override
	public ArrayList<KeyValueBean> fetch(XrcDamageTypes pXrcDamageTypes)
			throws SQLException {
		return getMyConcessionDao().fetch(pXrcDamageTypes);
	}

	@Override
	public int update(XxmSpConcessions pXxmSpConcessions) throws SQLException {
		return getMyConcessionDao().update(pXxmSpConcessions);
	}

	@Override
	public ArrayList<KeyValueBean> fetch(
			XrcMaterialSpecifications pXrcMaterialSpecifications)
			throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().fetch(pXrcMaterialSpecifications);
	}

	@Override
	public ArrayList<XrcDiaryEntries> fetch(XrcDiaryEntries pXrcDiaryEntries)
			throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().fetch(pXrcDiaryEntries);
	}

	/*
	 * 
	 * created this method to fetch content for dashboard table for concession
	 * received and open status
	 * 
	 * Venkat 27/9/2013
	 */

	public ArrayList<ConcessionDashboardOpenTable> fetchDashboardContent(
			ConcessionDashboardOpenTable object) throws SQLException {
		return getMyConcessionDao().fetchDashboardContent(object);
	}

	/*
	 * 
	 * created this method to fetch content for dashboard table for concession
	 * repair status
	 * 
	 * Venkat 08/10/2013
	 */

	public ArrayList<ConcessionDashboardOpenTable> fetchDashboardRepairStatus(
			ConcessionDashboardOpenTable object) throws SQLException {
		return getMyConcessionDao().fetchDashboardRepairStatus(object);
	}

	/*
	 * 
	 * created this method to fetch content for dashboard table for concession
	 * overdue repair status
	 * 
	 * Venkat 08/10/2013
	 */

	public ArrayList<ConcessionDashboardOpenTable> fetchDashboardOverdueRepair(
			ConcessionDashboardOpenTable object) throws SQLException {
		return getMyConcessionDao().fetchDashboardOverdueRepair(object);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public LinkedHashMap fetchFirstChart(
			ConcessionDashboardChart concessionDashboard) throws SQLException {
		return getMyConcessionDao().fetchFirstChart(concessionDashboard);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public LinkedHashMap fetchSecondChart(
			ConcessionDashboardChart concessionDashboard) throws SQLException {
		return getMyConcessionDao().fetchSecondChart(concessionDashboard);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public LinkedHashMap fetchThirdChart(
			ConcessionDashboardChart concessionDashboard) throws SQLException {
		return getMyConcessionDao().fetchThirdChart(concessionDashboard);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public ArrayList fetchChart(
			ConcessionDashboardChart concessionDashboard) throws SQLException {
		return getMyConcessionDao().fetchChart(concessionDashboard);
	}
	
	public List<XrcDocuments> fetchPendingOnbaseDocuments() throws SQLException {
		return getMyConcessionDao().fetchPendingOnbaseDocuments();
	}

	public XrcDocuments fetchDocumentById(Integer id) throws SQLException {
		return getMyConcessionDao().fetchDocumentById(id);
	}

	@Override
	public XrcEventDefinitions getXrcEventDefinitions(int eventId)
			throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().getXrcEventDefinitions(eventId);
	}

	@Override
	public ArrayList<XrcDiaryEntries> getMaxDiaryEntries(Long xrcConcessionId)
			throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().getMaxDiaryEntries(xrcConcessionId);
	}

	public HashMap<String, String> getMailProperties(
			XrcEmailRecipientsView xrcEmailRecipientsView) throws SQLException {
		HashMap<String, String> props = null;
		if (xrcEmailRecipientsView.getRecipientType() != null) {
			props = getCustEmail(xrcEmailRecipientsView);
			props.put("MAIL_TO", xrcEmailRecipientsView.getRecipeintEmail());
			props.put("MAIL_FROM", xrcEmailRecipientsView.getEmailFromAddress());
			logger.debug("Mail Properties---> : " + props);
		}
		return props;
	}

	// Code commented for EW-549 on 6/11/2014 by Raji
	/*
	 * private HashMap<String, String> getCustEmail(XrcEmailRecipientsView
	 * xrcEmailRecipientsView) throws SQLException{ HashMap<String, String>
	 * props = new HashMap<String, String>(); String subject = null; String body
	 * = null;
	 * 
	 * XxmSpConcessions
	 * sp=getMyConcessionDao().fetchConcessionById(xrcEmailRecipientsView
	 * .getXrcConcessionId()); if (xrcEmailRecipientsView.getNotifySubject() !=
	 * null && xrcEmailRecipientsView.getNotifySubject().length() > 0) { subject
	 * = SpicewebConstants.getParam(xrcEmailRecipientsView.getNotifySubject());
	 * } if (xrcEmailRecipientsView.getNotifyBody()!= null &&
	 * xrcEmailRecipientsView.getNotifyBody().length() > 0) { body =
	 * SpicewebConstants.getParam(xrcEmailRecipientsView.getNotifyBody()); } if
	 * (body != null && body.length() > 0) { body += "\r"; if
	 * (sp.getConcessionNo() != null) { body += "Repair Concession Number  :" +
	 * sp.getConcessionNo(); } if (sp.getCustRef() != null) { body +=
	 * " and  Customer Reference :  " + sp.getCustRef() + "  ."; } if
	 * (xrcEmailRecipientsView.getNotifyUrl() != null &&
	 * xrcEmailRecipientsView.getNotifyUrl().length() > 0) { body = body + "\r"
	 * +
	 * SpicewebConstants.getParam(xrcEmailRecipientsView.getNotifyUrl().trim());
	 * } if (xrcEmailRecipientsView.getDisclaimerUk() != null &&
	 * xrcEmailRecipientsView.getDisclaimerUk().length() > 0) { body = body +
	 * "\r" +
	 * SpicewebConstants.getParam(xrcEmailRecipientsView.getDisclaimerUk()
	 * .trim()); } if (xrcEmailRecipientsView.getDisclaimerFr() != null &&
	 * xrcEmailRecipientsView.getDisclaimerFr().length() > 0) { body = body +
	 * "\r" +
	 * SpicewebConstants.getParam(xrcEmailRecipientsView.getDisclaimerFr()
	 * .trim()); } } if (subject != null && subject.length() > 0) {
	 * props.put("SUBJECT", subject);
	 * logger.debug("Subject-------------->>>>>>>> :" + subject); } if (body !=
	 * null && body.length() > 0) { props.put("BODY", body);
	 * logger.debug("body-------------->>>>>> :" + body); } return props; }
	 */

	private HashMap<String, String> getCustEmail(
			XrcEmailRecipientsView xrcEmailRecipientsView) throws SQLException {
		HashMap<String, String> props = new HashMap<String, String>();
		String subject = null;
		String body = null;

		XxmSpConcessions sp = getMyConcessionDao().fetchConcessionById(
				xrcEmailRecipientsView.getXrcConcessionId());
		if (xrcEmailRecipientsView.getNotifySubject() != null
				&& xrcEmailRecipientsView.getNotifySubject().length() > 0) {
			subject = SpicewebConstants.getParam(xrcEmailRecipientsView
					.getNotifySubject());
		}
		if (xrcEmailRecipientsView.getNotifyBody() != null
				&& xrcEmailRecipientsView.getNotifyBody().length() > 0) {
			body = SpicewebConstants.getParam(xrcEmailRecipientsView
					.getNotifyBody());
		}
		if (body != null && body.length() > 0) {
			body += "\r";
			if (sp.getConcessionNo() != null) {
				body += "Repair Concession Number  :" + sp.getConcessionNo();
			}
			if (sp.getCustRef() != null) {
				body += " and  Customer Reference :  " + sp.getCustRef()
						+ "  .";
			}
			if (xrcEmailRecipientsView.getNotifyUrl() != null
					&& xrcEmailRecipientsView.getNotifyUrl().length() > 0) {
				body = body
						+ "\r"
						+ SpicewebConstants.getParam(xrcEmailRecipientsView
								.getNotifyUrl().trim());
			}
			if (xrcEmailRecipientsView.getDisclaimerUk() != null
					&& xrcEmailRecipientsView.getDisclaimerUk().length() > 0) {
				body = body
						+ "\r"
						+ SpicewebConstants.getParam(xrcEmailRecipientsView
								.getDisclaimerUk().trim());
			}
			if (xrcEmailRecipientsView.getDisclaimerFr() != null
					&& xrcEmailRecipientsView.getDisclaimerFr().length() > 0) {
				body = body
						+ "\r"
						+ SpicewebConstants.getParam(xrcEmailRecipientsView
								.getDisclaimerFr().trim());
			}
		}
		if (subject != null && subject.length() > 0) {
			ArrayList<String> keywords = getKeywords(subject);
			String keywordStr = "";
			if (keywords != null && keywords.size() > 0) {
				for (String keyword : keywords) {
					// $CONC_NO/$ISSUE/$CIVIL_MILITARY/$PARTY_TYPE/$AC_TYPE/$GEAR
					if (keyword.equals("CONC_NO")) {
						String concNum = "";
						if (sp != null && sp.getConcessionNo() != null) {
							concNum = sp.getConcessionNo();
						}
						subject = subject.replace("{" + keyword + "}", concNum);

					} else if (keyword.equals("ISSUE")) {
						subject = subject.replace("{" + keyword + "}",
								getConcIssueNo(sp.getConcId()));

					} else if (keyword.equals("CIVIL_MILITARY")) {
						subject = subject.replace("{" + keyword + "}",
								(sp.isMilitary() ? "M" : "C"));

					} else if (keyword.equals("PARTY_TYPE")) {
						subject = subject.replace("{" + keyword + "}", "A");

					} else if (keyword.equals("REV")) {
						try {
							subject = subject.replace("{" + keyword + "}",
									getConcRevision(sp.getConcId(), "SHORT"));
						} catch (Exception ex) {
							logger.error(ex);
						}
					} else if (keyword.equals("AC_TYPE")) {
						try {
							subject = subject.replace("{" + keyword + "}",
									getAcTypeDocGen(sp.getConcId()));
						} catch (Exception ex) {
							logger.error(ex);
						}
					} else if (keyword.equals("GEAR")) {
						try {
							subject = subject.replace("{" + keyword + "}",
									getGearDocGen(sp.getConcId()));
						} catch (Exception ex) {
							logger.error(ex);
						}
					}
				}
			}
			props.put("SUBJECT", subject);
			logger.debug("Subject-------------->>>>>>>> :" + subject);
		}
		if (body != null && body.length() > 0) {
			props.put("BODY", body);
			logger.debug("body-------------->>>>>> :" + body);
		}
		return props;
	}

	@Override
	public HashMap<String, String> getMailProperties(int eventId, long concId)
			throws SQLException {
		HashMap<String, String> props = null;
		// get the event

		XrcEventDefinitions event = getMyConcessionDao()
				.getXrcEventDefinitions(eventId);
		if (event.getXrcSendMsg() != null
				&& event.getXrcSendMsg().equals("Customer")) {
			props = getCustMail(eventId, concId);
			logger.debug("Mail Properties---> : " + props);
		} else if (event.getXrcSendMsg() != null
				&& event.getXrcSendMsg().equals("Engineer")) {
			ArrayList<XrcDiaryEntries> entries = getMyConcessionDao()
					.getMaxDiaryEntries(concId);
			if (entries != null && entries.size() > 0) {
				XrcDiaryEntries diary1 = entries.get(0);
				logger.debug("diary1----->> concId:"
						+ diary1.getXrcConcessionId());
				int usrId = getMyConcessionDao().getRCContact(
						diary1.getXrcConcessionId());
				Map emailSMSMap = getMyConcessionDao().getEmailSmsMap(usrId);
				logger.debug("emailSMSMap :" + emailSMSMap
						+ "-------concId--->>>>> "
						+ diary1.getXrcConcessionId());
				// get user from xia and get the email address and
				// send

				if (emailSMSMap != null && emailSMSMap.size() > 0) {
					props = getUsrMail(diary1, usrId);
					if (!((String) emailSMSMap.get("ismail")).equals("Y")) {
						props.remove("MAIL_TO");
					}
					if (!((String) emailSMSMap.get("istext")).equals("Y")) {
						props.remove("TEXT_TO");
					}

				}
			}
		}

		return props;

	}

	@Override
	public XxmSpConcessions fetchConcessionById(Long concId)
			throws SQLException {
		return getMyConcessionDao().fetchConcessionById(concId);
	}

	public Double getUSDPrice(Long custId, Long partId) throws SQLException {
		return getMyConcessionDao().getUSDPrice(custId, partId);
	}

	public String getRCNumber(Integer acType, Integer gearType, String site)
			throws SQLException {
		return getMyConcessionDao().getRCNumber(acType, gearType, site);
	}

	public Boolean isBAECondition(Long concId) throws SQLException {
		return getMyConcessionDao().isBAECondition(concId);
	}

	@Override
	public String getDocumentTypeById(int docTypeId) throws SQLException {
		return getMyConcessionDao().getDocumentTypeById(docTypeId);
	}
	
	@Override
	public XrcDocumentTypes getDocumentTypeByCode(String docTypeCode) throws SQLException {
		return getMyConcessionDao().getDocumentTypeByCode(docTypeCode);
	}	
	
	/*
	 * @Override public String chargeAndProcess(Long concId) throws SQLException
	 * { // TODO Auto-generated method stub return
	 * getMyConcessionDao().chargeAndProcess(concId); }
	 */

	@Override
	public int chargeAndProcess(Long concId) throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().chargeAndProcess(concId);
	}

	@Override
	public XrcDiaryEntries getLatestEvent(Long concId, String eventName)
			throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().getLatestEvent(concId, eventName);

	}

	@Override
	public String getConcIssueNo(Long concId) throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().getConcIssueNo(concId);

	}

	@Override
	public ArrayList<DocumentPack> fetchDocumentPack(DocumentPack pTmpDocPackTbl)
			throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().fetchDocumentPack(pTmpDocPackTbl);
	}

	@Override
	public ArrayList<DocumentPack> fetchDialogDocuments(
			DialogDocuments pTmpDocPackTbl) throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().fetchDialogDocuments(pTmpDocPackTbl);
	}

	@Override
	public ArrayList<String> getTemplateNames(ArrayList<String> docTypes) {
		return getMyConcessionDao().getTemplateNames(docTypes);
	}

	@Override
	public String getRTEValidationLevelCode(Long concId) throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().getRTEValidationLevelCode(concId);
	}

	public String getUserRTEValidationLevelCode(Long concId, Integer userId)
			throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().getUserRTEValidationLevelCode(concId,
				userId);
	}

	@Override
	public String getRTEValidationDate(Long concId) throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().getRTEValidationDate(concId);
	}

	@Override
	public Integer getRTEValidationUser(Long concId) throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().getRTEValidationUser(concId);
	}

	@Override
	public ArrayList<XrcAircraftGroup> fetchAircraftGroup(
			XrcAircraftGroup pXrcAircraftGroup) throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().fetchAircraftGroup(pXrcAircraftGroup);
	}

	/*
	 * public boolean checkPreassessCondition(Long concId) throws SQLException{
	 * XxmSpConcessions pXxmSpConcessions = new XxmSpConcessions();
	 * pXxmSpConcessions = getMyConcessionDao().fetchConcessionById(concId);
	 * ArrayList<Integer> eventIdList = new ArrayList<Integer>(); int eventId=0;
	 * XrcDiaryEntries entry = new XrcDiaryEntries();
	 * 
	 * boolean newBaseData = false; // check if new parts then change the event
	 * to added new base data. if (pXxmSpConcessions.getNewPartNo() != null ||
	 * pXxmSpConcessions.getNewParentPart() != null ||
	 * pXxmSpConcessions.getNewOperatorName() != null) { newBaseData = true; }
	 * 
	 * if (pXxmSpConcessions.getIsCmmRepairScheme() != null &&
	 * pXxmSpConcessions.getIsCmmRepairScheme().equals("Y")) { eventId =
	 * XrcEventDefinitions.REPAIR_SCHEME_EXISTS; entry.setXrcDiaryEntry(
	 * "System Generated Event: Repair Scheme Exists for the Concession : "
	 * +pXxmSpConcessions.getConcId());
	 * entry.setXrcConcessionId(pXxmSpConcessions.getConcId());
	 * entry.setXrcDiaryEntryDate(new Timestamp(System.currentTimeMillis()));
	 * entry.setXrcEventId(eventId); // for submission eventIdList.add(eventId);
	 * createDiaryEvent(entry); } else if (newBaseData) { eventId =
	 * XrcEventDefinitions.CUSTOMER_ADDED_NEW_BASE_DATA; entry.setXrcDiaryEntry(
	 * "System Generated Event: Customer has added the new base data for the Concession"
	 * ); entry.setXrcConcessionId(pXxmSpConcessions.getConcId());
	 * entry.setXrcDiaryEntryDate(new Timestamp(System.currentTimeMillis()));
	 * entry.setXrcEventId(eventId); // for submission eventIdList.add(eventId);
	 * createDiaryEvent(entry); } else { Double price = getUsdPrice(
	 * pXxmSpConcessions.getPartId()); Integer stock =
	 * getMyConcessionDao().getStock( (long)pXxmSpConcessions.getPartId());
	 * //int stock = get String pThreshold =
	 * SpicewebConstants.getParam("XRC_PRICE_THRESHOLD"); String sThreshold =
	 * SpicewebConstants.getParam("XRC_STOCK_THRESHOLD"); if (pThreshold == null
	 * ){ pThreshold = "100.00"; } else if (pThreshold.indexOf(".") < 0){
	 * pThreshold += "0.0"; } if (sThreshold == null ){ sThreshold = "1"; }
	 * Double priceThreshold = new Double(pThreshold); Integer stockThreshold =
	 * new Integer(sThreshold); //int stockThreshold = new
	 * Integer(SpicewebConstants.getParam("﻿XRC_STOCK_THRESHOLD"));
	 * 
	 * stock = stock!=null?stock:0;
	 * 
	 * logger.debug("Price of part "+ pXxmSpConcessions.getPartId()+
	 * " for customer : "+ pXxmSpConcessions.getCustomerId() + " ="+price+
	 * " against "+ priceThreshold); logger.debug("Stock of part "+
	 * pXxmSpConcessions.getPartId()+ " for customer : "+
	 * pXxmSpConcessions.getCustomerId() + " ="+stock+ " against "+
	 * stockThreshold);
	 * 
	 * if(0 < price && price<=priceThreshold){ eventId =
	 * XrcEventDefinitions.PRICE_BELOW_THRESHOLD; //
	 * entry.setXrcDiaryEntry("System Generated Event: Price of part "+
	 * bean.getPartId()+ " for customer : "+
	 * bean.getCustNamestr()+"("+bean.getCustomerId()+")" + " ="+price+
	 * " against "+ priceThreshold);
	 * entry.setXrcDiaryEntry("System Generated Event: part no."
	 * +pXxmSpConcessions.getPartNum()+". price "+price
	 * +". threshold set at "+priceThreshold);
	 * entry.setXrcConcessionId(pXxmSpConcessions.getConcId());
	 * entry.setXrcDiaryEntryDate(new Timestamp(System.currentTimeMillis()));
	 * entry.setXrcEventId(eventId); // for submission eventIdList.add(eventId);
	 * createDiaryEvent(entry); } if(stock>stockThreshold){ eventId =
	 * XrcEventDefinitions.STOCK_ABOVE_THRESHOLD; //
	 * entry.setXrcDiaryEntry("System Generated Event: Stock of part "+
	 * bean.getPartId()+ " for customer : "+
	 * bean.getCustNamestr()+"("+bean.getCustomerId()+")" + " ="+stock+
	 * " against "+ stockThreshold);
	 * entry.setXrcDiaryEntry("System Generated Event: part no."
	 * +pXxmSpConcessions.getPartNum()+". stock level "+stock
	 * +". threshold set at "+stockThreshold);
	 * entry.setXrcConcessionId(pXxmSpConcessions.getConcId());
	 * entry.setXrcDiaryEntryDate(new Timestamp(System.currentTimeMillis()));
	 * entry.setXrcEventId(eventId); // for submission eventIdList.add(eventId);
	 * createDiaryEvent(entry); } if(0 < price && price<=priceThreshold &&
	 * stock>stockThreshold){ eventId =
	 * XrcEventDefinitions.NON_ECONOMICAL_REPAIR; entry.setXrcDiaryEntry(
	 * "System Generated Event: The stock and price thresholds have been breached. Thus this is a non economical repair."
	 * ); entry.setXrcConcessionId(pXxmSpConcessions.getConcId());
	 * entry.setXrcDiaryEntryDate(new Timestamp(System.currentTimeMillis()));
	 * entry.setXrcEventId(eventId); // for submission eventIdList.add(eventId);
	 * createDiaryEvent(entry); }
	 * 
	 * }
	 * 
	 * // check the price and stock also ArrayList<Integer> preAssessList = new
	 * ArrayList<Integer>();
	 * preAssessList.add(XrcEventDefinitions.CUSTOMER_ADDED_NEW_BASE_DATA);
	 * preAssessList.add(XrcEventDefinitions.REPAIR_SCHEME_EXISTS);
	 * preAssessList.add(XrcEventDefinitions.PRICE_BELOW_THRESHOLD);
	 * preAssessList.add(XrcEventDefinitions.STOCK_ABOVE_THRESHOLD);
	 * preAssessList.add(XrcEventDefinitions.NON_ECONOMICAL_REPAIR);
	 * 
	 * boolean isPreassessConc = false; for(int i=0;i<eventIdList.size();i++){
	 * eventId = eventIdList.get(i); if(preAssessList.contains(eventId)){
	 * isPreassessConc = true; break; } }
	 * 
	 * if(!isPreassessConc){ return false; }else return true; }
	 */

	public boolean checkPreassessCondition(Long concId) throws SQLException {
		XxmSpConcessions pXxmSpConcessions = new XxmSpConcessions();
		pXxmSpConcessions = getMyConcessionDao().fetchConcessionById(concId);
		ArrayList<Integer> eventIdList = new ArrayList<Integer>();
		int eventId = 0;
		int maxEventId = getMyConcessionDao().getMaxEvent(concId);
		XrcDiaryEntries entry = new XrcDiaryEntries();
		// if the program is DO328NLG then return true.
		if (pXxmSpConcessions.getAcType() != null
				&& pXxmSpConcessions.getAcType() == 68
				&& pXxmSpConcessions.getGearType() != null
				&& pXxmSpConcessions.getGearType() == 1) {
			return true; // checking DO328 NLG condition according to the task
							// EW-281
		}
		boolean newBaseData = false;
		// check if new parts then change the event to added new base data.
		if (pXxmSpConcessions.getNewPartNo() != null
				|| pXxmSpConcessions.getNewParentPart() != null
				|| pXxmSpConcessions.getNewOperatorName() != null) {
			newBaseData = true;
		}

		eventId = XrcEventDefinitions.REPAIR_SCHEME_EXISTS;
		// Also check if this preassessment event already exist as previous
		// event
		if (pXxmSpConcessions.getIsCmmRepairScheme() != null
				&& pXxmSpConcessions.getIsCmmRepairScheme().equals("Y")
				&& maxEventId != eventId) {
			entry.setXrcDiaryEntry("System Generated Event: Repair Scheme Exists for the Concession : "
					+ pXxmSpConcessions.getConcId());
			entry.setXrcConcessionId(pXxmSpConcessions.getConcId());
			entry.setXrcDiaryEntryDate(new Timestamp(System.currentTimeMillis()));
			entry.setXrcEventId(eventId); // for submission
			eventIdList.add(eventId);
			createDiaryEvent(entry);

		} else if (newBaseData) {
			eventId = XrcEventDefinitions.CUSTOMER_ADDED_NEW_BASE_DATA;
			entry.setXrcDiaryEntry("System Generated Event: Customer has added the new base data for the Concession");
			entry.setXrcConcessionId(pXxmSpConcessions.getConcId());
			entry.setXrcDiaryEntryDate(new Timestamp(System.currentTimeMillis()));
			entry.setXrcEventId(eventId); // for submission
			eventIdList.add(eventId);
			createDiaryEvent(entry);
		} else {
			Double price = getUsdPrice(pXxmSpConcessions.getPartId());
			Integer stock = getMyConcessionDao().getStock(
					(long) pXxmSpConcessions.getPartId());
			// int stock = get
			String pThreshold = SpicewebConstants
					.getParam("XRC_PRICE_THRESHOLD");
			String sThreshold = SpicewebConstants
					.getParam("XRC_STOCK_THRESHOLD");
			if (pThreshold == null) {
				pThreshold = "100.00";
			} /*
			 * else if (pThreshold.indexOf(".") < 0){ pThreshold += "0.0"; }
			 */
			if (sThreshold == null) {
				sThreshold = "1";
			}
			Double priceThreshold = new Double(pThreshold);
			Integer stockThreshold = new Integer(sThreshold);
			// int stockThreshold = new
			// Integer(SpicewebConstants.getParam("﻿XRC_STOCK_THRESHOLD"));

			stock = stock != null ? stock : 0;

			logger.debug("Price of part " + pXxmSpConcessions.getPartId()
					+ " for customer : " + pXxmSpConcessions.getCustomerId()
					+ " =" + price + " against " + priceThreshold);
			logger.debug("Stock of part " + pXxmSpConcessions.getPartId()
					+ " for customer : " + pXxmSpConcessions.getCustomerId()
					+ " =" + stock + " against " + stockThreshold);

			// Commented based on Raj's clarification on 9-Jun-2014 - by raji
			// Opened because of Raj's clarificatio on 11-Jun-2014 - by Jay
			price = price != null ? price : 0;
			if (0 < price && price <= priceThreshold) {
				eventId = XrcEventDefinitions.PRICE_BELOW_THRESHOLD;
				// entry.setXrcDiaryEntry("System Generated Event: Price of part "+
				// bean.getPartId()+ " for customer : "+
				// bean.getCustNamestr()+"("+bean.getCustomerId()+")" +
				// " ="+price+ " against "+ priceThreshold);
				entry.setXrcDiaryEntry("System Generated Event: part no."
						+ pXxmSpConcessions.getPartNum() + ". price " + price
						+ ". threshold set at " + priceThreshold);
				entry.setXrcConcessionId(pXxmSpConcessions.getConcId());
				entry.setXrcDiaryEntryDate(new Timestamp(System
						.currentTimeMillis()));
				entry.setXrcEventId(eventId); // for submission
				eventIdList.add(eventId);
				createDiaryEvent(entry);
			}
			if (stock > stockThreshold) {
				eventId = XrcEventDefinitions.STOCK_ABOVE_THRESHOLD;
				// entry.setXrcDiaryEntry("System Generated Event: Stock of part "+
				// bean.getPartId()+ " for customer : "+
				// bean.getCustNamestr()+"("+bean.getCustomerId()+")" +
				// " ="+stock+ " against "+ stockThreshold);
				entry.setXrcDiaryEntry("System Generated Event: part no."
						+ pXxmSpConcessions.getPartNum() + ". stock level "
						+ stock + ". threshold set at " + stockThreshold);
				entry.setXrcConcessionId(pXxmSpConcessions.getConcId());
				entry.setXrcDiaryEntryDate(new Timestamp(System
						.currentTimeMillis()));
				entry.setXrcEventId(eventId); // for submission
				eventIdList.add(eventId);
				createDiaryEvent(entry);
			}
			eventId = XrcEventDefinitions.NON_ECONOMICAL_REPAIR;
			// Also Check if this preassessment event already exist as previous
			// event
			if (0 < price && price <= priceThreshold && stock > stockThreshold
					&& maxEventId != eventId) {

				entry.setXrcDiaryEntry("System Generated Event: The stock and price thresholds have been breached. Thus this is a non economical repair.");
				entry.setXrcConcessionId(pXxmSpConcessions.getConcId());
				entry.setXrcDiaryEntryDate(new Timestamp(System
						.currentTimeMillis()));
				entry.setXrcEventId(eventId); // for submission
				eventIdList.add(eventId);
				createDiaryEvent(entry);

			}

		}

		// check the price and stock also
		ArrayList<Integer> preAssessList = new ArrayList<Integer>();
		preAssessList.add(XrcEventDefinitions.CUSTOMER_ADDED_NEW_BASE_DATA);
		preAssessList.add(XrcEventDefinitions.REPAIR_SCHEME_EXISTS);
		// Commented based on Raj's clarification on 9-Jun-2014 - by raji
		// preAssessList.add(XrcEventDefinitions.PRICE_BELOW_THRESHOLD);
		// preAssessList.add(XrcEventDefinitions.STOCK_ABOVE_THRESHOLD);
		preAssessList.add(XrcEventDefinitions.NON_ECONOMICAL_REPAIR);

		boolean isPreassessConc = false;
		for (int i = 0; i < eventIdList.size(); i++) {
			eventId = eventIdList.get(i);
			if (preAssessList.contains(eventId)) {
				isPreassessConc = true;
				// break;
			}
		}

		if (!isPreassessConc) {
			return false;
		} else
			return true;
	}

	@Override
	public List<XrcEmailRecipientsView> getMailRecipients(int eventId,
			long concId) throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().getMailRecipients(eventId, concId);
	}

	@Override
	public Integer getMaxEvent(Long concId) throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().getMaxEvent(concId);
	}

	@Override
	public int saveEmailSentEntry(EmailSentEntry emailSentEntry)
			throws SQLException {
		return getMyConcessionDao().saveEmailSentEntry(emailSentEntry);
	}

	@Override
	public boolean checkATRAircraft(Long concId) throws SQLException {
		return getMyConcessionDao().checkATRAircraft(concId);
	}

	@Override
	public XxmSpConcessions getConcession(Long concId) throws SQLException {
		return getMyConcessionDao().getConcession(concId);
	}

	@Override
	public int rollBackEntries(Long concId, int eventId) throws SQLException {
		return getMyConcessionDao().rollBackEntries(concId, eventId);
	}

	@Override
	public String getConcRevision(Long concId, String revType)
			throws SQLException {
		return getMyConcessionDao().getConcRevision(concId, revType);
	}

	@Override
	public int getXrcEventDefinitionId(String strEventDesc) {
		return getMyConcessionDao().getXrcEventDefinitionId(strEventDesc);
	}

	@Override
	public ArrayList<XrcAcData> fetchJarFarsReq(Long concId)
			throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().fetchJarFarsReq(concId);
	}

	@Override
	public String getCSCEmail(Long concId) throws SQLException {
		return getMyConcessionDao().getCSCEmail(concId);
	}

	@Override
	public String getSubClass(Long concId) throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().getSubClass(concId);

	}

	@Override
	public XxmSpConcessions save(XxmSpConcessions cc) throws SQLException {
		return getMyCustomerConcessionDao().save(cc);
	}

	public ArrayList<XrcDocuments> fetchViewDoc(XrcDocuments pXrcDocuments)
			throws SQLException {
		return getMyConcessionDao().fetchViewDoc(pXrcDocuments);
	}
	
	public ArrayList<XrcDocuments> fetchDocxDoc(XrcDocuments pXrcDocuments)
			throws SQLException {
		return getMyConcessionDao().fetchDocxDoc(pXrcDocuments);
	}
	
	public String getUsrJobTitle(Integer usrIds) throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().getUsrJobTitle(usrIds);
	}

	public ArrayList<XxmSpUsers> fetchRteUsrPdfGen(Integer userId)
			throws SQLException {
		return getMyConcessionDao().fetchRteUsrPdfGen(userId);
	}

	@Override
	public ArrayList<XrcDocuments> fetchAttachDoc(XrcDocuments pXrcDocuments)
			throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().fetchAttachDoc(pXrcDocuments);
	}

	@Override
	public String getAcTypeDocGen(Long concId) throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().getAcTypeDocGen(concId);
	}

	@Override
	public String getGearDocGen(Long concId) throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().getGearDocGen(concId);
	}

	@Override
	public String getIsNewWorkflow(Long concId) throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().getIsNewWorkflow(concId);

	}

	@Override
	public ArrayList<OnbaseHistoricDocument> fetchHistoricDocument(
			OnbaseHistoricDocument pOnbaseHistoricDocument) throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().fetchHistoricDocument(
				pOnbaseHistoricDocument);
	}
	
	public String fetchUsrMail(Integer userId)
	throws SQLException {
		return getMyConcessionDao().fetchUsrMail(userId);
	}
	

	public ArrayList<String> getKeywords(String subject) {
		ArrayList<String> keywords = new ArrayList();
		String keyword = "";
		boolean isStarted = false;
		if (subject != null) {
			for (char c : subject.toCharArray()) {
				if (c == '}') {
					keywords.add(keyword);
					keyword = "";
					isStarted = false;
				}
				if (isStarted) {

					keyword += c;
				}
				if (c == '{') {
					isStarted = true;
					keyword = "";
				}
			}
		}
		return keywords;
	}

	@Override
	public int saveDoc(XrcDocuments doc) throws SQLException {
		return getMyCustomerConcessionDao().saveDoc(doc);
	}

	public Long getXrcRcId(Long xrcId) throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().getXrcRcId(xrcId);
	}

	@Override
	public ArrayList<CustomerConcessions> fetchCustConcessions(
			CustomerConcessions pCustomerConcessions) throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().fetchCustConcessions(pCustomerConcessions);
	}

	@Override
	public int update(CustomerConcessions pCustomerConcessions)
			throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().update(pCustomerConcessions);
	}

	@Override
	public String getXxmSpUserName(Integer usrId) throws SQLException {
		// TODO Auto-generated method stub
		return getMyConcessionDao().getXxmSpUserName(usrId);
	}

	@Override
	public ArrayList<ConcessionClassAttributes> fetch(Long concId) throws SQLException {
		return getMyConcessionDao().fetch(concId);
	}
	
	@Override
	public int[] save(ArrayList<XrcEngClassAttributes> pXrcEngClassAttributes){
		return getMyConcessionDao().save(pXrcEngClassAttributes);
	}
	
	@Override
	public int[] update (ArrayList<XrcEngClassAttributes> pXrcEngClassAttributes) throws SQLException{
		return getMyConcessionDao().update(pXrcEngClassAttributes);
	}
	
	@Override
	public int update (XrcEngClassAttributes pXrcEngClassAttributes) throws SQLException{
		return getMyConcessionDao().update(pXrcEngClassAttributes);
	}
	
	@Override
	public ArrayList<XrcEngClassAttributes> fetch (XrcEngClassAttributes pXrcEngClassAttributes) throws SQLException{
		return getMyConcessionDao().fetch(pXrcEngClassAttributes);
	}
	
	@Override
	public int refreshClassAttr(Long concId) throws SQLException{
		return getMyConcessionDao().refreshClassAttr(concId);
	}
	
	@Override
	public int[] saveHistory(ArrayList<XrcEngClassAttributes> pXrcEngClassAttributes,Long engHistoryId) {
		return getMyConcessionDao().saveHistory(pXrcEngClassAttributes,engHistoryId);
	}
	
	@Override
	public Long saveHistory(XrcEngineeringAttributes pXrcEngineeringAttributes) throws SQLException{
		return getMyConcessionDao().saveHistory(pXrcEngineeringAttributes);
	}
	
	public String getConcessionsDirPath() {
		return concessionsDirPath;
	}

	public void setConcessionsDirPath(String concessionsDirPath) {
		this.concessionsDirPath = concessionsDirPath;
	}

	public String getInDraftFolder() {
		return inDraftFolder;
	}

	public void setInDraftFolder(String inDraftFolder) {
		this.inDraftFolder = inDraftFolder;
	}
	
	public ArrayList<XrcSites> fetchHisSites(XrcSites pXrcSites)
	throws SQLException {
	return getMyConcessionDao().fetchHisSites(pXrcSites); 
}

	@Override
	public Integer isCustomerMapped(Integer id) throws SQLException {
		
		return getMyConcessionDao().isCustomerMapped(id);
	}
	
	@Override
	public Integer isPartMapped(Integer partId) throws SQLException {
		
		return getMyConcessionDao().isPartMapped(partId);
				
	}
	
	@Override
	public ArrayList<CMM> getXrcCmmList(Integer cmmId) throws SQLException {
		return getMyConcessionDao().getXrcCmmList(cmmId);
	}

	@Override
	public XiaSpUser getUser(Integer usrId) throws SQLException {
		return getMyConcessionDao().getUser(usrId);
	}

	@Override
	public Map getEmailSmsMap(Integer usrId) throws SQLException {
		return getMyConcessionDao().getEmailSmsMap(usrId);
	}

	@Override
	public List<XrcDocuments> fetchFailedOnbaseDmzDocs() throws SQLException {
		return getMyConcessionDao().fetchFailedOnbaseDmzDocs();
	}
	
}