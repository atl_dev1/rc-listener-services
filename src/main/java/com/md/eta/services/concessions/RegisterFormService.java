package com.md.eta.services.concessions;

import java.sql.SQLException;
import java.util.ArrayList;

import com.md.eta.model.concessions.RegisterForm;
import com.md.eta.model.concessions.TechPublication;
import com.md.eta.model.portal.users.XxmSpUsers;


public interface RegisterFormService {

	public Long save(RegisterForm registerForm) throws SQLException;
	
	public ArrayList<RegisterForm> fetchRegister(RegisterForm registerForm) throws Exception;
	
	public Long saveTechPublic(TechPublication techPublication) throws SQLException;
	
	public Long count(RegisterForm registerForm) throws SQLException;
	
	public Long update(RegisterForm registerForm) throws SQLException;
	
	public RegisterForm getOldUserRegister(RegisterForm registerForm) throws Exception;
	
	public XxmSpUsers getUsers(RegisterForm registerForm) throws Exception;
	
}
