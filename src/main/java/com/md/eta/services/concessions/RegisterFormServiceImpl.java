package com.md.eta.services.concessions;

import java.sql.SQLException;
import java.util.ArrayList;

import com.md.eta.model.concessions.RegisterForm;
import com.md.eta.model.concessions.TechPublication;
import com.md.eta.model.portal.users.XxmSpUsers;
import com.md.eta.services.AbstractService;

public class RegisterFormServiceImpl extends AbstractService implements RegisterFormService {

	@Override
	public Long save(RegisterForm registerForm) throws SQLException {
		return getRegisterFormDao().save(registerForm);
	}

	@Override
	public ArrayList<RegisterForm> fetchRegister(RegisterForm registerForm) throws Exception {
		return getRegisterFormDao().fetchRegister(registerForm);
	}

	@Override
	public Long saveTechPublic(TechPublication techPublication) throws SQLException {
		return getRegisterFormDao().saveTechPublic(techPublication);
	}

	@Override
	public Long count(RegisterForm registerForm) throws SQLException {
		return getRegisterFormDao().count(registerForm);
	}

	@Override
	public Long update(RegisterForm registerForm) throws SQLException {
		return getRegisterFormDao().update(registerForm);
	}

	@Override
	public RegisterForm getOldUserRegister(RegisterForm registerForm)
			throws Exception {
		return getRegisterFormDao().getOldUserRegister(registerForm);
	}

	@Override
	public XxmSpUsers getUsers(RegisterForm registerForm) throws Exception {
		return getRegisterFormDao().getUsers(registerForm);
	}
}
