/**
 * 
 */
package com.md.eta.services.concessions;

import java.sql.SQLException;
import java.util.ArrayList;

import com.md.eta.model.concessions.XxmSpAuditDocs;

/**
 * @author Raj Varadarajan
 * @date Oct 28, 2011
 * com.md.spiceweb.services
 * 
 */
public interface XxmSpAuditDocsService {

	
	public int save(XxmSpAuditDocs pXxmSpAuditDocs);
	
	public ArrayList<XxmSpAuditDocs> fetch (XxmSpAuditDocs pXxmSpAuditDocs) throws SQLException;
	
	

}
