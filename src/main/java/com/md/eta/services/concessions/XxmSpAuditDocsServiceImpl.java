/**
 * 
 */
package com.md.eta.services.concessions;

import java.sql.SQLException;
import java.util.ArrayList;

import com.md.eta.model.concessions.XxmSpAuditDocs;
import com.md.eta.services.AbstractService;

/**
 * @author Raj Varadarajan
 * @date Oct 28, 2011
 * com.md.spiceweb.services
 * 
 */
public class XxmSpAuditDocsServiceImpl extends AbstractService implements XxmSpAuditDocsService {

	/* (non-Javadoc)
	 * @see com.md.spiceweb.services.XxmSpAuditDocsService#fetch(com.md.spiceweb.domain.concessions.XxmSpAuditDocs)
	 */
	
	public ArrayList<XxmSpAuditDocs> fetch(XxmSpAuditDocs pXxmSpAuditDocs)
			throws SQLException {
		// TODO Auto-generated method stub
		return getXxmSpAuditDocsDao().fetch(pXxmSpAuditDocs);
	}

	/* (non-Javadoc)
	 * @see com.md.spiceweb.services.XxmSpAuditDocsService#save(com.md.spiceweb.domain.concessions.XxmSpAuditDocs)
	 */
	
	public int save(XxmSpAuditDocs pXxmSpAuditDocs) {
		// TODO Auto-generated method stub
		return getXxmSpAuditDocsDao().save(pXxmSpAuditDocs);
	}

}
