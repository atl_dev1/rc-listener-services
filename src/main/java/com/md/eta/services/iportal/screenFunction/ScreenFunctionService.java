package com.md.eta.services.iportal.screenFunction;
import java.sql.SQLException;
import java.util.ArrayList;

import com.md.eta.model.admin.ScreenFunction;


public interface ScreenFunctionService {

	
	public  ArrayList<ScreenFunction> fetch(ScreenFunction screenFunction) throws SQLException;
       
	public abstract int update(ScreenFunction screenFunction) throws SQLException;
    
	public abstract int  save(ScreenFunction screenFunction) throws SQLException;

	public  Long getCount(ScreenFunction screenFunction) throws SQLException;

	public ArrayList<ScreenFunction> getAllFunctions(ScreenFunction screenFunction)  throws SQLException;
	
	public ArrayList<ScreenFunction> getParentFunction(ScreenFunction screenFunction)  throws SQLException;

	public ArrayList<ScreenFunction> getDefaultFunctions(ScreenFunction screenFunction) throws SQLException;
	
	public boolean isFunctionExists(Integer fnId) throws SQLException;

}