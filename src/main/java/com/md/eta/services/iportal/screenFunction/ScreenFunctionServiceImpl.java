package com.md.eta.services.iportal.screenFunction;

import java.sql.SQLException;
import java.util.ArrayList;

import com.md.eta.model.admin.ScreenFunction;
import com.md.eta.services.AbstractService;




public class ScreenFunctionServiceImpl extends AbstractService implements ScreenFunctionService {

	@Override
	public ArrayList<ScreenFunction> fetch(ScreenFunction screenFunction)throws SQLException {
		return getScreenFunctionsDao().fetch(screenFunction);
		
	}
	@Override
	public int update(ScreenFunction screenFunction ) throws SQLException {
		return getScreenFunctionsDao().update(screenFunction);
	}

	@Override
	public int save(ScreenFunction screenFunction) throws SQLException {
		return getScreenFunctionsDao().save(screenFunction);
	}
	@Override
	public Long getCount(ScreenFunction screenFunction) throws SQLException{
		return getScreenFunctionsDao().getCount(screenFunction);
		
	}
	@Override
	public ArrayList<ScreenFunction> getParentFunction(ScreenFunction screenFunction) throws SQLException {
		return getScreenFunctionsDao().getParentFunction(screenFunction);

	}
	
	@Override
	public ArrayList<ScreenFunction> getAllFunctions(ScreenFunction screenFunction)throws SQLException {
		return getScreenFunctionsDao().getAllFunctions(screenFunction);
		
	}
	@Override
	public  ArrayList<ScreenFunction> getDefaultFunctions(
			ScreenFunction screenFunction) throws SQLException {
		return  getScreenFunctionsDao().getDefaultFunctions(screenFunction);
	}
	@Override
	public boolean isFunctionExists(Integer fnId) throws SQLException {
		// TODO Auto-generated method stub
		return getScreenFunctionsDao().isFunctionExists(fnId);
	}
}