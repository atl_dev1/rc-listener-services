package com.md.eta.services.iportal.users;

import java.sql.SQLException;
import java.util.ArrayList;

import com.md.eta.model.iportal.users.XiaSpUser;
import com.md.eta.model.iportal.users.XiaSpUserFunctions;
import com.md.eta.model.iportal.users.XiaSpUserNotifyPrefs;
import com.md.eta.model.iportal.users.XrcConcessionRoles;


public interface XiaSpUserService {

	public abstract Long getCount(XiaSpUser pXxmSpUsers) throws SQLException;

	public abstract int update(XiaSpUser pXxmSpUsers) throws SQLException;

	public abstract int save(XiaSpUser pXxmSpUsers) throws SQLException;

	public abstract ArrayList<XiaSpUser> fetch(XiaSpUser pXxmSpUsers) throws SQLException;

	public abstract int delete(XiaSpUser pXxmSpUsers) throws SQLException;
	
	public abstract ArrayList<XiaSpUserFunctions> getUserFunctions(XiaSpUserFunctions pUserFunctions) throws SQLException;
	
	/*----------Notification Preference-------------*/
	
	public ArrayList<XiaSpUserNotifyPrefs> getNotifyPrefType(int usrId) throws SQLException;
	
	public int notifyPrefSave(XiaSpUserNotifyPrefs pXiaSpUserNotifyPrefs) throws SQLException;
	
	public int deleteNotification(XiaSpUserNotifyPrefs pXiaSpUserNotifyPrefs) throws SQLException;
	public abstract int deAllocateFunction(XiaSpUserFunctions uf) throws SQLException;
	
	public Long getCountNotify(XiaSpUserNotifyPrefs pXiaSpUserNotifyPrefs)throws SQLException;
	
	public ArrayList<XiaSpUserNotifyPrefs> getNotification(XiaSpUserNotifyPrefs pXiaSpUserNotifyPrefs) throws SQLException;

	/*----------Password Algorithm-------------*/
	public String generatePassword(XiaSpUser xiaSpUser) throws Exception;

	public abstract int allocateFunction(XiaSpUserFunctions uf) throws SQLException;

	/*----------RC Profile-------------*/
	public ArrayList<XrcConcessionRoles> getUserRoles(Integer usrId) throws SQLException;

	public int saveUserRole(XiaSpUser xiaSpUser) throws SQLException;
	
	public int deleteUserRoles(int usrId) throws SQLException;
	
	public boolean checkUserRoleExists(int usrId) throws SQLException;
	
	public ArrayList<XiaSpUser> getUser (XiaSpUser xiaSpUser) throws SQLException;
	
	public Long getUserFunctionsCount(XiaSpUserFunctions pUserFunctions)  throws SQLException;
	
	public int approveUser(XiaSpUser xiaSpUser)  throws SQLException;

	ArrayList<XiaSpUserNotifyPrefs> getUserNotifications(int usrId)
			throws SQLException;
	
	public int updateRetrys(Integer usrId) throws SQLException;
}