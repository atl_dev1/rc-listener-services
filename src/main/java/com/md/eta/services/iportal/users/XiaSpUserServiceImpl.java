package com.md.eta.services.iportal.users;

import java.sql.SQLException;
import java.util.ArrayList;

import com.md.eta.model.iportal.users.XiaSpUser;
import com.md.eta.model.iportal.users.XiaSpUserFunctions;
import com.md.eta.model.iportal.users.XiaSpUserNotifyPrefs;
import com.md.eta.model.iportal.users.XrcConcessionRoles;
import com.md.eta.services.AbstractService;

public class XiaSpUserServiceImpl extends AbstractService implements
		XiaSpUserService {

	@Override
	public ArrayList<XiaSpUser> fetch(XiaSpUser pXxmSpUsers)
			throws SQLException {
		return getXiaSpUserDao().fetch(pXxmSpUsers);
	}

	@Override
	public int save(XiaSpUser pXxmSpUsers) throws SQLException {
		// TODO Auto-generated method stub
		return getXiaSpUserDao().save(pXxmSpUsers);
	}

	@Override
	public int update(XiaSpUser pXxmSpUsers) throws SQLException {
		// TODO Auto-generated method stub
		return getXiaSpUserDao().update(pXxmSpUsers);
	}

	@Override
	public Long getCount(XiaSpUser pXxmSpUsers) throws SQLException {
		// TODO Auto-generated method stub
		return getXiaSpUserDao().getCount(pXxmSpUsers);
	}
	
	@Override
	public int delete(XiaSpUser pXxmSpUsers) throws SQLException {
		// TODO Auto-generated method stub
		return getXiaSpUserDao().delete(pXxmSpUsers);
	}
	
	/*----------Notification Preference-------------*/
	
	@Override
	public ArrayList<XiaSpUserNotifyPrefs> getUserNotifications(int usrId) throws SQLException {
		return getXiaSpUserDao().getUserNotifications(usrId);
	}
	
	@Override
	public ArrayList<XiaSpUserNotifyPrefs> getNotifyPrefType(int usrId) throws SQLException {
		return getXiaSpUserDao().getNotifyPrefType(usrId);
	}
	
	@Override
	public int notifyPrefSave(XiaSpUserNotifyPrefs pXiaSpUserNotifyPrefs) throws SQLException {
		return getXiaSpUserDao().notifyPrefSave(pXiaSpUserNotifyPrefs);
	}
		
	@Override
	public int deleteNotification(XiaSpUserNotifyPrefs pXiaSpUserNotifyPrefs) throws SQLException {
		return getXiaSpUserDao().deleteNotification(pXiaSpUserNotifyPrefs);
	}
	
	@Override
	public  int deAllocateFunction(XiaSpUserFunctions uf) throws SQLException{
		return getXiaSpUserDao().deAllocateFunction(uf);
	}

	@Override
	public ArrayList<XiaSpUserFunctions> getUserFunctions(
			XiaSpUserFunctions pUserFunctions) throws SQLException {
		// TODO Auto-generated method stub
		return getXiaSpUserDao().getUserFunctions(pUserFunctions);
	}

	@Override
	public Long getCountNotify(XiaSpUserNotifyPrefs pXiaSpUserNotifyPrefs)throws SQLException {
		return getXiaSpUserDao().getCountNotify(pXiaSpUserNotifyPrefs);
	
	}

	@Override
	public ArrayList<XiaSpUserNotifyPrefs> getNotification(XiaSpUserNotifyPrefs pXiaSpUserNotifyPrefs) throws SQLException {
		return getXiaSpUserDao().getNotification(pXiaSpUserNotifyPrefs);
	
	}

	@Override
	public int allocateFunction(XiaSpUserFunctions uf) throws SQLException {
		// TODO Auto-generated method stub
		return getXiaSpUserDao().allocateFunction(uf);
	}

	@Override
	public String generatePassword(XiaSpUser user) throws Exception {
		 String currentTime = System.currentTimeMillis()+"";
		 String password = currentTime.substring(currentTime.length()-6)+user.getUsrName().substring(0,2).toLowerCase()+user.getUsrFirstName().substring(0,1).toUpperCase()+user.getUsrLastName().substring(0,1).toUpperCase();
		 return password;
	}

	@Override
	public ArrayList<XrcConcessionRoles> getUserRoles(Integer usrId) throws SQLException {
		// TODO Auto-generated method stub
		return getXiaSpUserDao().getUserRoles(usrId);
	}

	@Override
	public int saveUserRole(XiaSpUser xiaSpUser) throws SQLException {
		return getXiaSpUserDao().saveUserRole(xiaSpUser); 
	}
	
	@Override
	public int deleteUserRoles(int usrId) throws SQLException {
		return getXiaSpUserDao().deleteUserRoles(usrId); 
	}

	@Override
	public boolean checkUserRoleExists(int usrId) throws SQLException {
		return getXiaSpUserDao().checkUserRoleExists(usrId);
	}

	@Override
	public ArrayList<XiaSpUser> getUser(XiaSpUser xiaSpUser)
			throws SQLException {
		return getXiaSpUserDao().getUser(xiaSpUser);
	}

	@Override
	public Long getUserFunctionsCount(XiaSpUserFunctions pUserFunctions)
			throws SQLException {
		return getXiaSpUserDao().getUserFunctionsCount(pUserFunctions);
	}

	@Override
	public int approveUser(XiaSpUser xiaSpUser) throws SQLException {
		// TODO Auto-generated method stub
		return getXiaSpUserDao().approveUser(xiaSpUser); 
	}
	
	@Override
	public int updateRetrys(Integer usrId) throws SQLException {
		return getXiaSpUserDao().updateRetrys(usrId);
	}
}