package com.md.eta.services.menus;

import java.sql.SQLException;
import java.util.List;

import com.md.eta.model.SsoMenu;

public interface SsoMenuService {
	
	public int[] save(List<SsoMenu> ssoMenuList) throws SQLException;
	
	public int deleteAll() throws SQLException;

}
