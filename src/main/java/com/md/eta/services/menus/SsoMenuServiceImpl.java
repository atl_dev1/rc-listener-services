package com.md.eta.services.menus;

import java.sql.SQLException;
import java.util.List;

import com.md.eta.model.SsoMenu;
import com.md.eta.services.AbstractService;

public class SsoMenuServiceImpl extends AbstractService implements SsoMenuService{

	@Override
	public int[] save(List<SsoMenu> ssoMenuList) throws SQLException {
		// TODO Auto-generated method stub
		return getSsoMenuDao().save(ssoMenuList);
	}

	@Override
	public int deleteAll() throws SQLException {
		// TODO Auto-generated method stub
		return getSsoMenuDao().deleteAll();
	}

}
