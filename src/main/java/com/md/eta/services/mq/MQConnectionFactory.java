/**
File 	:	 MQConnectionFactory.java
Author	:	 Raj Varadarajan @ Sopra Group Limited.
Date	:	 Thu Jul 02 08:22:02 BST 2009
Description:	 
 */
package com.md.eta.services.mq;

import java.util.Enumeration;
import java.util.Properties;

import javax.jms.ConnectionFactory;

import org.apache.log4j.Logger;

public class MQConnectionFactory {
	private Properties props;
	Logger logger = Logger.getLogger(MQConnectionFactory.class);
	public void setProperties(Properties props) {
		this.props = props;
	}

	public Properties getProperties() {
		return this.props;
	}

	public ConnectionFactory createConnectionFactory() {
		com.sun.messaging.ConnectionFactory cf = new com.sun.messaging.ConnectionFactory();

		try {
			Enumeration keys = props.propertyNames();
			while (keys.hasMoreElements()) {
				String name = (String) keys.nextElement();
				String value = props.getProperty(name);
				cf.setProperty(name, value);
				logger.debug("------ ConnectionFactory called " + name
						+ "- " + value);
			}

		} catch (Exception e) {
			throw new RuntimeException(
					"MQConnectionFactoryFactory.createConnectionFactory() failed: "
							+ e.getMessage(), e);
		}
		return cf;
	}

}