package com.md.eta.services.portal.screenFunction;

import java.sql.SQLException;
import java.util.ArrayList;

import com.md.eta.model.admin.ScreenFunction;

public interface PortalScreenFunctionsService {
	
	
	
	
	public  ArrayList<ScreenFunction> fetch(ScreenFunction screenFunction) throws SQLException;
    
	public abstract int update(ScreenFunction screenFunction) throws SQLException;
    
	public abstract int  save(ScreenFunction screenFunction) throws Exception;

	public  Long getCount(ScreenFunction screenFunction) throws Exception;

	public ArrayList<ScreenFunction> getAllFunctions(ScreenFunction screenFunction)  throws SQLException;
	
	public ArrayList<ScreenFunction> getParentFunction(ScreenFunction screenFunction)  throws SQLException;

	public ArrayList<ScreenFunction> getDefaultFunctions(ScreenFunction screenFunction) throws SQLException;

	public boolean isUserFunctionExists(Integer fnId) throws SQLException;
	
	public boolean isCompanyFunctionExists(Integer fnId) throws SQLException;
}
