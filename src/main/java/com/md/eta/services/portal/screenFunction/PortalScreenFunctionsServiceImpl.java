package com.md.eta.services.portal.screenFunction;

import java.sql.SQLException;

import java.util.ArrayList;


import com.md.eta.model.admin.ScreenFunction;
import com.md.eta.services.AbstractService;

public class PortalScreenFunctionsServiceImpl extends AbstractService implements PortalScreenFunctionsService {

	@Override
	public ArrayList<ScreenFunction> fetch(ScreenFunction screenFunction)throws SQLException {
		return getPortalScreenFunctionsDao().fetch(screenFunction);
		
	}
	@Override
	public int update(ScreenFunction screenFunction ) throws SQLException {
		return getPortalScreenFunctionsDao().update(screenFunction);
	}

	@Override
	public int save(ScreenFunction screenFunction) throws Exception {
		return getPortalScreenFunctionsDao().save(screenFunction);
	}
	@Override
	public Long getCount(ScreenFunction screenFunction) throws Exception{
		return getPortalScreenFunctionsDao().getCount(screenFunction);
		
	}
	@Override
	public ArrayList<ScreenFunction> getParentFunction(ScreenFunction screenFunction) throws SQLException {
		return getPortalScreenFunctionsDao().getParentFunction(screenFunction);

	}
	
	@Override
	public ArrayList<ScreenFunction> getAllFunctions(ScreenFunction screenFunction)throws SQLException {
		return getPortalScreenFunctionsDao().getAllFunctions(screenFunction);
		
	}
	@Override
	public  ArrayList<ScreenFunction> getDefaultFunctions(
			ScreenFunction screenFunction) throws SQLException {
		return  getPortalScreenFunctionsDao().getDefaultFunctions(screenFunction);
	}
	@Override
	public boolean isUserFunctionExists(Integer fnId) throws SQLException {
		// TODO Auto-generated method stub
		return getPortalScreenFunctionsDao().isUserFunctionExists(fnId);
	}
	@Override
	public boolean isCompanyFunctionExists(Integer fnId) throws SQLException {
		// TODO Auto-generated method stub
		return getPortalScreenFunctionsDao().isCompanyFunctionExists(fnId);
	}
}