package com.md.eta.services.portal.users;

import java.sql.SQLException;
import java.util.ArrayList;

import com.md.eta.model.admin.Company;
import com.md.eta.model.portal.users.XxmSpUserFunctions;
import com.md.eta.model.portal.users.XxmSpUserNotifyPrefs;
import com.md.eta.model.portal.users.XxmSpUsers;
import com.md.eta.model.portal.users.XxmSpUsersMultiAccts;


public interface XxmSpUsersService {

	public ArrayList<XxmSpUsers> fetch(XxmSpUsers xxmSpUsers) throws SQLException;

	public int save(XxmSpUsers xxmSpUsers) throws SQLException;

	public int update(XxmSpUsers xxmSpUsers) throws SQLException;
	
	Long getCount(XxmSpUsers xxmSpUsers) throws SQLException;

	public ArrayList<XxmSpUsers> getUser(XxmSpUsers xxmSpUsers) throws SQLException;

	public ArrayList<Company> getCustomers() throws SQLException;
	
	public ArrayList<XxmSpUserNotifyPrefs> getNotifyPrefType(int usrId)throws SQLException;

	public int saveMultiUsersAccts(XxmSpUsersMultiAccts xxmSpUsersMultiAccts)throws SQLException;

	public int deleteExtraAccounts(XxmSpUsersMultiAccts xxmSpUsersMultiAccts) throws SQLException; 	

    Long getCountMultiAccounts(XxmSpUsersMultiAccts xxmSpUsersMultiAccts)throws SQLException; 

	public abstract ArrayList<XxmSpUsersMultiAccts> fetchMultiAccounts(XxmSpUsersMultiAccts xxmSpUsersMultiAccts) throws SQLException;
	
	public ArrayList<XxmSpUsersMultiAccts> fetchUserMultiAccounts(int usrId) throws SQLException;

	public int notifyPrefSave(XxmSpUserNotifyPrefs pXxmSpUserNotifyPrefs) throws SQLException;
	
	public ArrayList<XxmSpUserNotifyPrefs> getUserNotifications(int usrId) throws SQLException;

	public int deleteNotification(XxmSpUserNotifyPrefs pXxmSpUserNotifyPrefs) throws SQLException;

	public abstract ArrayList<XxmSpUserNotifyPrefs> getNotification(XxmSpUserNotifyPrefs pXxmSpUserNotifyPrefs) throws SQLException;

	Long getCountNotify(XxmSpUserNotifyPrefs pXxmSpUserNotifyPrefs)throws SQLException;

	public String generatePassword(XxmSpUsers user) throws Exception; 
	
	public int saveUserFunctions(XxmSpUserFunctions userFunctions) throws SQLException ;
	
	public ArrayList<XxmSpUserFunctions> getUserAllocatedFunctions(XxmSpUserFunctions userFunctions) throws SQLException;
	
	public Long getUserFunctionsCount(XxmSpUserFunctions pUserFunctions)throws SQLException;
	
	public int deleteUserFunctions(XxmSpUserFunctions userFunctions) throws SQLException ;
	
	public int userApprove(XxmSpUsers xxmSpUsers) throws SQLException;

	public ArrayList<Company> getUnallocattedMultiAccts(XxmSpUsersMultiAccts multiAccts) throws SQLException;
	
	public XxmSpUsers getCustomerDetails(String userName) throws SQLException;
}