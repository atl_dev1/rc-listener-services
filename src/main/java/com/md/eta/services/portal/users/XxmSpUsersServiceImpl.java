package com.md.eta.services.portal.users;

import java.sql.SQLException;
import java.util.ArrayList;

import com.md.eta.model.admin.Company;
import com.md.eta.model.portal.users.XxmSpUserFunctions;
import com.md.eta.model.portal.users.XxmSpUserNotifyPrefs;
import com.md.eta.model.portal.users.XxmSpUsers;
import com.md.eta.model.portal.users.XxmSpUsersMultiAccts;
import com.md.eta.services.AbstractService;

public class XxmSpUsersServiceImpl extends AbstractService implements XxmSpUsersService{

	@Override
	public ArrayList<XxmSpUsers> fetch(XxmSpUsers xxmSpUsers) throws SQLException {
		// TODO Auto-generated method stub
		return getXxmSpUsersDao().fetch(xxmSpUsers);
	}

	@Override
	public int save(XxmSpUsers xxmSpUsers) throws SQLException {
		// TODO Auto-generated method stub
		return getXxmSpUsersDao().save(xxmSpUsers);
	}

	@Override
	public int update(XxmSpUsers xxmSpUsers) throws SQLException {
		// TODO Auto-generated method stub
		return getXxmSpUsersDao().update(xxmSpUsers);
	}
	
	@Override
	public Long getCount(XxmSpUsers xxmSpUsers) throws SQLException {
		// TODO Auto-generated method stub
		return getXxmSpUsersDao().getCount(xxmSpUsers); 
	}
	
	@Override
	public ArrayList<XxmSpUsers> getUser(XxmSpUsers xxmSpUsers) throws SQLException {
		// TODO Auto-generated method stub
		return getXxmSpUsersDao().getUsers(xxmSpUsers);
	}
	
	@Override
	public ArrayList<Company> getCustomers() throws SQLException {
		return getXxmSpUsersDao().getCustomers();
	}

	@Override
	public ArrayList<XxmSpUserNotifyPrefs> getNotifyPrefType(int usrId)
			throws SQLException {
		return getXxmSpUsersDao().getNotifyPrefType(usrId);
	}

	/*@Override
	public ArrayList<XxmSpUserNotifyPrefs> getNotification(int usrId)
			throws SQLException {
		return getUsersDAO().getNotification(usrId);
	}*/
	/*public int notifyPrefSave(XxmSpUserNotifyPrefs pXxmSpUserNotifyPrefs) throws SQLException {
		
		return getUsersDAO().notifyPrefSave(pXxmSpUserNotifyPrefs);
	}*/
	@Override
	public int saveMultiUsersAccts(XxmSpUsersMultiAccts xxmSpUsersMultiAccts)throws SQLException {
		return getXxmSpUsersDao().saveMultiUsersAccts(xxmSpUsersMultiAccts);
		}

	@Override
	public int deleteExtraAccounts(XxmSpUsersMultiAccts xxmSpUsersMultiAccts)throws SQLException {
		return getXxmSpUsersDao().deleteExtraAccounts(xxmSpUsersMultiAccts);
	}

	@Override
	public Long getCountMultiAccounts(XxmSpUsersMultiAccts xxmSpUsersMultiAccts)throws SQLException {
		return getXxmSpUsersDao().getCountMultiAccounts(xxmSpUsersMultiAccts); 
	}
	
	@Override
	public ArrayList<XxmSpUserNotifyPrefs> getUserNotifications(int usrId) throws SQLException {
		return getXxmSpUsersDao().getUserNotifications(usrId);
	}
	
	@Override
	public int notifyPrefSave(XxmSpUserNotifyPrefs pXxmSpUserNotifyPrefs) throws SQLException {
		return getXxmSpUsersDao().notifyPrefSave(pXxmSpUserNotifyPrefs);
	}
		
	@Override
	public int deleteNotification(XxmSpUserNotifyPrefs pXxmSpUserNotifyPrefs) throws SQLException {
		return getXxmSpUsersDao().deleteNotification(pXxmSpUserNotifyPrefs);
	}

	@Override
	public ArrayList<XxmSpUsersMultiAccts> fetchMultiAccounts(XxmSpUsersMultiAccts xxmSpUsersMultiAccts) throws SQLException {
		return getXxmSpUsersDao().fetchMultiAccounts(xxmSpUsersMultiAccts);
	
	}
	
	@Override
	public ArrayList<XxmSpUsersMultiAccts> fetchUserMultiAccounts(int usrId) throws SQLException {
		return getXxmSpUsersDao().fetchUserMultiAccounts(usrId);
	}

	@Override
	public Long getCountNotify(XxmSpUserNotifyPrefs pXxmSpUserNotifyPrefs)throws SQLException {
		return getXxmSpUsersDao().getCountNotify(pXxmSpUserNotifyPrefs);
	
	}

	@Override
	public ArrayList<XxmSpUserNotifyPrefs> getNotification(XxmSpUserNotifyPrefs pXxmSpUserNotifyPrefs) throws SQLException {
		return getXxmSpUsersDao().getNotification(pXxmSpUserNotifyPrefs);
	
	}

	@Override
	public String generatePassword(XxmSpUsers user) throws Exception{
		    String currentTime = System.currentTimeMillis()+"";
		    String password = currentTime.substring(currentTime.length()-6)+user.getUsrFirstName().substring(0,1).toLowerCase()+user.getUsrLastName().substring(0,1).toLowerCase()+user.getUsrFirstName().substring(0,1).toUpperCase()+user.getUsrLastName().substring(0,1).toUpperCase();
		    return password;
		   }

	@Override
	public int saveUserFunctions(XxmSpUserFunctions userFunctions) throws SQLException {
		return getXxmSpUsersDao().saveUserFunctions(userFunctions);
	}

	@Override
	public ArrayList<XxmSpUserFunctions> getUserAllocatedFunctions(
			XxmSpUserFunctions userFunctions) throws SQLException {
		return getXxmSpUsersDao().getUserAllocatedFunctions(userFunctions);
	}
	
	@Override
	public Long getUserFunctionsCount(XxmSpUserFunctions pUserFunctions)throws SQLException {
		return getXxmSpUsersDao().getUserFunctionsCount(pUserFunctions);
	}

	@Override
	public int deleteUserFunctions(XxmSpUserFunctions userFunctions)
			throws SQLException {
		return getXxmSpUsersDao().deleteUserFunctions(userFunctions);
	}
	
	@Override
	public int userApprove(XxmSpUsers xxmSpUsers) throws SQLException {
		return getXxmSpUsersDao().userApprove(xxmSpUsers);
	}

	@Override
	public ArrayList<Company> getUnallocattedMultiAccts(
			XxmSpUsersMultiAccts multiAccts) throws SQLException {
		// TODO Auto-generated method stub
		return getXxmSpUsersDao().getUnallocattedMultiAccts(multiAccts); 
	}

	@Override
	public XxmSpUsers getCustomerDetails(String userName) throws SQLException {
		return getXxmSpUsersDao().getCustomerDetails(userName);
	}

}