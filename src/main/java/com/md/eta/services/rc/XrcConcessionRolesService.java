package com.md.eta.services.rc;

import java.sql.SQLException;
import java.util.ArrayList;
import com.md.eta.model.iportal.users.XrcConcessionRoles;

public interface XrcConcessionRolesService {

	public abstract ArrayList<XrcConcessionRoles> fetch(XrcConcessionRoles xrcConcessionRoles)throws SQLException;

    public abstract int save(XrcConcessionRoles xrcConcessionRoles) throws SQLException;

    public abstract int update(XrcConcessionRoles xrcConcessionRoles) throws SQLException;

    Long getCount(XrcConcessionRoles xrcConcessionRoles) throws SQLException;

    public ArrayList<XrcConcessionRoles> getRoles(XrcConcessionRoles xrcConcessionRoles) throws SQLException;
    
    public int delete(XrcConcessionRoles xrcConcessionRoles) throws SQLException;
    
    public boolean isUserFunctionExists(Integer roleId) throws SQLException;

}
