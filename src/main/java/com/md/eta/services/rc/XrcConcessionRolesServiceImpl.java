package com.md.eta.services.rc;

import java.sql.SQLException;
import java.util.ArrayList;
import com.md.eta.model.iportal.users.XrcConcessionRoles;
import com.md.eta.services.AbstractService;


public class XrcConcessionRolesServiceImpl extends AbstractService implements XrcConcessionRolesService{

	@Override
	public ArrayList<XrcConcessionRoles> fetch(XrcConcessionRoles xrcConcessionRoles) throws SQLException {
		return getXrcConcessionRolesDao().fetch(xrcConcessionRoles); 
		
	}

	@Override
	public int save(XrcConcessionRoles xrcConcessionRoles) throws SQLException {
		return getXrcConcessionRolesDao().save(xrcConcessionRoles);
	}

	@Override
	public int update(XrcConcessionRoles xrcConcessionRoles)throws SQLException {
		return getXrcConcessionRolesDao().update(xrcConcessionRoles);
		
	}

	@Override
	public Long getCount(XrcConcessionRoles xrcConcessionRoles)throws SQLException {
		return getXrcConcessionRolesDao().getCount(xrcConcessionRoles); 
	}

	@Override
	public ArrayList<XrcConcessionRoles> getRoles(XrcConcessionRoles xrcConcessionRoles) throws SQLException {
		return getXrcConcessionRolesDao().getRoles(xrcConcessionRoles);
	}

	@Override
	public int delete(XrcConcessionRoles xrcConcessionRoles) throws SQLException {
		return getXrcConcessionRolesDao().delete(xrcConcessionRoles);
	}

	@Override
	public boolean isUserFunctionExists(Integer roleId) throws SQLException {
		// TODO Auto-generated method stub
		return getXrcConcessionRolesDao().isUserFunctionExists(roleId);
	}

}
