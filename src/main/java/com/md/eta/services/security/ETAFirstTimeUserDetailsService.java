package com.md.eta.services.security;


import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.md.eta.dao.security.UserRepository;
import com.md.eta.model.security.User;

public class ETAFirstTimeUserDetailsService  implements UserDetailsService{
	
	private UserRepository userRepository;

	
	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException, DataAccessException {
		try {
			User domainUser = userRepository.findByUsernameFirstTime(username);
			if (domainUser == null){
				throw new UsernameNotFoundException("Invalid or unrecognized username");
			} 
			return domainUser;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	

	public UserRepository getUserRepository() {
		return userRepository;
	}
	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}
}
