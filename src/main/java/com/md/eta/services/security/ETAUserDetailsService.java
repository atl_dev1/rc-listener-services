package com.md.eta.services.security;


import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.md.eta.dao.security.UserRepository;
import com.md.eta.model.security.User;

public class ETAUserDetailsService  implements UserDetailsService{
	
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException, DataAccessException {
		try {
			User domainUser = userRepository.findByUsername(username);
			
			return domainUser;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	
	public void changePassword(User user) throws SQLException{
		try {
			userRepository.savePassword(user);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}


	public UserRepository getUserRepository() {
		return userRepository;
	}
	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}
	
}
