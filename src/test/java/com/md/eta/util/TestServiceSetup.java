/**
 * 
 */
package com.md.eta.util;

import org.springframework.context.support.FileSystemXmlApplicationContext;

/**
 * @author Raj Varadarajan
 * @date 14 Jun 2010
 * com.md.spiceweb.actions
 * 
 */
public class TestServiceSetup {
	
//	private String configLocation = "E:\\Projects\\MD\\portal\\web\\WEB-INF\\applicationContext.xml";
	private String configLocation = "classpath:test-daoContext.xml";
	private FileSystemXmlApplicationContext springCtx;
	public void startUp(){
		try {
			long startUpTime = System.currentTimeMillis();
			setSpringCtx(new FileSystemXmlApplicationContext(configLocation));
			System.out.println("Loaded context in " + (System.currentTimeMillis()-startUpTime));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void startUp(String configLocation){
		try {
			long startUpTime = System.currentTimeMillis();
			setSpringCtx(new FileSystemXmlApplicationContext(configLocation));
			System.out.println("Loaded context in " + (System.currentTimeMillis()-startUpTime));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public static void main(String[] args){
		TestServiceSetup setup = new TestServiceSetup();
		setup.startUp();
	}

	/**
	 * @param springCtx the springCtx to set
	 */
	public void setSpringCtx(FileSystemXmlApplicationContext springCtx) {
		this.springCtx = springCtx;
	}

	/**
	 * @return the springCtx
	 */
	public FileSystemXmlApplicationContext getSpringCtx() {
		return springCtx;
	}
	

}
